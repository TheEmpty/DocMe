<?php
session_start();

  require_once "config.php";
  require_once "objects/user.php";
  require_once "objects/edit.php";
  require_once "objects/project.php";
  require_once "objects/article.php";

  require_once "resources/scripts/pdo.php";

  $GLOBALS["Path"] = $Path;
  $GLOBALS["User"] = new USER($db);
  $GLOBALS["Project"] = new PROJECT($db);
  $GLOBALS["Article"] = new ARTICLE($db);
  $GLOBALS["Editor"] = new EDITOR_FACTORY($db);
  //Kontrola, zda je uživatel přihlášen
  //$GLOBALS["User"]->is_loggedin();

  $get_action = isset( $_GET['action'] ) ? $_GET['action'] : "";
  $get_hash = isset( $_GET['hash']) ? $_GET['hash'] : "";
  $get_ID = isset( $_GET['ID']) ? $_GET['ID'] : "";
  $get_description = isset( $_GET['desc']) ? $_GET['desc'] : "";
  $get_type = isset( $_GET['type']) ? $_GET['type'] : "";
  $get_name = isset( $_GET['name']) ? $_GET['name'] : "";
  $get_theme = isset( $_GET['theme']) ? $_GET['theme'] : "";
  $get_key = isset($_GET['key']) ? $_GET['key'] : '';

  $post_action = isset( $_POST['action']) ? $_POST['action'] : "";
  $post_ID = isset( $_POST['ID']) ? $_POST['ID'] : "";
  $post_repassword = isset( $_POST['recovery_password']) ? $_POST['recovery_password'] : "";
  $post_name = isset( $_POST['name'] )? $_POST['name'] : "";
  $post_description = isset( $_POST["description"]) ? $_POST['description'] : "";
  $post_email = isset($_POST["email"]) ? $_POST["email"] : "";
  $post_password = isset($_POST["password"]) ? $_POST["password"] : "";
  $post_bio = isset($_POST["bio"]) ? $_POST["bio"] : "";
  $post_college = isset($_POST["college"]) ? $_POST["college"] : "";
  $post_highschool = isset($_POST["highschool"]) ? $_POST["highschool"] : "";
  $post_experience = isset($_POST["experience"]) ? $_POST["experience"] : "";
  $post_file = isset($_POST["file"]) ? $_POST["file"] : "";
  $post_folder = isset($_POST["folder"]) ? $_POST["folder"] : "";
  $post_type = isset($_POST["type"]) ? $_POST["type"] : "";
  $post_gender = isset($_POST["gender"]) ? $_POST["gender"] : "";
  $post_born = isset($_POST["birth"]) ? $_POST["birth"] : "";
  $post_color = isset($_POST["color"]) ? $_POST["color"] : "";

  //$post_saveEditorJson = isset($_POST["saveProjectJson"])  ? $_POST["saveProjectJson"] : "";
  $post_saveElementJson = isset($_POST["elementJson"]) ? $_POST["elementJson"] : "";
  $post_saveConfigJson = isset($_POST["configJson"]) ? $_POST["configJson"] : "";
  $post_saveCustomJson = isset($_POST["customJson"]) ? $_POST["customJson"] : "";

  switch ( $get_action ) {
    case '': //Pokud někdo posílá prázdný GET parametr "action"
      header("Location: ../index.php");
      break;
    case 'login':
      login($post_email, $post_password);
      break;
    case 'logout': //Pokud je potřeba někoho logoutnout
      logout();
      break;
    case 'openEditor': //Otevření projektu (editoru)
      openEditor($get_ID);
      break;
    case 'createProject': //Pokud se vytváří nový projekt
      createProject($get_type, $post_name, $post_description, $get_theme);
      break;
    case 'editProject': //Pokud se edituje hlavička již vytvořeného projektu
      editProject($post_ID, $post_name, $post_description);
      break;
    case 'deleteProject': //Pokud se maže již vytvořený projekt
      deleteProject($get_ID);
      break;
    case 'saveProject':
      saveProject($post_ID, $post_saveElementJson, $post_saveConfigJson, $post_saveCustomJson);
      break;
    case 'recovery_link': //Získání recovery hashe (pro obnovu profilu) v databázi "UsersMeta"
      recovery_link($post_hash);
      break;
    case 'recovery_script': //Aktualizace záznamu uživatele v DB (po obnovení profilu)
      recovery_script($get_ID, $post_repassword);
      break;
    case 'deleteArticle': //Smazání záznamu příspěvku z DB
      deleteArticle($get_ID);
      break;  
    case 'editArticle': //Editace příspěvku
      editArticle($post_ID, $post_name, $post_description, $post_content, $post_type, $post_status);
      break;
    case 'deleteUser':
      deleteUser($get_ID);
      break;
    case 'editUser':
      editUser($post_ID, $post_name, $post_email, $post_password, $post_gender, $post_born);
      break;
    case 'saveUserBtn':
      saveUserBtn($post_color);
      break;
    case 'saveUserNav':
      saveUserNav($post_color);
      break;
    case 'echome':
      echome();
      break;
    case 'downloadProject':
      downloadProject();
      break;
    case 'newUserTheme':
      createUserTheme($post_ID, $post_folder, $post_file, $post_type, $post_name, $post_bio, $post_college, $post_highschool, $post_experience);
      break;
    case 'deleteUserTheme':
      deleteUserTheme($post_ID, $post_file, $post_folder);
      break;
    case 'deleteUserPic':
      deleteUserPic($post_ID, $post_file, $post_folder);
      break;
    case 'useAsProfilePic':
      useAsProfilePic($post_ID, $post_file);
      break;
    case 'registrateUserViaGoogle':
      registrateUserViaGoogle($post_ID, $post_name, $post_email);
    default:
      header("Location: ../index.php");
      break;

  }

  function login($email, $pswd){
    $return = $GLOBALS["User"]->login($email, $pswd);
    if(!empty($return)){
     // echo "<script>location.href='". path ."';</script>";
      header("Location: index.php");
      exit();
    }else{
      header("Location: ../index.php");
      exit();
    }
  }

  function downloadProject($key){
    $GLOBALS["Project"]->downloadProject($key);
  }

  function registrateUserViaGoogle($id, $name, $email){
    $GLOBALS["User"]->register_google($id, $name, $email);
    //header("Location: index.php");
    exit();
  }

  function openEditor($id){ //Otevření editoru
  	$_SESSION["edit_ID"] = $id;
  	$openEditorPermission = $GLOBALS["Project"]->checkPermissions($id, $_SESSION["user_session"]);
  	if($openEditorPermission == true){
  		unset($_SESSION["editPermission"]);
  		unset($_SESSION["edit_id"]);
  		$_SESSION["edit_id"] = $id;
		$_SESSION["editPermission"] = true;
  	}else{
  		$_SESSION["editPermission"] = false;
  	}
  	header("Location: editor/editor.php");
    exit();
  }

  //funkce na vytvoření nového souboru
  function createProject($type, $name, $description, $theme){

     $GLOBALS["Project"]->create($name, $description, $_SESSION["user_session"], $type, $theme, $userKey, $_SESSION["email"]);
    header("Location: index.php");
    exit();
  }

  function editProject($id, $name, $description){

    $GLOBALS["Project"]->editBasics($id, $name, $description);
    header("Location: index.php");
    exit();
  }

  function deleteProject($ID){

    $GLOBALS["Project"]->delete($ID, $_SESSION["email"]);
    header("Location: index.php");
    exit();
  }

  function saveProject($projectID, $elementJson, $configJson, $customJson){
    $return = $GLOBALS["Editor"]->save($projectID ,$elementJson, $configJson, $customJson);
    return $return;
  }

  //user->logout() funkce
   function logout() {

  	include_once "resources/scripts/pdo.php";
  	$var = $GLOBALS["User"]->logout();
    if ($var == true){
          header("Location: ../index.php");
          unset($var);
          exit();
    }else{ //Pokud neexistuje instance uživatele
          echo "Failure"; 
          header("Location: ../index.php");
          exit();
    }

  }

   function recovery_link($recovery_hash){

      include_once "resources/scripts/pdo.php";

      $metaRec ="SELECT * FROM UsersMeta WHERE RecoveryHash = :recoveryhash LIMIT 1";
      $metaRecovery = $db->prepare($metaRec);
      $metaRecovery->execute(array(":recoveryhash" => $recovery_hash));
      $metaRecoveryResult=$metaRecovery->fetchAll();
      foreach($metaRecoveryResult as $data){
        $_SESSION["recovery_who"] = $data["UsersID"];
        header("Location: ../pages/recovery.php");
        exit();
      
      }
   }

   function recovery_script($recovery_ID, $recovery_password){
        
        $renew_password = hash("sha256", $recovery_password);

        include_once "resources/scripts/pdo.php";
        $recovery_sql = "UPDATE Users SET Password = :password WHERE ID=:usersID";
        $recovery_on = $db->prepare($recovery_sql);
        $recovery_on->execute(array(":password" => $renew_password, ":usersID" => $recovery_ID));

        $_SESSION["annoucment"] = "Your password was successfully recovered!";
        header("Location: ../pages/login.php");
        exit();

   }

   function deleteArticle($id){
        $GLOBALS["Article"]->deleteArticle($id);
        header("Location: /pages/admin/view_article.php");
        exit();
   }

   function editArticle($id, $name, $description, $content, $type, $status){
   		$GLOBALS["Article"]->editArticle($id, $name, $description, $content, $type, $status);
   		header("Location: /pages/admin/view_article.php");
      exit();
   }

   function deleteUser($id){
      $GLOBALS["User"]->deleteMe($id);
      header("Location: /pages/admin/view_user.php");
      exit();
   }

   function editUser($id, $username, $email, $password, $gender, $born){
      $GLOBALS["User"]->edit($id, $username, $email, $password, $gender, $born);
      header("Location: index.php?page=profile");
      exit();
   }

   function saveUserNav($color){
    $GLOBALS["User"]->saveNav($_SESSION["user_session"], $color);
    return "Uloženo";
   }

   function saveUserBtn($color){
    $GLOBALS["User"]->saveBtn($_SESSION["user_session"], $color);
    return "Uloženo";
   }

   function createUserTheme($id, $folder, $file, $type, $name, $bio, $college, $highschool, $experience){
      $GLOBALS["Editor"]->createUserTheme($id, $folder, $file, $type, $name, $bio, $college, $highschool, $experience);
      header("Location: index.php?page=profile");
      exit();
   }

   function deleteUserTheme($id, $themeID, $folder){
      $GLOBALS["Editor"]->deleteUserTheme($id, $themeID, $folder);
      exit();
   }

   function deleteUserPic($id, $picID, $folder){
      $GLOBALS["User"]->deletePicture($id, $picID, $folder);
      exit();
   }

   function useAsProfilePic($id, $picID){
      $GLOBALS["User"]->useAsProfilePic($id, $picID);
      exit();
   }

   function echome(){
      echo $GLOBALS["return"]->ID;
      echo " Class ID hodnota: ". $GLOBALS["User"]->ID ."<br>";
      echo " Session ID hodnota: " . $_SESSION["user_session"] . "<br>";
   }
?>