    <ul id="slide-out" class="side-nav">  
      <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li>
            <a class="collapsible-header"><i class="material-icons">group</i>Příspěvky<i class="material-icons right">arrow_drop_down</i></a>
            <div class="collapsible-body">
              <ul>
                <li><a href="pages/admin/view_article.php"><i class="material-icons">content_paste</i>Přehled příspěvků</a></li>
                <li><a href="pages/admin/new_article.php"><i class="material-icons">insert_comment</i>Vytvoření příspěvku</a></li>
                <li><a href="#!"><i class="material-icons">mode_comment</i>Přehled komentářů</a></li>
              </ul>
            </div>
          </li>
        </ul>
      </li>
      <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li>
            <a class="collapsible-header"><i class="material-icons">credit_card</i>Projekty<i class="material-icons right">arrow_drop_down</i></a>
            <div class="collapsible-body">
              <ul>
                <li><a href="pages/admin/view_project.php"><i class="material-icons">content_paste</i>Přehled projektů</a></li>
                <li><a href="pages/admin/create_project.php"><i class="material-icons">create_new_folder</i>Vytvoření projektu</a></li>
              </ul>
            </div>
          </li>
        </ul>
      </li>
       <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li>
            <a class="collapsible-header"><i class="material-icons">announcement</i>Oznámení<i class="material-icons right">arrow_drop_down</i></a>
            <div class="collapsible-body">
              <ul>
                <li><a href="pages/admin/view_announcement.php"><i class="material-icons">collections_bookmark</i>Přehled oznámení</a></li>
                <li><a href="pages/admin/create_announcemenet.php"><i class="material-icons">add_to_photos</i>Vytvoření oznámení</a></li>
              </ul>
            </div>
          </li>
        </ul>
      </li>
      <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li>
            <a class="collapsible-header"><i class="material-icons">group</i>Uživatelé<i class="material-icons right">arrow_drop_down</i></a>
            <div class="collapsible-body">
              <ul>
                <li><a href="pages/admin/view_user.php"><i class="material-icons">group</i>Přehled uživatelů</a></li>
                <li><a href="pages/admin/new_user.php"><i class="material-icons">group_add</i>Vytvoření uživatele</a></li>
              </ul>
            </div>
          </li>
        </ul>
      </li>
    </ul>
  <a href="#" data-activates="slide-out" class="button-collapse"><i class="material-icons medium">keyboard_arrow_right</i></a>
          