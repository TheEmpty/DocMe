<?php 
	//$config = require_once "../config.php";
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="utf-8" />
		<meta author="Jan Kočvara" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
     	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
     	<link rel="stylesheet" type="text/css" href="../resources/styles/style.css" />
     	<!-- linkování na všechny js knihovny, css soubory určené pro Drag&Drop -->
     	<link rel="stylesheet" type="text/css" href="../resources/drag/objectDrag.css"/>
     	<script src="../resources/drag/interact.js"> </script>
		<script src="../resources/drag/drag.js"> </script>  
		<script src="../resources/drag/changeSize.js"> </script>	
		<title> Editor / DocMe! </title>
	</head>
	<body>
		<div class="page">
			<?php 
				//include_once "../resources/includes/editor_menu.php";
			?>
			<h1> Editor Demo Testing </h1>
			<form method='POST' action=''> 
			 	<ul>
			 		<li class="sortable"> ONE </li>
			 		<li class="sortable"> TWO </li>
			 	</ul>
				<div class='project_content resize-container' style='height:100%'>
			 		<div class='input-field col s12 m10 orange draggable' id='drag-1'>
						<input type='text' class='project_h1 validate' id='h1' name='h1' placeholder='". $user["Firstname"] ." ". $user["Surname"] ."'/> 
					</div> 
				   		<div class='project_bio purple resize-drag' id='drag-2'> 
							<input type='text' class='project_description' id='bio' name='bio' placeholder='Něco málo o vás, co jste za člověka, jakou máte vizi...'/> 
						</div> 
					<div class='timeline'> 
						
					</div> 
					<div class='skills'> 
						
					</div>
				</div> 
			</form>
		</div>
	</body>
</html>
