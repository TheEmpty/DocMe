<?php
	session_start();
?>
<!DOCTYPE HTML>
 <html>
 	<head>
 		<meta charset="utf-8" />
		<meta author="Jan Kočvara" />

		<link rel="stylesheet" type="text/css" href="resources/styles/dd.css"/>

		<link rel="stylesheet" type="text/css" href="resources/styles/style.css" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css"/>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
	    <script src="js/interact.js"> </script>
	    <script src="js/basic.js"> </script>
     	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>
     	<script>
     	$(document).ready(function () {
		    $('select').material_select();
     		$(".button-collapse").sideNav();
  			$('#textarea1').trigger('autoresize');
     	});
     	</script>
     	<title> DocMe! </title>

 	</head>
 	<body>
 		<div class="page">
 			<?php 
 				include_once "../../resources/includes/nav_sub.php";
 				include_once "../../resources/includes/admin_menu_sub.php";
 				include_once "../../../resources/includes/msg.php";
	 		?>
	 		<div class="row">
			    	<form class="col s12" method="POST">
	      				<div class="row">
					        <div class="input-field col s6">
						        <input name="first_name" id="first_name" type="text" class="validate" minlength="3">
						        <label for="first_name" data-error="You need to write 3 symbols at least.">First Name</label>
					        </div>
					        <div class="input-field col s6">
						        <input name="last_name" id="last_name" type="text" class="validate" minlength="2">
						        <label for="last_name" data-error="You need to write 2 symbols at least.">Last Name</label>
					        </div>
					    </div>
				      	<div class="row">
					        <div class="input-field col s12">
						        <input name="password" id="password" type="password" class="validate" minlength="5">
						        <label for="password" data-error="You need to write 5 symbols at least.">Password</label>
					        </div>
					    </div>
					    <div class="row">
					        <div class="input-field col s12">
						        <input name="email" id="email" type="email" class="validate">
						        <label for="email" data-error="Invalid E-mail format.">Email</label>
					        </div>
					    </div>
					    <div class="row">
				      <div class="input-field col s6">
					    <select name="level">
					      <option value="" disabled selected>Zvolte oprávnění</option>
					      <option value="0">Uživatel</option>
					      <option value="1">Moderátor</option>
					      <option value="2">Administrátor</option>
					    </select>
					    <label>Typ příspěvku</label>
					  </div>	
					    <button class="btn waves-effect waves-light" type="submit" name="action"> Registrate </button>
					</form>
				</div>
  			<?php
  				if(isset($_POST["action"])){
  					if(!empty($_POST["first_name"]) && ($_POST["last_name"]) && ($_POST["password"]) && ($_POST["email"]) && ($_POST["level"])){
  						$pom_type = explode("_", $_POST["level"]);
  						$level = $pom_type[0];
  						$firstname = $_POST["first_name"];
  						$surname = $_POST["last_name"];
  						$password = $_POST["password"];
  						$email = $_POST["email"];

  						include_once "../../resources/scripts/pdo.php";
  						$user = new USER($db);
  						$user->register($firstname, $surname, $password, $email, $level);
	  				}else{
	  					unset($_SESSION["error"]);
	  					$_SESSION["error"] = "Nevyplnili jste všechny potřebné údaje!";
	  				}
  				}else{
  					unset($_SESSION["error"]);
  				}				
  			?>
	 	</div>
	 </body>
</html>