<?php
	require "../admin/config.php";
	session_start();
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="utf-8" />
		<meta author="Jan Kočvara" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
     	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
     	<link rel="stylesheet" type="text/css" href="../resources/styles/style.css" />
     	<script> $(".button-collapse").sideNav(); </script>
		<title> DocMe! </title>
	</head>
	<body>
		<div class="page">
			<?php include_once '../resources/includes/nav_pages.php';
				  include_once '../resources/includes/msg.php';
			 ?>
			<div class="content">
				<div class="text-box">
					<h5> Version 0.4 </h5>
					<ul>
						<li> - Re-designed websie. </li>
						<li> - Fix user account login script. </li>
						<li> - Added new icons. </li>
						<li> - Updated F&Q (in progress). </li>
						<li> - I re-wrote parts of HTML / PHP / CSS code for better </li>
					</ul>
					<br/>
				</div>	
				<div class="text-box">
					<h5> Version 0.3 </h5>
						<ul>
							<li> - Updated pages. </li>
							<li> - Implemented and activated registration with login page (still hidden). </li>
							<li> - Little differences in web design. </li>
							<li> - Updated F&Q (in progress). </li>
							<li> - Fixed forms. </li>
							<li> - Start preparing administration (code) </li>
							<li> - Start preparing profile UI with administration UI </li>
						</ul>
					<br/>
				</div>	
				<div class="text-box">
					<h5> Version 0.2 </h5>
						<ul>
							<li> - Implementation of Registration page for new users. </li>
							<li> - Started implementation of Login page for new users. </li>
							<li> - Created and implementated database. </li>
							<li> - Implementated stable connection to database. </li>
						</ul>
					<br/>
				</div>							
				<div class="text-box">
					<h5> Version 0.1.1 </h5>
						<ul>
							<li> - Added icons. </li>
							<li> - HotFix of CSS animations. </li>
						</ul>
					<br/>
				</div>
				<div class="text-box">
					<h5> Version 0.1 </h5>
						<ul>
							<li>  - Created and included all website pages. </li>
							<li>  - Created elementary CSS structure. </li>
							<li>  - Included CSS framework Materilize. </li>
							<li>  - Tested our API for converting HTML into PDF format. </li> 
							<li>  - Implementation of jQuery via Google API. </li>
							<li>  - Implementation of Google Fonts via Google API. </li>
						</ul>
					<br/>
				</div>
			</div>		
			<?php include_once '../resources/includes/footer.php'; ?>	
		</div>
	</body>
</html> 