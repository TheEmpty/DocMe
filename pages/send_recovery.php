<?php
	session_start();
	require_once "../resources/scripts/pdo.php";
	require_once "../admin/objects/user.php";
	$user = new USER($db);
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="utf-8" />
		<meta author="Jan Kočvara" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
     	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
     	<link rel="stylesheet" type="text/css" href="../resources/styles/style.css" />
     	<script> $(".button-collapse").sideNav(); </script>
         
		<title> DocMe! </title>
	</head>
	<body>
		<div class="page">
			<?php  include_once "../resources/includes/nav_pages.php";
			 		include_once "../resources/includes/msg.php";
			?>		
			<div class="content">
				<h2 class="text-center"> Obnova hesla / účtu </h2>
			 	<div class="row">
			    	<form class="col s12" method="POST">
		      			<div class="row">
					        <div class="input-field col s6">
				        	   <input name="email" id="email" type="email" class="validate">
					           <label for="email" data-error="Invalid email format.">Napište registrační email účtu, který se pokoušíte obnovit.</label>
					        </div>
						 </div>
					        <button class="btn waves-effect waves-light" type="submit" name="action"> Odeslat email pro obnovení účtu </button>
			   		</form>
				</div>
			<?php
				if(isset($_POST["action"])){
					if(isset($_POST["email"])){
						$user->recovery($_POST["email"]);						
					}else{
						echo "<p> Nevyplnili jste všechny pole! </p>";
					}
				}
			?>
			</div>
			<?php include_once "../resources/includes/footer.php" ?>
		</div>
	</body>
</html> 