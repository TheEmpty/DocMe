<?php
	//$configs = require "../admin/config.php";
	session_start();
?>
<!DOCTYPE HTML>
<html>
	<head>
	<meta charset="utf-8" />
	<meta charset="utf-8" />
		<meta author="Jan Kočvara" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
     	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
     	<link rel="stylesheet" type="text/css" href="../resources/styles/style.css" />
     	<script> $(".button-collapse").sideNav(); </script>
         
        <title> DocMe! </title> 
	</head>
	<body>
		<div class="page">
			<?php 
			//include_once $configs->path."resources/includes/nav_pages.php"
			include_once "../resources/includes/nav_pages.php";
			 ?>
			<div class="content">
			 	<div class="text-box">
			 	<h2> About DocMe! project </h2>
					<p> Project <span class="bold"> DocMe! </span> was created as school project of one student from Czech Republic. My name is <span class="bold"> Jan Kočvara </span> and this is my project DocMe! </p>
					<br />
					<p> This project has started at 11.09.2017 (DD/MM/YYYY). Before the project has been created, I did few test versions to discover potential bugs, threats in programming and decided which programming language I am going to use. I chose the PHP, specially the "spagheti" programming style combined with OOP style. The basic alone-standing functions are written in typical spagheti PHP style and the complicated or functions what are connected to some other functions (like login and logout) are written in OOP to speed up the processing.</p>
					<br />
					<p> Every single code of administration, or pages setted for visitors are written by myself, It's original code. We are using these codes from other web portals:
					<ul>
						<li>  - Materilize CSS Framework </li>
						<ul> 
							<li>This page is using Materilize CSS Framework to be more responsible for mobile phones, or on very different display resolutions. Also this framework has pre-setted some animations based on jQuery.
						</ul>
						<li> - PDFy.net </li>
						<ul>
							<li> For converting from HTML source code (or URL) to PDF this page using PDFy.net convertor. It's free API that access the website and convert the website into PDF format file. </li>
						</ul>
					</ul>
					<br />

 			 	</div>
			</div>
			<?php include_once '../resources/includes/footer.php'; ?>
		</div>
	</body>
</html> 