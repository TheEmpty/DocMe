<!DOCTYPE HTML>
 <html>
 	<head>
 		<meta charset="utf-8" />
		<meta author="Jan Kočvara" />

		<link rel="stylesheet" type="text/css" href="resources/styles/dd.css"/>
		<link rel="stylesheet" type="text/css" href="resources/styles/style.css"/> 
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css"/>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
		<script src="resources/scripts/dropdownContextMenu.js"> </script>
     	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>
     	<link rel="stylesheet" type="text/css" href="resources/styles/cards.css"/>
     	<script src="https://apis.google.com/js/platform.js" async defer></script>
     	<meta name="google-signin-client_id" content="864532971569-r15luhunka88mfp4m2qld9k0uc89smcr.apps.googleusercontent.com"> 
	
	   	<?php include "resources/scripts/user_color.php" ?>

     	<script> 
     	 $(document).ready(function(){
     	    $(".dropdown-button").dropdown();
     	    $(".button-collapse").sideNav();
		    $('.modal').modal();

		    	//
		    /*$(document.body).on('click', '.delete', function(e){
		    	var project_id = $(this).attr('id');
		    	//alert(project_id);
		  
			    $.ajax({
					url: 'core.php?action=deleteProject',
					type: 'POST',
					contentType: 'text; charset=utf-8', //Formát, ve kterém já posílám
					dataType: 'text', //Formát odpovědi
					data: {ID: project_id},
					success: function(response){
						Materialize.toast("Projekt byl úspěšně smazán", 4000);
						$('#'+project_id).fadeOut();
					},
					error: function(response){
						Materialize.toast("Nastala chyba při smazání souboru", 4000);
						console.log('Chyba: '+JSON.stringify(response));
					},
				});
			});*/

	});	
     	</script>
     	<title> DocMe! </title>

 	</head>
 	<body>
 		<div class="page">
 			<?php 
 				include_once "resources/includes/nav.php";
 				include_once "../resources/includes/msg.php";
	 		?>
	 		<div class="row">
	 			<h1 class="center"> Vaše projekty </h1>
	 		</div>
		    <div class="project">
		    	<?php 
		    		include_once "objects/project.php";

		    		$project = new PROJECT($db);
		    		$showProjects = $project->showByUserID($_SESSION["user_session"]);
		    		if(!empty($showProjects)){
		    			echo '<div class="row">'; //Div určující řádku, pokud by byl ve Foreach, každý projekt by se zobrazil na nové řádce (<br />)
			    		foreach($showProjects as $data){ //Výpis a seřazení všech projektů přiřazených k dannému UserID
			    			$modal_id = "#modal_". $data['ID'] .""; //unikátní modal ID
							$modal_id_d = "modal_". $data['ID']. ""; //unikátní modal ID pro samotný div (id)

							//Výpis všech projektů uživatele ve formě karet (card) s možnostmi jako je delete, edit etc
			    			echo '
			    			<a class="modal-trigger" href="core.php?action=openEditor&ID='.$data["ID"].'&uID='.$_SESSION["user_session"].'">
							    <div class="col s6 m3">
							      <div class="card text-overflow hoverable">
							        <div class="card-image">
							          <img src="resources/images/test.jpg">
							          <span class="card-title card-shadow">'. $data["Name"] .' - <i> '. $data["Type"] .'</i> </span>

							          <a href="core.php?action=deleteProject&ID='.$data["ID"].'" id="'.$data["ID"].'" class="btn-floating halfway-fab waves-effect waves-light red delete tooltipped" data-position="bottom" data-delay="20" data-tooltip="Smazat projekt"><i class="material-icons">delete_forever</i></a>
							          <a href="'.$modal_id.'" class="btn-floating halfway-fab waves-effect waves-light red modal-trigger edit tooltipped" data-position="bottom" data-delay="20" data-tooltip="Upravit detaily projektu" style="margin-right:50px;"><i class="material-icons">edit</i></a>
							          <a href="#downloadModal" class="modal-trigger btn-floating halfway-fab waves-effect waves-light red download tooltipped" data-position="bottom" data-delay="20" data-tooltip="Stáhnout projekt" style="margin-right:100px;"><i class="material-icons">file_download</i></a>
							        </div>
							        <div class="card-content">
							          <p> '. $data["Description"] .'</p>
							        </div>
							      </div>
							    </div>
							</a>
			    			<div id="'.$modal_id_d.'" class="modal modal-fixed-footer">
							    <div class="modal-content">
							      <h4>Upravit projekt</h4>
							      	    <div class="row">
										      <form class="col s12" method="POST" action="core.php?action=editProject">
											      <div class="row">
											        <div class="input-field col s6">
											          <input name="name" id="name" type="text" class="validate" placeholder="'. $data["Name"] .'"/>
											          <label for="name">Název</label>
											        </div>
												  </div>
											      <div class="row"> 
											        <div class="input-field col s6">
											          <input name="description" id="description" type="text" class="validate" placeholder="'. $data["Description"] .'"/>
											          <label for="description">Popisek</label>
											        </div>
											      </div>
											      <div class="row">
											      	<input id="ID" name="ID" type="hidden" value="'. $data["ID"] .'"/>
	
											      </div>
										    </div>
										</div>
										<div class="modal-footer">
										<button id="submit" class="btn waves-effect waves-light right" type="submit" name="action"> Upravit projekt </button>
										    <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ">Zrušit</a>
										</div>
									</form>
								</div>
			    			';
			    		}
			    	}
			    		//Poslední možnost (tvorba nového projektu)
			    		echo ' 
			    		<a class="modal-trigger" href="#modal_create">
			    			<div class="row">
							    <div class="col s6 m3">
							      <div class="card hoverable">
							        <div class="card-image">
							          <img src="resources/images/test.jpg">
							        </div>
							        <div class="card-content">
								    	<p> Vytvořit nový projekt </p>
								    </div>
							      </div>
								</div>
							</a>
						</div> 
					</div>';

		    	?>
		    </div>

			  <!-- MODAL OKNO S VÝBĚREM TYPU PROJEKTU PŘO VYTVOŘENÍ SOUBORU (PRVNÍ KROK) -->
			  <div id="modal_create" class="modal modal-fixed-footer">
			    <div class="modal-content">
			      <h4>Vytvořit nový projekt</h4>
			      <!-- VÝBĚR TYPU PROJEKTU PŘO VYTVOŘENÍ SOUBORU (PRVNÍ KROK) [CV] -->
			      <a id='CV' class="modal-trigger pom" href="#create_form">
			      	    <div class="row">
						    <div class="col s12 m4">
							    <div class="card hoverable">
								    <div class="card-image">
									    <img src="resources/images/test.jpg">
									    <span class="card-title card-shadow">CV - životopis</span>
								    </div>
							    <div class="card-content">
								    <p>Začnete utvářet svůj životopis kliknutím na tuto kartu.</p>
							    </div>
						    </div>
					    </div>
				  </a>

				  <!-- VÝBĚR TYPU PROJEKTU PŘO VYTVOŘENÍ SOUBORU (PRVNÍ KROK) [ANNOUCEMENT]-->
				   <a id="Ann" class="modal-trigger pom" href="#create_form">
						    <div class="col s12 m4">
							    <div class="card hoverable">
								    <div class="card-image">
									    <img src="resources/images/test.jpg">
									    <span class="card-title card-shadow">Oznámení</span>
								    </div>
							    <div class="card-content">
								    <p>Začnete utvářet své oznámení kliknutím na tuto kartu.</p>
							    </div>
						    </div>
					    </div>
				  </a>

				   <!-- VÝBĚR TYPU PROJEKTU PŘO VYTVOŘENÍ SOUBORU (PRVNÍ KROK) [INVITATION] -->
			       <a id="Inv" class="modal-trigger pom" href="#create_form">
						    <div class="col s12 m4">
							    <div class="card hoverable">
								    <div class="card-image">
									    <img src="resources/images/test.jpg">
									    <span class="card-title card-shadow">Pozvánka</span>
								    </div>
							    <div class="card-content">
								    <p>Začnete utvářet svou pozvánku kliknutím na tuto kartu.</p>
							    </div>
						    </div>
					    </div>
				  </a>
				</div>
			</div>
				<div class="modal-footer">
			      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ">Zrušit</a>
			    </div>
			</div>

			<!-- MODAL OKNO S FORMULÁŘEM PŘO VYTVOŘENÍ SOUBORU (FINÁLNÍ KROK) [!CV!] -->
			<div id="create_form" class="modal modal-fixed-footer">
				<form id="formSubmit" class="col s12" method="POST">
			    <div class="modal-content">
			      <h4>Vytvořit nový projekt</h4>
		      	    <div class="row">
					      <div class="row">
					        <div class="input-field col s6">
					          <input name="name" id="name" type="text" class="validate">
					          <label for="name">Název</label>
					        </div>
						  </div>
					      <div class="row"> 
					        <div class="input-field col s6">
					          <input name="description" id="description" type="text" class="validate">
					          <label for="description">Popisek</label>
					        </div>
					      </div>
					    </div>
					</div>
					<div class="modal-footer">
					    <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ">Zrušit</a>
					    <button id="submit" class="btn waves-effect waves-light right modal-action" type="submit" name="action"> Založit nový projekt </button>
					</div>
				</form>
				</div>

			    			<div id="downloadModal" class="modal modal-fixed-footer">
							    <div class="modal-content">
							      <h3 class="center" style="padding-top: 2vh">Stáhnout projekt</h3>
						      	    <div class="row" style="padding-top: 10vh">
								      <div class="row">
								        <div class="col s6 center">
								         <?php echo '<a href="http://pdfy.net/api/GeneratePDF.aspx?url=https://student.sps-prosek.cz/~kocvaja14/maturita/admin/editor/editor.php?key='. $data["Dkey"] .'"> <img src="resources/images/test.jpg" style="max-width: 80%"> </a>' ?>
								        </div>
								        <div class="col s6 center">
								          <img src="resources/images/test.jpg" style="max-width: 80%">
								        </div>
								      </div>
								      <div class="row">
								      	<div class="col s6 center">
								      		<?php echo '<a href="http://pdfy.net/api/GeneratePDF.aspx?url=https://student.sps-prosek.cz/~kocvaja14/maturita/admin/editor/editor.php?key='. $data["Dkey"] .'" class="btn waves-effect waves-light"> Stáhnout jako PDF </a>'; ?>
								      	</div>
								      	<div class="col s6 center">
								      		<?php echo '<a href="#" class="btn waves-effect waves-light"> Stáhnout jako JPG </a>'; ?>
								      	</div>
								      </div>
							    </div>
							</div>
							<div class="modal-footer">
							    <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ">Zrušit</a>
							</div>
						</div>

				

				<!-- Button (pravý dolní roh) pro vytvoření nového projektu -->
				<div class="fixed-action-btn vertical">
				    <a class="btn-floating btn-large red tooltipped" data-position="left" data-delay="30" data-tooltip="Vytvořit nový projekt">
				      <i class="large material-icons">mode_edit</i>
				    </a>
				    <ul>
				      <li><a class="btn-floating red modal-trigger pom tooltipped" data-position="left" data-delay="30" data-tooltip="Pozvánka" id="Inv" href="#create_form"><i class="material-icons">insert_chart</i></a></li>
				      <li><a class="btn-floating yellow darken-1 modal-trigger pom tooltipped" data-position="left" data-delay="30" data-tooltip="Oznámení" id="Ann" href="#create_form"><i class="material-icons">format_quote</i></a></li>
				      <li><a class="btn-floating green modal-trigger pom tooltipped" data-position="left" data-delay="30" data-tooltip="CV / Životopis" id="CV" href="#create_form"><i class="material-icons">publish</i></a></li>
				    </ul>
				  </div>
			</div>
		</div>
		<script>
			$(document.body).on('click', '.pom', function (e) {
		 	project_type = jQuery(this).attr("id");
		 	if(project_type == "CV"){
		 		$('#formSubmit').attr('action', 'core.php?action=createProject&type=CV&theme=1'); //Zapsání parametru "action", konkrétně "action=core.php?action=createProject&type=[TYPE]&theme=[THEME]"
		 		$('#formSubmit h4').text("Vytvořit nový životopis");
		 	}else if(project_type == "Ann"){
		 		$("#formSubmit").attr('action', 'core.php?action=createProject&type=Oznámení&theme=1');
		 		$('#formSubmit h4').text("Vytvořit nové oznámení");

		 	}else if(project_type == "Inv"){
		 		$("#formSubmit").attr('action', 'core.php?action=createProject&type=Pozvánka&theme=1');
		 		$('#formSubmit h4').text("Vytvořit novou pozvánku");

		 	}
		 	
		 });
		</script>
 	</body>
 </html>