<?php
	session_start();    
	require_once "../admin/config.php";

    if(!empty($_SESSION["user_session"])){
        header("Location: ../admin/index.php");
    }else{
    	unset($_SESSION["user_session"]);
    }
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<meta author="Jan Kočvara" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
     	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
     	<link rel="stylesheet" type="text/css" href="../resources/styles/style.css" />
     	<script src="https://apis.google.com/js/platform.js" async defer></script>
     	<meta name="google-signin-client_id" content="864532971569-r15luhunka88mfp4m2qld9k0uc89smcr.apps.googleusercontent.com"> 
     	<script> 
     		//Přihlášení pomocí Google Účtu
     	 	function onSignIn(googleUser) {
			  var profile = googleUser.getBasicProfile();

			 var s = "";
			 for(var p in profile){
			 	s += p + " = " + profile[p] + "\n";
			 }	
			 // var googleID = profile.getAuthResponse().id_token; //Google ID uživatele
			  var googleName = profile.getName(); //Google Jméno uživatele (Firstname + Surname)
			  var googleEmail = profile.getEmail(); // Google Email uživatele 
			//  var googlePicture = profile.getImageUrl(); //Google Profile picture (link) uživatele
			//  var googleEmailVer = profile.getEmail

			  $.ajax({
			  	type: "POST",
			  	url: "../admin/core.php?action=registrate_google",
			  	dataType: 'text',
			  	//contentType: 'text',
			  	data: {
			  		//ID: googleID,
			  		name: profile.getName(),
			  		email: profile.getEmail(),
			  		//gender: googleGender,
			  		//file: googlePicture,
			  	},
			  	success: function(response){
			  		window.location.replace("../admin/index.php");

			  	},
			  	error: function(){
			  		alert("Nastala kritická chyba při pokusu k připojení ke Google API: " + response);
			  	}
			  });

			  }

			  //Funkce pro odhlášení z Google účtu
			    function signOut() {
				    var auth2 = gapi.auth2.getAuthInstance();
				    auth2.signOut().then(function () {
				      console.log('User signed out.');
				    });
				  }
        </script>       
		<title> DocMe! </title>
	</head>
	<body>
		<div class="page">
			<?php
		      /* include_once PATH."resources/includes/msg.php";
		       include_once "C://Program Files (x64)/Ampps/www/DocMe/resoucres/includes/nav_pages.php";
		       include "http://localhost/docme/admin/core.php";*/
		       include_once "../resources/includes/msg.php";
		   	   include_once "../resources/includes/nav_pages.php"; //CHYBA!
			?>		
			<div class="content">
				<h2 class="text-center"> Přihlášení k účtu </h2>
			 	<div class="row">
			    	<form class="col s12" method="POST" action="../admin/core.php?action=login">
		      			<div class="row">
					        <div class="input-field col s6 offset-s3">
				        	   <input name="email" id="email" type="email" class="validate">
					           <label for="email" data-error="Invalid E-mail format.">Zadejte váš Email</label>
					        </div>
					    </div>
				        <div class="row">
					        <div class="input-field col s6 offset-s3">
					        	<input name="password" id="password" type="password" class="validate" minlength="3">
				          		<label for="password" data-error="Minimal length are 10 symbols.">Zadejte vaše heslo</label>
					        </div>
				        </div>
				        <div class="row">
				        	<div class="col s4 offset-s5">
						     	<div class="row">
							     	<div id="g-login" class="g-signin2 right" data-onsuccess="onSignIn"> </div>
							     	<span class="right" style="padding: 5px 10px"> nebo </span>
						        	<button class="btn waves-effect waves-light right" type="submit" name="action" value="login"> Přihlásit se </button>
					        	</div>
							    <div class="row right">
						            <a href="send_recovery.php" class="a" title=""> Zapomenuté heslo </a>
						            <br/>
					            	<a href="registration.php" class="a" title=""> Vytvořit nový účet </a>
					            </div>       
				           <!-- FB LOGIN BUTTON   
				           	<div class="fb-login-button" data-width="40" data-max-rows="1" data-size="large" data-button-type="login_with" data-show-faces="false" data-auto-logout-link="false" data-use-continue-as="true"></div>
				           <div id="fb-root"></div>
								<script>(function(d, s, id) {
								  var js, fjs = d.getElementsByTagName(s)[0];
								  if (d.getElementById(id)) return;
								  js = d.createElement(s); js.id = id;
								  js.src = 'https://connect.facebook.net/cs_CZ/sdk.js#xfbml=1&version=v2.11&appId=142798999697789';
								  fjs.parentNode.insertBefore(js, fjs);
								}(document, 'script', 'facebook-jssdk'));
							</script> -->
				        </div>
				    </div>
			   		</form>
			   	</div>
				</div>
			    <?php
					if(isset($_POST["action"])){
						if(!empty($_POST["password"]) && ($_POST["email"])){
							unset($_SESSION["user_session"]);
	                        unset($_SESSION["username"]);
						}else{
							echo "<script> Materialize.toast('Nevyplnili jste všechny pole!', '4000') </script>";
						}
					}
					echo '</div>';
				//include_once "../resources/scripts/fb_login.js";
				//include_once "../resources/includes/footer.php";
			?>
            </div>
        </div>
	</body>
</html> 