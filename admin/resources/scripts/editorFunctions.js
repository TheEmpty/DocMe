	$(document).ready(function () {
 		$(".button-collapse").sideNav();
 		var type = "<?php echo $editor->call('Type') ?>";
 		var theme = "<?php echo $editor->call('Theme') ?>";
 		$('head').append('<link id="'+ type+theme +'" rel="stylesheet" href="../resources/styles/editor_templates/'+type+'_'+theme+'.css" type="text/css" />'); //Funkce pro nahrání defaultního CSS template souboru, který je zaznamenán v databázi

 		//Funkce pro dynamickou změnu načteného CSS souboru, původní se smaže a načte se nový, požadovaný
 		$(document.body).on('click', '.loadCSS', function (e) {
 			e.preventDefault();
 			var buttonTheme = jQuery(this).val(); //Získání value hodnoty z bttonu
 			$('#'+type+theme).remove(); //Smazání <link> na původní CSS file
 			$('#'+type+buttonTheme).remove(); //Smazání <link> CSS file, který mohl být vygenerován dříve tímto buttonem
 			$('head').append('<link id="'+ type+buttonTheme +'" rel="stylesheet" href="../resources/styles/editor_templates/'+type+'_'+buttonTheme+'.css" type="text/css" />'); //Přidání požadovaného CSS file pomocí <link>
 			 });

 			//Jakmile se bude ukládat (save) tento dokument, do skrytého inputu se vloží aktuálně používaný template a aktualizuje se v databázi, pokud se změnil od minula (pokud se používá jiný)

 	});