<?php
	error_reporting();
	require_once "../admin/config.php";
    require_once "../admin/objects/article.php";
    $article = new ARTICLE($db);
	session_start();
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="utf-8" />
		<meta author="Jan Kočvara" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
     	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
     	<link rel="stylesheet" type="text/css" href="../resources/styles/style.css" />
     	<script> $(".button-collapse").sideNav(); </script>

         
		<title> DocMe! </title>
	</head>
	<body>
		<div class="page">
			<!-- Include navigačního menu + zpráv -->
			<?php include_once "../resources/includes/nav_pages.php";
				  include_once "../resources/includes/msg.php";
		    ?>
			<div class="content">
				<div class="promo-box center">
                    <?php 
                        $allArticles = $article->showByStatus("Publikováno", "Blog");
                        foreach($allArticles as $data){
                            echo 
                                '<div class="row">
                                     <div class="col s6 m12 z-depth-4 center">
                                        <h4 class="card-title left title"> '. $data["Name"] .' </h4>
                                       	<span class="right date"> '. $data["Created"] .' </span>
                                        <h5 class="left description"> '. $data["Description"] .' </h5>
                                        <div class="card-content content">
                                            <p> '. $data["Content"] .' </p>
                                        </div>
                                      </div>
                                 </div>
                        ';
                        }
                    ?>
				</div>
                <ul class="pagination">
                    <li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
                    <li class="active"><a href="#!">1</a></li>
                    <li class="waves-effect"><a href="#!">2</a></li>
                    <li class="waves-effect"><a href="#!">3</a></li>
                    <li class="waves-effect"><a href="#!">4</a></li>
                    <li class="waves-effect"><a href="#!">5</a></li>
                    <li class="waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
                </ul>
			</div>
			<?php include_once "../resources/includes/footer.php"; ?>	
		</div>
	</body>
</html> 