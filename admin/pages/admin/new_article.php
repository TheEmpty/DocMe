<?php
	session_start();
?>
<!DOCTYPE HTML>
 <html>
 	<head>
 		<meta charset="utf-8" />
		<meta author="Jan Kočvara" />

		<link rel="stylesheet" type="text/css" href="resources/styles/dd.css"/>

		<link rel="stylesheet" type="text/css" href="resources/styles/style.css" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css"/>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
	    <script src="js/interact.js"> </script>
	    <script src="js/basic.js"> </script>
     	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>
     	<script>
		
     	$(document).ready(function () {
		    $('select').material_select();
     		$(".button-collapse").sideNav();
  			$('#textarea1').trigger('autoresize');
		 });
     	</script>
     	<title> DocMe! </title>

 	</head>
 	<body>
 		<div class="page">
 			<?php 
 				include_once "../../resources/includes/nav_sub.php";
 				include_once "../../resources/includes/admin_menu_sub.php";
 				include_once "../../../resources/includes/msg.php";
	 		?>
	 		<h1 class="center"> Nový příspěvek </h1>
	 		 <div class="row">
			    <form class="col s12" method="POST">
			      <div class="row">
			        <div class="input-field col s6">
			          <input name="name" id="first_name" type="text" class="validate">
			          <label for="first_name">Název</label>
			        </div>
			        <div class="input-field col s6">
			          <input name="description" id="last_name" type="text" class="validate">
			          <label for="last_name">Popisek</label>
			        </div>
			      </div>
			      <div class="row">
			        <div class="input-field col s12">
        	          <textarea name="content" id="textarea1" class="materialize-textarea validate" ></textarea>
					  <label for="textarea1">Obsah</label>
			        </div>
			      </div>
			      <div class="row">
				      <div class="input-field col s6">
					    <select name="type">
					      <option value="" disabled selected>Zvolte typ příspěvku</option>
					      <option value="Oznámení"> Oznámení </option>
                          <option value="Blog"> Blog </option>
					      <option value="Log">Update log</option>
					    </select>
					    <label>Typ příspěvku</label>
					  </div>	
				       <div class="input-field col s6">
					    <select name="status">
					      <option value="" disabled selected>Zvolte stav příspěvku</option>
					      <option value="Publikováno">Publikovat</option>
					      <option value="Koncept">Koncept</option>
					      <option value="Na schválení">Na schválení</option>
					    </select>
					    <label>Stav příspěvku</label>
					  </div>  
				  </div>   
				  <div class="row">
				  	<button class="btn waves-effect waves-light right" type="submit" name="action"> Vytvořit příspěvek </button>
			      </div>
			    </form>
  			</div>
  			<?php
  				if(isset($_POST["action"])){
  					if(!empty($_POST["name"]) && ($_POST["description"]) && ($_POST["content"]) && ($_POST["type"]) && ($_POST["status"])){
  						$pom_status = explode("_", $_POST["status"]);
  						$pom_type = explode("_", $_POST["type"]);
  						$type = $pom_type[0];
  						$status = $pom_status[0];
  						$name = $_POST["name"];
  						$description = $_POST["description"];
  						$content = $_POST["content"];

  						include_once "../../resources/scripts/pdo.php";
  						include_once "../../objects/article.php";

  						$article = new ARTICLE($db);
  						$article->createArticle($_SESSION["user_session"], $name, $description, $content, $type, $status);
	  				}else{
	  					unset($_SESSION["error"]);
	  					$_SESSION["error"] = "Nevyplnili jste všechny potřebné údaje!";
	  				}
  				}else{
  					unset($_SESSION["error"]);
  				}				
  			?>
	 	</div>
	 </body>
</html>