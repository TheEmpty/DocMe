<?php
	session_start();
	require_once "../../resources/scripts/pdo.php";
	require_once "../../objects/user.php";
	$user = new USER($db);
	$user->is_loggedin();
?>
<!DOCTYPE HTML>
 <html>
 	<head>
 		<meta charset="utf-8" />
		<meta author="Jan Kočvara" />

		<link rel="stylesheet" type="text/css" href="resources/styles/dd.css"/>

		<link rel="stylesheet" type="text/css" href="resources/styles/style.css" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css"/>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
	    <script src="js/interact.js"> </script>
	    <script src="js/basic.js"> </script>
     	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>
     	<script>
     	$(document).ready(function () {
     		$(".button-collapse").sideNav();
     	});
     	</script>
     	<title> DocMe! </title>

 	</head>
 	<body>
 		<div class="page">
 			<?php 
 				include_once "../../resources/includes/nav_sub.php";
 				include_once "../../resources/includes/admin_menu_sub.php";
 				include_once "../../../resources/includes/msg.php";

 				echo "<h1 class='center'> Přehled uživatelů </h1>";

				$allUsers = $user->showAll();

	 			echo "
		    			<table class='highlight'>
		    				<thead>
		    					<tr>
		    						<th> ID </th>
		    						<th> Křestní jméno </th>
		    						<th> Příjmení </th>
		    						<th> Email </th>
		    						<th> Oprávnění</th>
		    						<th> Vytvořen </th>
		    						<th> Naposled přihlášen </th>
		    						<th> IP (registrace) </th>
		    						<th> IP (poslední) </th>
		    					</tr>
		    				</thead>
		    				<tbody>";

	 			foreach($allUsers as $data){
	 				if($data['Level'] < 1){ $lvl = "Uživatel"; }elseif($data['Level'] == 1){ $lvl = "Moderátor"; }elseif($data['Level'] > 1){ $lvl = "Administrátor";}
	 				$metaUser = $user->showAllMeta($data["ID"]);


	 				echo "
						<tr>
    						<td>". $data['ID'] ."</td>
    						<td>". $data['Firstname'] ."</td>
    						<td>". $data['Surname'] ."</td>
    						<td>". $data['Email'] ."</td>
    						<td>". $lvl . "</td>
    						";
		    		foreach($metaUser as $metadata){
		    			echo "
    						<td>". $metadata['Created'] ." </td>
    						<td>". $metadata['LastLogin'] ."</td>
    						<td>". $metadata['CreatedIP']. "</td>
    						<td>". $metadata['LastIP']. "</td>	
    						<td> <a href='edit_user.php?id=". $metadata['UsersID']."' title='Editovat zápis'><i class='material-icons'>mode_edit</i></a></td>
    						<td> <a href='../../core.php?action=deleteUser&ID=". $metadata['UsersID'] ."' title='Smazat zápis'><i class='material-icons'>clear</i></a></td>
	    				</tr>
	 					";
	 				}
	 			

		    	
	 		}
	 		echo "</tbody>
	 				</table>
	 		";
	 		
	 		?>

		  
	 	</div>
	 </body>
</html>