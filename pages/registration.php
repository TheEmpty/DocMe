<?php
	session_start();
	//$configs = include "../admin/config.php";
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="utf-8" />
		<meta author="Jan Kočvara" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
     	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
     	<link rel="stylesheet" type="text/css" href="../resources/styles/style.css" />
     	<script> $(".button-collapse").sideNav(); </script>
         
		<title> DocMe! </title>
	</head>
	<body>
		<div class="page">
			 <?php  include "../resources/includes/nav_pages.php";
					include "../resources/includes/msg.php";
			 ?>			
			<div class="content">
			<h2 class="text-center"> Vytvoření nového účet </h2>
			 	<div class="row">
			    	<form class="col s12" method="POST">
	      				<div class="row">
					        <div class="input-field col s3 offset-s3">
						        <input name="first_name" id="first_name" type="text" class="validate" minlength="3">
						        <label for="first_name" data-error="Minimální délka jsou 3 symboly">Vaše křestní jméno</label>
					        </div>
					        <div class="input-field col s3">
						        <input name="last_name" id="last_name" type="text" class="validate" minlength="2">
						        <label for="last_name" data-error="Minimální délka jsou 2 symboly">Vaše příjmení</label>
					        </div>
					    </div>
				      	<div class="row">
					        <div class="input-field col s6 offset-s3">
						        <input name="password" id="password" type="password" class="validate" minlength="5">
						        <label for="password" data-error="Heslo musí být minimálně 5 znaků dlouhé">Vymyslete heslo</label>
					        </div>
					    </div>
					    <div class="row">
					        <div class="input-field col s6 offset-s3">
						        <input name="email" id="email" type="email" class="validate">
						        <label for="email" data-error="Napište prosím email v tomto tvaru: vasemail@domena.cz">Váš email</label>
					        </div>
					    </div>
					    <div class="row">
					    	<div class="col s3 offset-s6">
					    		<button class="btn waves-effect waves-light right" type="submit" name="action"> Vytvořit účet </button>
					    	</div>
					    </div>
					</form>
				</div>
				<?php
					if(isset($_POST["action"])){
						if(isset($_POST["first_name"]) && ($_POST["last_name"]) && ($_POST["password"]) && ($_POST["email"])){
							include_once "../resources/scripts/pdo.php";
							require_once "../admin/objects/user.php";
							$user = new USER($db);
							$user->register($_POST["first_name"], $_POST["last_name"], $_POST["password"], $_POST["email"], "0");
						}else{
							//echo "<p> Nevyplnili jste všechny pole! </p>";
						}
					}
				?>
			</div>
			<?php include "../resources/includes/footer.php" ?>
		</div>
	</body>
</html> 