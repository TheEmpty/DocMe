<?php

	class LOGGED_USER
	{
		private $DB;
		private $ID;
		private $Firstname;
		private $Surname;
		private $DisplayName;
		private $Gender;
		private $Birth;
		private $Email;
		private $Level;
		private $FolderPath;
		private $LastLogin;
		private $LastIP;
		private $LevelWord;
		private $ProfilePicture;
		private $ProfilePictureID;

		function __construct($DB_con, $ID, $Fname, $Srname, $Email, $Level, $FolderPath, $LastLogin, $LastIP, $Gender, $Birth, $DisplayName, $ProfilePicture, $ProfilePictureID){

			$BirthDay = strtotime($Birth);

			$this->DB = $DB_con;
			$this->ID = $ID;
			$this->Firstname = $Fname;
			$this->Surname = $Srname;
			$this->Gender = $Gender;
			$this->Birth = date("d-m-Y", $BirthDay);
			$this->Email = $Email;
			$this->Level = $Level;
			$this->FolderPath = $FolderPath;
			$this->LastLogin = $LastLogin;
			$this->LastIP = $LastIP;	
			$this->LevelWord = $this->sayLevel($Level);
			$this->DisplayName = $DisplayName;
			$this->ProfilePicture = $ProfilePicture;
			$this->ProfilePictureID = $ProfilePictureID;
		}

		//Funkce pro zobrazení danné vlastnosti
	 	public function show($atribut){
			return $this->$atribut;
		}

	/*	//Funkce pro editaci uživatelského profilu
	    public function edit($username, $email, $password, $gender, $born){
	    	//if($password != ""){
	    		$password = hash("SHA256", $password);
	    	
		  	$sql = $this->DB->prepare("UPDATE Users SET DisplayName = COALESCE(NULLIF(:username,''),DisplayName), Birth = COALESCE(NULLIF(:birth, ''),Birth), Gender = COALESCE(NULLIF(:gender, ''),Gender), Email = COALESCE(NULLIF(:email, ''),Email), Password = COALESCE(NULLIF(:password, ''),Password) WHERE ID = :id LIMIT 1");
		  	$sql->execute(array(":username" => $username, ":birth" => $birth, ":gender" => $gender, ":email" => $email, ":password" => $password, ":id" => $this->ID));

	   	}
*/

	   	//Funkce pro vypsání existujících textových šablon spojených s tímto profilem
	   	public function showUserThemes(){
	   		$sql = $this->DB->prepare("SELECT * FROM UsersThemes WHERE UsersID = :id");
	   		$sql->execute(array(":id" => $this->ID));
	   		return $sql;
	   	}

		private function sayLevel($level){
			if($level < 1){
				return "Uživatel";
			}elseif($level == 1){
				return "Moderátor";
			}elseif($level > 1){
				return "Administrátor";
			}else{
				return "Chyba v oprávnění!";
			}
		}
	}