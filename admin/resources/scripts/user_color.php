<?php
//Nastavení barvy uživatelského NAVU a Buttonů
	if(!empty($_SESSION["user_session"])){
		$sql = "SELECT NavColor, ButtonColor FROM UsersConfig WHERE UsersID = :id";
		$prep = $db->prepare($sql);
		$prep->execute(array(":id" => $_SESSION["user_session"]));
		$result = $prep->fetchAll(PDO::FETCH_ASSOC);
		foreach($result as $data){
			$_SESSION["navColor"] = $data["NavColor"];
			$_SESSION["btnColor"] = $data["ButtonColor"];
		}
	}

	echo "
		<script> 
			$(document).ready(function(){
				$('nav').attr('class', '". $_SESSION['navColor'] ."');
				$('.profile-button').attr('class', 'waves-effect waves-light btn right modal-trigger profile-button ". $_SESSION['btnColor'] ."');
				$('nav').css('visibility', 'visible');
				$('.profile-button').css('visibility','visible');
			});
		</script>
	";

?>