<?php

	class ARTICLE
	{
      	// Interní data o uživateli
		public $UserID;
		public $Name;
		public $Description;
		public $Content;
		public $Type;

		public function show_one(){
			echo $this->User_ID;
			echo $this->Name;
			echo $this->Description;
			echo $this->Content;
			echo $this->Created;
			echo $this->Modified;
			echo $this->Type;

		}

	    private $DB;
	 	
	    function __construct($DB_con)
	    {

	      $this->DB = $DB_con;	    
	    }

	    //Funkce na vypsání veškerých příspěvků v DB
	    public function viewArticleTable ($filter){
	    	if(empty($filter_pom)){
	    	 	$sql = $this->DB->prepare("SELECT * FROM Articles ORDER BY ID DESC");
		    	$sql->execute();
		    	$sqlResult = $sql->fetchAll(PDO::FETCH_ASSOC);
		    	
		    	return $sqlResult; 
	    	 }else{
	    	  try{
	    	  	$sqlFilter = $this->DB->prepare("SELECT * FROM Articles ORDER BY :filter DESC");
				$sqlFilter->execute(array(":filter" => $filter));
				$sqlFilterResult = $sqlFilter->fetchAll(PDO::FETCH_ASSOC);
				return $sqlFilterResult;
	    	    }catch(PDOException $e){
					$e->getMessage();
					return "Nebylo možné nastolit spojení s databázovým serverem, zkuste to prosím později.";
	    	    }
	    	 }
	    	try{
		       	$sql = $this->DB->prepare("SELECT * FROM Articles ORDER BY ID DESC");
		    	$sql->execute();
		    	$sqlResult = $sql->fetchAll(PDO::FETCH_ASSOC);
		    	
		    	return $sqlResult;
		    }catch(PDOException $e){
		    	$e->getMessage();
		    }
		}

		public function showByType($type){
			try{
                $sql = $this->DB->prepare("SELECT * FROM Articles WHERE Type = :type ORDER BY ID DESC");
                $sql->execute(array(":type" => $type));
                $sqlResult = $sql->fetchAll(PDO::FETCH_ASSOC);
                return $sqlResult;
            }catch(PDOException $e){
                $e->getMessage();
                return "Nebylo možné nastolit spojení s databázovým serverem, zkuste to prosím později.";
            }
		}
        
        public function showByStatus($status, $type){
            try{
                $sql = $this->DB->prepare("SELECT * FROM Articles WHERE Status = :status && Type = :type ORDER BY ID DESC");
                $sql->execute(array(":status" => $status, ":type" => $type));
                $sqlResult = $sql->fetchAll(PDO::FETCH_ASSOC);
                return $sqlResult;
            }catch(PDOException $e){
                $e->getMessage();
                return "Nebylo možné nastolit spojení s databázovým serverem, zkuste to prosím později.";
            }
        }

	    //Funkce na zapsání jednotlivých příspěvků
	    public function createArticle ($user_id, $name, $description, $content, $type, $status){
	    	try{
		    	$created = date("Y-m-d H:i:s");
		    	$modified = date("Y-m-d H:i:s");

		    	$sql = $this->DB->prepare("INSERT INTO Articles(User_ID, Name, Description, Content, Type, Created, Modified, Status) VALUES (:user_id, :name, :description, :content, :type, :created, :modified, :status)");
		    	$sql->bindparam(":user_id", $user_id);
		    	$sql->bindparam(":name", $name);
		    	$sql->bindparam(":description", $description);
		    	$sql->bindparam(":content", $content);
		    	$sql->bindparam(":type", $type);
		    	$sql->bindparam(":created", $created);
		    	$sql->bindparam(":modified", $modified);
		    	$sql->bindparam(":status", $status);
		    	$sql->execute();

		    	unset($_SESSION["message"]);
		    	$_SESSION["message"] = "Příspěvek byl úspěšně vytvořen!";

		    	return $_SESSION["message"];
	    }catch(PDOException $e) {
	    	echo $e->getMessage();
	    }	
	}

	public function editArticle($id, $name, $description, $content, $type, $status){
		try{
			if(!empty($name) && ($description) && ($content) && ($type) && ($status)){
			/*	$sql = $this->DB->prepare("UPDATE Articles SET Name = :name, Description = :description, Content = :content, Type = :type, Modified = :modified, Status = :status WHERE ID = :id");
				$sql->execute(array(":name" => $name, ":description" => $description, ":content" => $content, ":type" => $type, ":modified" => $modified, ":status" => $status, ":id" => $id));*/
			}elseif(!empty($name) && ($description) && ($status) && ($type)){

			}elseif(!empty($name) && ($description) && ($type) && ($content)){

			}elseif(!empty($name) && ($description) && ($status) && ($content)){

			}elseif(!empty($name) && ($status) && ($content) && ($type)){

			}elseif(!empty($name) && ($type) && ($status)){

			}elseif(!empty($name) && ($status)){

			}elseif(!empty($name) && ($type)){

			}elseif(!empty($name)){

			}elseif(!empty($description) && ($type) && ($status)){

			}elseif(!empty($description) && ($type)){

			}elseif(!empty($description) && ($status)){

			}elseif(!empty($description)){

			}elseif(!empty($type) && ($status)){

			}elseif(!empty($type)){

			}elseif(!empty($status)){

			}
		}catch(PDOException $e){
			echo $e->getMessage();
		}
	}

	public function showDetailID($id){
		try{
			$sql = $this->DB->prepare("SELECT * FROM Articles WHERE ID = :id LIMIT 1");
			$sql->execute(array(":id" => $id));
			$sqlResult = $sql->fetchAll(PDO::FETCH_ASSOC);
			return $sqlResult;
		}catch(PDOException $e){
			$e->getMessage();
		}
	}

	public function deleteArticle($id){
		try{
			$sql = $this->DB->prepare("DELETE FROM Articles WHERE ID = :id LIMIT 1");
			$sql->execute(array(":id" => $id));
			unset($_SESSION["message"]);
			$_SESSION["message"] = "Záznam úspěšně smazán! [DONE]";

			return $_SESSION["message"];
		}catch(PDOException $e){
			return $e->getMessage();
		}
	}
}