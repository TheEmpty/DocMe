// JAVASCRIPT (jQuery)

    $(document).bind("contextmenu", function (event) {
        //Kontrola, jestli uživatel kliká na obrázek s classou userPicsCollection
        if( !!$('.userPicsCollection').filter(function() { return $(this).is(":hover") }).length){
            // Deaktivuj původní (defaultní) context-menu
            event.preventDefault();
            
            // Zobrazit context-menu
            $(".profile-custom-menu").finish().toggle(100).
            
            //Napravo od kurzoru (zobraz context-menu)
            css({
                top: event.pageY + "px",
                left: event.pageX + "px"
            });
        }

        //Skript pro Editor (editor.php)
        if( !!$('.moovable').filter(function() { return $(this).is(":hover") }).length){
            event.preventDefault();

            $(".editor-custom-menu").finish().toggle(100).

            css({
                top: event.pageY + "px",
                left: event.pageX + "px"
            });
        }

            
        if( !!$('div[class=image]').filter(function() { return $(this).is(":hover") }).length){
            event.preventDefault();

            $(".editor-custom-image-menu").finish().toggle(100).

            css({
                top: event.pageY + "px",
                left: event.pageX + "px"
            });
        }
    });


    // Pokud uživatel klikne mimo context-menu
    $(document).bind("mousedown", function (e) {
            //Profile.php
        if (!$(e.target).parents(".profile-custom-menu").length > 0) {
            $(".profile-custom-menu").hide(100); //skryj menu
        }

            //Editor
        if (!$(e.target).parents(".editor-custom-menu").length > 0){
            $(".editor-custom-menu").hide(100);
        }

            //default_user_index.php
        if (!$(e.target).parents(".editor-custom-image-menu").length > 0){
            $(".editor-custom-image-menu").hide(100);
        }


    });


    $(".profile-custom-menu li").click(function(){  
        //Schovat po kliknutí na akci
        $(".profile-custom-menu").hide(100);
      });

    $(".editor-custom-menu li").click(function(){
        //schovat po kliknutí na akci
        $(".editor-custom-menu").hide(100);
    });

    $(".editor-custom-image-menu li").click(function(){
        //schovat po kliknutí na akci
        $(".user-index-custom-menu").hide(100);
    });

//});