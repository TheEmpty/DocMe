<?php

	CLASS EDITOR
	{
		private $DB;
		private $pID;
		private $uID;
		private $Name;
		private $Path;
		private $Type;
		private $Theme;

		function __construct($DB_con, $projectID, $userID, $projectName, $path, $type, $theme){
			$this->DB = $DB_con;
			$this->pID = $projectID;
			$this->uID = $userID;
			$this->Name = $projectName;
			$this->Path = "../users/".$path;
			$this->Type = preg_replace('~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml|caron);~i', '$1', htmlentities($type, ENT_COMPAT, 'UTF-8')); //Odebrání diakritiky
			$this->Theme = $theme;
		}

		//Funkce pro zobrazení jednotlivých dat uvnitř objektu
		public function show($atribut){
			return mb_strtolower($this->$atribut);
		}

		//Funkce pro nahrání defaultní šablony (elements.json)
		public function loadDefaultText(){
			$jsonFile = "themes/". $this->Type ."/default/elements.json";	
			if((is_file($jsonFile))){	
				return file_get_contents($jsonFile);
			}else{
				return array("Error" => "Textová konfigurace danných elementů ještě nebyla vytvořena");

			}
		}

		//Funkce pro nahrání defaultní šablony (config.json)
		public function loadDefaultConfig(){
			$jsonFile = "themes/". $this->Type ."/default/config.json";	
			if((is_file($jsonFile))){	
				return file_get_contents($jsonFile);
			}else{
				return array("Error" => "Textová konfigurace danných elementů ještě nebyla vytvořena");

			}
		}

		//Funkce pro nahrání defaultní šablony (custom.json)
		public function loadDefaultCustom(){
			$jsonFile = "themes/". $this->Type ."/default/custom.json";	
			if((is_file($jsonFile))){	
				return file_get_contents($jsonFile);
			}else{
				return array("Error" => "Textová konfigurace danných elementů ještě nebyla vytvořena");

			}
		}

		//Funkce pro kontrolu, jestli se jedná o úplně první spuštění projektu
		public function firstLoad(){
			$elFile = $this->Path."/elements.json";
			$confFile = $this->Path."/config.json";
			if((is_file($elFile)) OR (is_file($confFile))){
				return false;
			}else{
				return true;
			}
		}

		//Funkce pro načtení uloženého textu a vlastností danného elementu
		public function loadText(){
			$jsonFile = $this->Path."/elements.json";
			if(file_exists($jsonFile)){
				$jsonFileData = file_get_contents($jsonFile);
				return $jsonFileData;
			}else{
				return array("Error" => "Textová konfigurace danných elementů ještě nebyla vytvořena");
			}

		}

		//Načtení konfigurace pro projekt (pozadí, globální font, barvy, atd) -- Nastavení, které není přiřazené k uričtému prvku, ale celému projektu
		public function loadConfig(){
			$jsonFile = $this->Path."/config.json";
			if(file_exists($jsonFile)){
				$jsonFileData = file_get_contents($jsonFile);
				return $jsonFileData;
			}else{
				return array("Error" => "Konfigurace nebyla vytvořena");
			}
		}

		//Načtení konfigurace pro projekt (custom prvky)
		public function loadCustom(){
			$jsonFile = $this->Path."/custom.json";
			if(file_exists($jsonFile)){
				$jsonFileData = file_get_contents($jsonFile);
				return $jsonFileData;
			}else{
				return array("Error" => "Konfigurace nebyla vytvořena");
			}
		}

		//Public funkce pro vypsání všech uživatelských textových šablon přímo do editoru
		public function showUserThemes(){
			try{
				$sql = $this->DB->prepare("SELECT * FROM UsersThemes WHERE UsersID = :uID AND Type = :atribut");
				$sql->execute(array(":uID" => $this->uID, ":atribut" => $this->Type));
				$result = $sql->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			}catch(PDOException $e){
				return $e->getMessage();
			}
		}

		private function getUser($id){
			include_once "user.php";
			$user = new USER($this->DB);
			return $user->showByID($id);
		}

		
		//Původní prototyp ukládání / zálohování / načítání projektů
		/*public function load(){
			$file_index = $this->Path."/index.php";
			$file_toConvert = $this->Path."/toConvert.php";
			if(file_exists($file_index)) //Pokud existuje soubor "index.php" v projektu, udělej...
			{
					$this->createIndexBackup(); //Vytvoř zálohu index.php souboru, před jeho používáním
					return include_once $file_index;
					 //Include index.php souboru
			}else{
				if($this->Type == "CV"){
					$this->createNewIndex($file_index);
					return include_once $file_index; //a poté soubor includuj
				}elseif($this->Type == "Oznameni"){
					$this->createNewIndex($file_index);
					return include_once $file_index;
				}elseif($this->Type == "Pozvanka"){
					$this->createNewIndex($file_index);
					return include_once $file_index;
				}
				
			}
		}*/

		/*
		private function createIndexBackup(){
			//$old_index = $this->Path."/old_index.php";
			$file_index = $this->Path."/index.php";
			$this->doBackup($file_index);
		/*	if(file_exists($old_index)){
				unlink($old_index); //Smazání souboru "../old_index.php"
				$backup_index = fopen($old_index, "w"); //Vytvoření nového souboru "../old_index.php"
				$index_content = file_get_contents($file_index); //Získání celého obsahu souboru z adresy $index -> aktuálně aktivní index, za chvíli "old_index.php" -->Záloha
				fwrite($backup_index, $index_content); //Zapsání aktuálního indexu do "old_index.php"
				fclose($backup_index); //Zavření souboru
				return "Záloha prepsána";
			}else{
				$backup_index = fopen($old_index, "w"); //Vytvoření souboru "../old_index.php"
				$index_content = file_get_contents($file_index); //Získání celého obsahu souboru z adresy $index -> aktuálně aktivní index, za chvíli "old_index.php" -->Záloha
				fwrite($backup_index, $index_content); //Zapsání obsahu aktuálního indexu do old_index.php
				fclose($backup_index); //Zavření souboru old_index.php
				return "Záloha nově vytvořena";
			}
			return true;
		}
*/
		/*
		public function save($json){
			//Získej aktuálně upravené data a všechny je přepiš! Ha a jak na to co, debile... JS :P #UžNikdyNedělejEditorKokote --Kočvik z budoucnosti po maturitě
			//Teoreticky je možné udělat tady form_reciever, veškeré data které uživatel uloží (typu jméno, dovednosti atd) se pošlou pomocí tajného POST formu a tady se spracují, uloží do souboru...

			//$this->FillMeUp(); //Zapíše array nebo list hodnot, které se pošlou z editoru
		
		/*	//$obj = json_decode($_POST["keyName"], false);
			$obj = "Toto je můj JSON file: " . $json;
			$logs = fopen('log.txt', 'w');
			file_put_contents($logs, $obj);

			$file_index = $this->Path."/index.php";
			$return = $this->doBackup($file_index);
			$overWritten = $this->createNewIndex($file_index);
			return $json;
		}*/
/*
				private function createNewIndex($filename){
			$file_index = fopen($filename, "w"); //Vytvoření index.php souboru, pokud neexistuje, s oprávněním "WRITE"
			$userInfo = $this->getUser($this->uID);
			foreach($userInfo as $user){
				//Připravení HTML textu, který se vloží do "index.php" souboru
			$txt =" 		
					<div id='project-container'>
						<div id='sortable'>		
							<div id='username' class='moovable'>
								<div id='controlPanel_username' class='hidenPanel'>
									<button class='btn'> You find me! </button>
									<button class='material-icons deleteDiv' value='username'> delete </button>
									<button class='hidePanel btn' value='username'> Schovat panel </button>
								</div>
								<span id='usernameText' class='description editable'> ". $user[Firstname] ." ". $user[Surname] ."</span>
							</div>	
							<div id='image' class='moovable'>
								<div id='controlPanel_image' class='hidenPanel'>
									<button class='btn'> You find me! </button>
									<button class='material-icons deleteDiv' value='image'> delete </button>
									<button class='hidePanel btn' value='image'> Schovat panel </button>
								</div>
								<img id='profilePic' class='responsive-img' src='../resources/images/.jpg'/>
							</div>
							<div id='bio' class='moovable'>
								<div id='controlPanel_bio' class='hidenPanel'>
									<button class='btn'> You find me! </button>
									<button class='material-icons deleteDiv' value='bio'> delete </button>
									<button class='hidePanel btn' value='bio'> Schovat panel </button>
								</div>
								<span id='bioText' class='description editable'> Bio o mě, kdo že to jsem? </span>
							</div>
							<div id='experience' class='moovable'>
								<div id='controlPanel_experience' class='hidenPanel'>
									<button class='btn'> You find me! </button>
									<button class='material-icons deleteDiv transparent' value='experience'> delete </button>
									<button class='hidePanel btn' value='experience'> Schovat panel </button>
								</div>
								<h3 id='experienceTitle' class='editable'> Název firmy </h3>
								<span id='experiencePosition' class='editable'> Pozice ve firmě </span>
								<p id='experienceText' class='description editable'> Můj popisek firmy, co jsem všechno skvělého dělal </p>
							</div>
							<div id='highschool' class='moovable'>
								<div id='controlPanel_highschool' class='hidenPanel'>
									<button class='btn'> You find me! </button>
									<button class='material-icons deleteDiv' value='highschool'> delete </button>
									<button class='hidePanel btn' value='highschool'> Schovat panel </button>
								</div>
								<h3 id='highschoolTitle' class='editable'> Název vysoké školy </h3>
								<span id='highschoolPosition' class='editable'> Titul, kam jsem to dopracoval </span>
								<p id='highschoolText' class='description editable'> Co jsem se vše naučil </p>
							</div>
							<div id='school' class='moovable'>
								<div id='controlPanel_school' class='hidenPanel'>
									<button class='btn'> You find me! </button>
									<button class='material-icons deleteDiv' value='school'> delete </button>
									<button class='hidePanel btn' value='school'> Schovat panel </button>
								</div>
								<h3 id='schoolTitle' class='editable'> Název střední školy </h3>
								<span id='schoolPosition' class='editable'> Kdy jsem získal maturitu, od kdy do kdy jsem studoval </span>
								<p id='schoolText' class='description editable'> Moje úspěchy za dobu školy, co jsem se naučil </h3>
							</div>
							<div id='ability' class='moovable next'>
								<div id='controlPanel_ability' class='hidenPanel'>
									<button class='btn'> You find me! </button>
									<button class='material-icons deleteDiv' value='ability'> delete </button>
									<button class='hidePanel btn' value='ability'> Schovat panel </button>
								</div>
								<span id='abilityTitle' class='editable'> Mé silné stránky </span>
								<p id='abilityText' class='editable'> - Čas věnovaný práci </p>
							</div>
							<div id='lang' class='moovable next'>
								<div id='controlPanel_lang' class='hidenPanel'>
									<button class='btn'> You find me! </button>
									<button class='material-icons deleteDiv' value='lang'> delete </button>
									<button class='hidePanel btn' value='lang'> Schovat panel </button>
								</div>
								<span id='langTitle' class='editable'> Jazyky </span>
								<p id='langText' class='editable'> seznam jazyků které ovládám </p>
							</div>
							<div id='skills' class='moovable next'>
								<div id='controlPanel_skills' class='hidenPanel'>
									<button class='btn'> You find me! </button>
									<button class='material-icons deleteDiv' value='skills'> delete </button>
									<button class='hidePanel btn' value='skills'> Schovat panel </button>
								</div>
								<span id='skillsTitle' class='editable'> Mé schopnosti </span>
								<p id='skillsText' class='editable'> Nějaké ty skillz! :) </p>
							</div>
						</div>
						<div id='sortorder'> </div>
					</div>";

				fwrite($file_index, $txt); //Zapiš do souboru $filename string uložený v proměnné $txt
				fclose($file_index); //Zavři soubor $filename
				return "Hotovo!";
			}
			return "Hotovo!";
		}

		private function makePreview($filename){
			
			$this->doBackup($filename);		
			$file_index = fopen($filename, "w");

			$txt = "	<div class='project_content'> \n";
			$txt .= " 		<div class='input-field col s12 m10'>\n";
			$txt .= "			<h1 class='center'> ". $name ."</h1> \n";
			$txt .= "		</div> \n";
			$txt .= "   	<div class='project_bio'> \n";
			$txt .= "			<p> ". $bio ." </p> \n";
			$txt .= "		</div> \n";
			$txt .= "		<div class='timeline'> \n";
			$txt .= "			\n";
			$txt .= "		</div> \n";
			$txt .= "		<div class='skills'> \n";
			$txt .= "			\n";
			$txt .= "		</div>";
			$txt .= "	</div> \n";

			fwrite($file_index, $txt);
			fclose($file_index);
			return "Uloženo!";
		}

		private function doBackup($file){
			$file_index_content = file_get_contents($file);
			$file_backup = $this->Path."/backup.php";
			file_put_contents($file_backup, $file_index_content);
		/*	if(file_exists($file_backup)){
				unlink($file_backup);
				file_put_contents($file_backup, $file_index_content);
				fclose($file_backup_open);
			}else{
				$file_backup_open = fopen($file_backup, "w");
				fwrite($file_backup_open, $file_index_content);
				fclose($file_backup_open);
			}
			return true;
		}*/

	}

?>