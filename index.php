<?php
	session_start();
	require_once "admin/config.php";
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="utf-8" />
		<meta author="Jan Kočvara" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
     	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
     	<link rel="stylesheet" type="text/css" href="resources/styles/animations.css" />
     	<link rel="stylesheet" type="text/css" href="resources/styles/index_style.css" />
        <script>
     	 $(document).ready(function(){
 	 		 $(".button-collapse").sideNav();
 	 	  });
        </script>
		<title> DocMe! </title>
	</head>
	<body>
		<div class="page col s12 m12 l6 grey darken-3">
			<!-- Include navigačního menu + zpráv -->
			<?php include_once "resources/includes/nav.php";
				  include_once "resources/includes/msg.php";
		    ?>
			<div class="header">
				<div class="filter">
					<div class="banner">
						<h1> DocMe! </h1>
						<h3> Vytvořte si svůj vlastní životopis, oznámení nebo pozvánku! </h3>
					</div>
				</div>
			</div>
			<div class="content">
				<div class="row center">
					<hr class="divider" />
				</div>
				<div class="row">
					<div class="col s12 center">
						<a class="waves-effect waves-light btn" href="pages/login.php" title=""> Přihlásit se k účtu </a> 
						<a class="waves-effect waves-light btn" href="pages/registration.php" title="">Vytvořit nový účet</a>
					</div>
				</div>
				<div class="row">
						<!-- <div class="col s12 grey darken-3">
							<i class="material-icons promo-pic"> description </i>
							<h3 class="center"> Možnost okamžitého stažení dokumentu </h3>
						</div>
				        <div class="col s12 grey darken-2">
					        <i class="material-icons promo-pic right"> money_off </i>
					        <h3 class="center-align"> Vše, co vytvoříte můžete uložit na našem serveru úplně zdarma </h3>
					    </div>
				        <div class="col s12 grey darken-3">
					        <i class="material-icons promo-pic"> assessment </i>
					        <h3> Přehledné uživatelské prostředí pro správu vašich projektů </h3>
				        </div>
				        <div class="col s12 grey darken-2">
					        <i class="material-icons promo-pic right"> reorder </i>
					        <h3 class="center"> Vytvořte si své vlastní šablony pro jakýkoliv budoucí projekt </h3>
				    	</div>
				    	<div class="col s12 grey darken-3 left">
				    		<i class="material-icons promo-pic"> view_quilt </i>
				    		<h3 class="center"> Předem připravené šablony pro rychlejší tvorbu vašich dokumentů </h3>
				    	</div>			
				    	<div class="col s12 grey darken-2">
				    		<i class="material-icons promo-pic right"> wb_auto </i>
				    		<h3 class="center"> Inteligentní našeptávač </h3>
				    	</div> -->
				    	<div class="col s10 offset-s1">
				    		<div class="row">
						    	<div class="col s4 center-align">
						    		<i class="material-icons promo-pic"> description </i>
						    		<h4 class="center"> Možnost okamžitého stažení dokumentu </h4> 
						    	</div>
						    	<div class="col s4 center-align">
						    		<i class="material-icons promo-pic"> money_off </i>
						    		<h4 class="center"> Vše, co vytvoříte můžete uložit na našem serveru úplně zdarma </h4>
								</div>
								<div class="col s4 center-align">
									<i class="material-icons promo-pic"> assessment </i>
									<h4 class="center"> Přehledné uživatelské prostředí pro správu vašich projektů </h4>
								</div>
							</div>
							<div class="row">
								<div class="col s4 center-align">
							        <i class="material-icons promo-pic"> reorder </i>
							        <h4 class="center"> Vytvořte si své vlastní šablony pro jakýkoliv budoucí projekt </h4>
						    	</div>
						    	<div class="col s4 center-align">
						    		<i class="material-icons promo-pic"> view_quilt </i>
						    		<h4 class="center"> Předem připravené šablony pro rychlejší tvorbu vašich dokumentů </h4>
						    	</div>			
						    	<div class="col s4 center-align">
						    		<i class="material-icons promo-pic"> wb_auto </i>
						    		<h4 class="center"> Inteligentní našeptávač </h4>
						    	</div>						    
						    </div>
					    </div>
					</div>
				    <div class="row promo-div">
				    	<!-- <div class="col s9 offset-s2">
				    		<h2 class="center"> Vývojářský tým </h2>
				    		<div class="col s3 offset-s1">
				    			<img src="resources/pictures/main_developer.jpg" class="circle responsive-img" />
				    		</div>
				    		<div class="col s7 offset-s1">
				    			<h3> Jan Kočvara </h3>
				    			<span> Autor projektu, programátor, vývojář, student </span>
				    		</div>
			    		</div> -->
			    	</div>
				</div>
				<?php include_once "resources/includes/footer.php"; ?>	
			</div>
		</div>
	</body>
</html> 