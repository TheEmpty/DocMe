<?php
	//require "admin/config.php";
	session_start();
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="utf-8" />
		<meta author="Jan Kočvara" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
     	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
     	<link rel="stylesheet" type="text/css" href="../resources/styles/style.css" />
     	<script> $(".button-collapse").sideNav(); </script>

		<title> DocMe! </title>
	</head>
	<body>
		<div class="page">
			<?php //include_once $configs->path.'resources/includes/nav_pages.php'
				include_once "../resources/includes/nav_pages.php";
			 ?>
			<div class="content">
				<div class="row">
					<div class="col s10 offset-s1">
						<div class="row">
							<div class="person">
								<div class="col s5">
									<img src="#!" alt="" class="circle responsive-img" />
								</div>
								<h3> Jan Kočvara </h3>
								<span> Autor, programátor, návrhář designu </span>
							</div>
						</div>
						<div class="divider"></div>
						<div class="row">
							<div class="person">
								<div class="col s5">
									<img src="#!" alt="" class="circle responsive-img" />
								</div>
								<h3> Štěpán Štanc </h3>
								<span> Beta-tester </span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php //include_once $configs->path.'resources/includes/footer.php';
				include_once '../resources/includes/footer.php';
			 ?>
		</div>
	</body>
</html> 