<?php
	session_start();
	require_once "../../resources/scripts/pdo.php";
	require_once "../../objects/user.php";
	require_once "../../objects/article.php";
	$user = new USER($db);
	$user->is_loggedin();
	$article = new ARTICLE($db);
?>
<!DOCTYPE HTML>
<html>
 	<head>
 		<meta charset="utf-8" />
		<meta author="Jan Kočvara" />

		<link rel="stylesheet" type="text/css" href="resources/styles/dd.css"/>

		<link rel="stylesheet" type="text/css" href="resources/styles/style.css" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css"/>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
	    <script src="js/interact.js"> </script>
	    <script src="js/basic.js"> </script>
     	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>
     	<script>
     	$(document).ready(function () {
     		$(".button-collapse").sideNav();
     		$('.modal').modal();
     	});
     	</script>
     	<title> DocMe! </title>

 	</head>
 	<body>
 		<div class="page">
 			<?php 
 				include_once "../../resources/includes/nav_sub.php";
 				include_once "../../resources/includes/admin_menu_sub.php";

 				echo "<h1 class='center'> Přehled oznámení </h1>";
				echo "
		    			<table class='highlight'>
		    				<thead>
		    					<tr>
		    						<th> ID </th>
		    						<th> Jméno oznámení </th>
		    						<th> Popisek oznámení </th>
		    						<th> Obsah oznámení </th>
		    						<th> Vytvořen </th>
		    						<th> Upraven </th>
		    						<th> Stav </th>
		    					</tr>
		    				</thead>
		    				<tbody>";
				$showAnn = $article->showByType("Oznámení");
				foreach($showAnn as $ann){
					echo 
					"
						<tr>
							<td> ". $ann["ID"] ."</td>
							<td> ". $ann["Name"] ."</td>
							<td> ". $ann["Description"] ."</td>
							<td> ". $ann["Content"] ."</td>
							<td> ". $ann["Created"] ."</td>
							<td> ". $ann["Modified"] ."</td>
							<td> ". $ann["Status"] ."</td>
							<td> <a href='#modal1' class='modal-trigger' title='Zobrazit detaily zápisu'><i class='material-icons'>fullscreen</i></a></td>
    						<td> <a href='#modal_edit' class='modal-trigger' title='Editovat zápis'><i class='material-icons'>mode_edit</i></a></td>
    						<td> <a href='../../core.php?action=deleteArticle&ID=". $ann['ID'] ."' title='Smazat zápis'><i class='material-icons'>clear</i></a></td>
    					</tr>
						<div id='modal_edit' class='modal modal-fixed-footer'>
						    <div class='modal-content'>
						      <h4>Upravit projekt</h4>
						      	    <div class='row'>
									      <form class='col s12' method='POST' action='../../core.php?action=editArticle'>
										      <div class='row'>
										        <div class='input-field col s6'>
										          <input name='name' id='name' type='text' class='validate' placeholder='". $ann["Name"] ."'/>
										          <label for='name'>Název</label>
										        </div>
											  </div>
										      <div class='row'> 
										        <div class='input-field col s6'>
										          <input name='description' id='description' type='text' class='validate' placeholder='". $ann["Description"] ."'/>
										          <label for='description'>Popisek</label>
										        </div>
										      </div>
										      <div class='row'>
										      	<input id='ID' name='ID' type='hidden' value='". $ann["ID"] ."'/>
										      	<button id='submit' class='btn waves-effect waves-light right' type='submit' name='action'> Upravit projekt </button>
										      </div>
										  </form>
								    </div>
								</div>
								<div class='modal-footer'>
								    <a href='#!'' class='modal-action modal-close waves-effect waves-green btn-flat ''>Zrušit</a>
								</div>
							</div>
		    		";
				}
				?>
		</div>
	</body>
</html>
