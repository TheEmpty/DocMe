<?php
	session_start();
	require_once "config.php";

	if(isset($_GET["page"])){
		//Přesměruj na dannou page + oveření uživatele a jeho lvlu
		if(empty($_SESSION["user_session"])){
			unset($_SESSION["error"]);
			$_SESSION["error"] = "Pro umožnění přístupu do profilu musíte být přihlášen!";
			header("Location: ../pages/login.php");
		}else{
			$page = $_GET["page"];
			header("Location: pages/".$page.".php");
		}
	}

	if(empty($_SESSION["user_session"])){
		unset($_SESSION["error"]);
		$_SESSION["error"] = "Pro umožnění přístupu do profilu musíte být přihlášen!";
		header("Location: ../pages/login.php");
	}else if($_SESSION["Level"] < 1) {
		include_once "/pages/default_user_index.php";
	} elseif($_SESSION["Level"] >= 1) {
		include_once "/pages/admin/admin_index.php";
	}
	
	?>
 