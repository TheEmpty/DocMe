<?php
	require "../admin/config.php";
	session_start();
?>
<!DOCTYPE HTML>
<html>
	<head>
	<meta charset="utf-8" />
		<meta author="Jan Kočvara" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
     	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
     	<link rel="stylesheet" type="text/css" href="../resources/styles/style.css" />
     	<script> $(".button-collapse").sideNav(); </script>
         
		<title> DocMe! </title>
	</head>
	<body>
		<div class="page">
			<?php //include_once $configs->path."resources/includes/nav_pages.php"
			include_once '../resources/includes/nav_pages.php';
			 ?>
			<div class="content">
			 	<div class="text-box">
					<span class="bold"> Q: Is using this website free including all functions and features? </span>
					<br/>
					<p> A: Yes, It is. You don't need to pay anything if you don't want to! </p>
					<span class="bold"> Q: Do I have to create an account to try this website? </span>
					<br/>
					<p> A: No, but It's highly recommended. Without created account you can't save your progress on our server for example. </p>
			 	</div>
			</div>
			<?php //include_once $configs->path.'resources/includes/footer.php';
				  include_once '../resources/includes/footer.php';

			 ?>
		</div>
	</body>
</html> 