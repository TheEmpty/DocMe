	<?php

	CLASS PROJECT
	{
		private $DB;

		function __construct($DB_con){
			$this->DB = $DB_con;
		}
		
		public function show(){
			try{
				$sql = $this->DB->prepare("SELECT * FROM Projects");
				$sql->execute();
				$sqlReturn = $sql->fetchAll(PDO::FETCH_ASSOC);
				return $sqlReturn;
			}catch(PDOException $e){
				echo $e->getMessage();
			}

		}

		//funkce pro zobrazení detailů užiatele podle ID uživatele --------- PŘEDELAT
		public function showUsersProject($UserID){
			try{
				$sql = $this->DB->prepare("SELECT * FROM Users WHERE ID = :userid LIMIT 1");
				$sql->execute(array(":userid" => $UserID));
				$sqlReturn = $sql->fetchAll(PDO::FETCH_ASSOC);
				return $sqlReturn;
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		//funkce pro zobrazení všech detailů určitého projektu
		public function showProject($ID){
			try{
				$sql = $this->DB->prepare("SELECT * FROM Projects WHERE ID = :id LIMIT 1");
				$sql->execute(array(":id" => $ID));
				$sqlReturn = $sql->fetchAll(PDO::FETCH_ASSOC);
				return $sqlReturn;
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		//funkce pro ověření, jestli uživatel, který se pokouší projekt otevřít opravdu vlastní oprávnění k otevření souboru
		public function checkPermissions($projectID, $userID){
			try{
				$sql = $this->DB->prepare("SELECT User_ID FROM Projects WHERE ID = :id LIMIT 1");
				$sql->execute(array(":id" => $projectID));
				$sqlReturn = $sql->fetchAll(PDO::FETCH_ASSOC);
				foreach($sqlReturn as $data){
					if($data["User_ID"] == $userID){
						return true;
					}else{
						return false;
					}
				}
			}catch(PDOException $e){
				return $e->getMessage();
			}	
		}

		public function downloadProject($key){
			try{
				$sql = $this->DB->prepare("SELECT ID FROM Projects WHERE Dkey = :key");
				$sql->execute(array(":key" => $key));
				//return $sql->fetchAll(PDO::FETCH_ASSOC);
			}catch(PDOException $e){
				$e->getMessage();
			}
		}

		//Funkce vytvářející nový projekt
		public function create($name, $description, $userID, $type, $theme, $userKey, $email){
			try{
				$created = date("Y-m-d H:i:s");
				$hash = hash("sha256", $created);
				$write = $this->DB->prepare("INSERT INTO Projects(User_ID, Name, Description, Type, Theme, Created, Dkey) VALUES (:userID, :name, :description, :type, :theme, :created, :hash)");
				$write->bindparam(":userID", $userID);
				$write->bindparam(":name", $name);
				$write->bindparam(":description", $description);
				$write->bindparam(":type", $type);
				$write->bindparam(":theme", $theme);
				$write->bindparam(":created", $created);
				$write->bindparam(":hash", $hash);
				$write->execute();	

				$lastUsedID = $this->LastInsertId();
				$hashMail = hash("sha256", $email);
				$hashID = hash("sha256", $lastUsedID);
				
				//Jestli již složka existuje
				$projectFolder = "users/".$hashMail."/projects";
				if(!is_dir($projectFolder)){
					mkdir($projectFolder);
				    chmod($projectFolder, 0777);
				}

				$filepath = "users/".$hashMail."/projects/".$hashID;
				$filepathSQL = $hashMail."/projects/".$hashID;
				mkdir($filepath); //Vytoření složky s projektem
				chmod($filepath, 0777);

				$update = $this->DB->prepare("UPDATE Projects SET Folder_Hash = :filepath WHERE ID = :id LIMIT 1");
				$update->execute(array(":filepath" => $filepathSQL ,":id" => $lastUsedID));

				unset($_SESSION["message"]);
				$_SESSION["message"] = "Projekt byl úspěšně vytvořen";

				return $_SESSION["message"];
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
		
		//Funkce pro editaci základních údajů o projektu (jméno, popisek)
		public function editBasics($id, $name, $description){
			try{
				unset($_SESSION["message"]);
				if(!empty($description) && ($name)){
					$edit_all = $this->DB->prepare("UPDATE Projects SET Name = :name, Description = :description WHERE ID = :id LIMIT 1");
					$edit_all->execute(array(":name" => $name, ":description" => $description, ":id" => $id));
					return $_SESSION["message"];
				}else if(empty($description)){
					$edit_b = $this->DB->prepare("UPDATE Projects SET Name = :name WHERE ID = :id LIMIT 1");
					$edit_b->execute(array(":name" => $name, ":id" => $id));
					return $_SESSION["message"];
				}else if(empty($name)){
					$edit_n = $this->DB->prepare("UPDATE Projects SET Description = :description WHERE ID = :id LIMIT 1");
					$edit_n->execute(array(":description" => $description, ":id" => $id));
					return $_SESSION["message"];
				}else{
					$edit_m = $this->DB->prepare("UPDATE Projects SET Name = :name, Description = :description WHERE ID = :id LIMIT 1");
					$edit_m->execute(array(":name" => $name, ":description" => $description, ":id" => $id));
				}
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public function delete($ID, $mail){
			try{
				$delete = $this->DB->prepare("DELETE FROM Projects WHERE ID = :id"); //SQL Pro smazání projektu v databázi
				$delete->execute(array(":id" => $ID));

				 $hashMail = hash("sha256", $mail);
				 $hashId = hash("sha256", $ID);
				 $filepath = "users/".$hashMail."/projects/".$hashId; //Připravení cesty ke složce projektu
				 $this->delTree($filepath);
				 //rmdir($filepath, true); //Mazání složky projektu z domovského adresáře uživatele
				 
				unset($_SESSION["message"]);
				$_SESSION["message"] = "Projekt úspěšně smazán";
				return $_SESSION["message"];
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public function showByUserID($userID){
			try{
				$show = $this->DB->prepare("SELECT * FROM Projects WHERE User_ID = :userID ORDER BY ID DESC");
				$show->execute(array(":userID" => $userID));
				$showResult = $show->fetchAll(PDO::FETCH_ASSOC);
			    return $showResult;
				
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		//Private funkce pro získání posledního ID z posledního SQL zápisu
		private function LastInsertId(){
			return $this->DB->lastInsertId();
		}

		//Private funkce na rekurzivní mazání složek
		private function delTree($dir){ 
		   $files = array_diff(scandir($dir), array('.','..')); 
		    foreach ($files as $file) { 
		      (is_dir("$dir/$file")) ? delTree("$dir/$file") : unlink("$dir/$file"); 
		    } 
		    return rmdir($dir); 
		  } 
		}
?>