<?php
session_start();
$userFolder = $_POST["userFolder"];
$target_dir = "../users/". $userFolder . "images/";
if(!is_dir($target_dir)){
	mkdir($target_dir);
}

include_once "../resources/scripts/pdo.php";

//Zkontroluj předběžně velikost souboru pro zápis do databáze (pokud je větší, než je uveden limit, nezapisuj)
$fileTypeTest = explode(".", $_FILES["fileToUpload"]["name"]);
if($_FILES["fileToUpload"]["size"] < 1000000){
     $uploaded = date("Y-m-d H:i:s");
     $uploadSQL = $db->prepare("INSERT INTO UsersPictures(UsersID, Name, Type, Uploaded) VALUES (:id, :name, :type, :uploaded)");
     $uploadSQL->execute(array(":id" => $_SESSION['user_session'], ":name" => $_FILES["fileToUpload"]["name"], ":type" => $fileTypeTest[1], ":uploaded" => $uploaded));
 }

 $lastID = $db->lastInsertId();
//$lastID = $lastUsedID;

$target_file = $target_dir . $lastID ."_" .basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
// Zkontroluj, pokud je obrázek opravdu obrázek (kontroluj velikost souboru a temporary_name)
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if($check !== false) {
        echo "Soubor je obrázek - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "Soubor není obrázek.";
        $uploadOk = 0;
    }
}
// Zkontroluj, jestli soubor již neexistuje (nebylo by díky ID být možné)
if (file_exists($target_file)) {
    $_SESSION["error"] =  "Soubor s tímto názvem byl již nahrán!";
    $uploadOk = 0;
}
// Zkontroluj velikost souboru
if ($_FILES["fileToUpload"]["size"] > 1000000) {
    $_SESSION["error"] = "Soubor, který se snažíte nahrát je moc veliký!";
    $uploadOk = 0;
}
// Povol určité formáty, které lze nahrát
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
    $_SESSION["error"] = "Nahrát lze pouze soubory s příponou .jpg / .png / .jpeg / .gif";
    $uploadOk = 0;
}
// Zkontroluj, jestli není kontrolní proměnná 0
if ($uploadOk == 0) {
	echo "Obrázek nebyl nahrán.";
	header("Location: ../pages/profile.php");
// Pokud není problém, pokus se uploadovat obrázek
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        echo "Soubor ". basename( $_FILES["fileToUpload"]["name"]). " byl nahrán.";
        $_SESSION["error"] = "Obrázek byl nahrán a uložen";
        header("Location: ../pages/profile.php");
    } else {
    	header("Location: ../pages/profile.php");
    }
}
?>