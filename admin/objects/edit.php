<?php

	CLASS EDITOR_FACTORY 
	{

		private $Project_ID;
		private $Project_Name;
		private $Project_Path;
		private $Project_Type;
		private $Project_Theme;

		function __construct($DB_con){
			$this->DB = $DB_con;
		}

		public function inicialize($projectID, $userID){
			try{
				$load = $this->DB->prepare("SELECT * FROM Projects WHERE ID = :id LIMIT 1");
				$load->execute(array(":id" => $projectID));
				$loadResult = $load->fetchAll(PDO::FETCH_ASSOC);
				foreach($loadResult as $loaded){
					$this->Project_ID = $loaded["ID"];
					$this->Project_Name = $loaded["Name"];
					$this->Project_Path = $loaded["Folder_Hash"];
					$this->Project_Type = $loaded["Type"];
					$this->Project_Theme = $loaded["Theme"];
					require_once "edit_live.php";
					return new EDITOR($this->DB, $this->Project_ID, $userID, $this->Project_Name, $this->Project_Path, $this->Project_Type, $this->Project_Theme);
				}
			}catch(PDOException $e){
				echo $e->getMessage();
				return "Nebylo možné spojit se s databázovým serverem, prosíme, zkuste to později.";
			}
		}

		public function save($projectID, $elementJson, $configJson, $customJson){
			try{
				$load = $this->DB->prepare("SELECT * FROM Projects WHERE ID = :id LIMIT 1");
				$load->execute(array(":id" => $projectID));
				$loadResult = $load->fetchAll(PDO::FETCH_ASSOC);
				foreach($loadResult as $loaded){
					$elJsonFile = "users/". $loaded["Folder_Hash"] ."/elements.json";
					$coJsonFile = "users/". $loaded["Folder_Hash"] ."/config.json";
					$cuJsonFile = "users/". $loaded["Folder_Hash"] ."/custom.json";
					if(file_exists($jsonFile)){
						file_put_contents($elJsonFile, $elementJson);
					}else{
						$saveFileE = fopen($elJsonFile, "w");
						fwrite($saveFileE, $elementJson);
						fclose($saveFileE);
					}

					if(file_exists($coJsonFile)){
						file_put_contents($coJsonFile, $configJson);
					}else{
						$saveFileC = fopen($coJsonFile, "w");
						fwrite($saveFileC, $configJson);
						fclose($saveFileC);
					}

					if(file_exists($cuJsonFile)){
						file_put_contents($cuJsonFile, $customJson);
					}else{
						$saveFileCustom = fopen($cuJsonFile, "w");
						fwrite($saveFileCustom, $customJson);
						fclose($saveFileCustom);
					}
					return "Projekt úspěšně uložen!";
				}
			}catch(PDOException $e){
				echo $e->getMessage();
				return "Nebylo možné spojit se s databázovým serverem, zkuste to prosím později.";
			}
		}
	

		public function checkKey($key){
			try{
				$downloadSQL = $this->DB->prepare("SELECT * FROM Projects WHERE Dkey = :key LIMIT 1");
				$downloadSQL->execute(array(":key" => $key));
				$resultSQL = $downloadSQL->fetchAll(PDO::FETCH_ASSOC);
				if(empty($downloadSQL)){
						$_SESSION["message"] = "Pokoušíte se otevřít soubor, který vám nepatří!";
						return false;
				}else{
					foreach($resultSQL as $data){
						$this->displayGuest($data);
					}
					return "Chyba";
				}
			}catch(PDOException $e){
				$e->getMessage();
				return "Nelze se spojit s databází!";
			}
		}

		//Funkce pro vytvoření vlastní uživatelské textové šablony ve formátu PHP spolu se zápisem do databáze (záznam, kdy byla vytvořena )
		public function createUserTheme($id, $folder, $fileName, $type, $name, $bio, $college, $highschool, $experience){
			//Jestli již složka existuje
			$projectFolder = "users/".$folder."/themes";
			if(!is_dir($projectFolder)){
				mkdir($projectFolder);
			    chmod($projectFolder, 0777);
			}
			$filePath = "users/".$folder ."/themes/". $fileName.".json";
			//$fileContent = "name: ". $name ." \n bio: ". $bio ." \n college: ". $college ." \n highschool: ". $highschool . "\n experience: ". $experience; 
			$fileContent[] = array(
				"name" => $name,
				"bio" => $bio,
				"college" => $college,
				"highschool" => $highschool,
				"experience" => $experience,
			);
			$jsonFileContent = json_encode($fileContent);
			$themeFile = fopen($filePath, "w");
			fwrite($themeFile, $jsonFileContent);
			fclose($themeFile);

			$created = date("Y-m-d H:i:s");  

			$sql = $this->DB->prepare("INSERT INTO UsersThemes(UsersID, Name, Type, Created) VALUES (:id, :name, :type, :created)");
			$sql->execute(array(":id" => $id, ":name" => $fileName, ":type" => $type, ":created" => $created));

			return "Šablona úspěšně vytvořena!";
			exit();
		}

		//Funkce pro smazání vlastní uživatelské textové šablony z databáze i ze složky
		public function deleteUserTheme($id, $themeID, $folder){
			$sql = $this->DB->prepare("SELECT Name FROM UsersThemes WHERE UsersID = :uID AND ID = :tID LIMIT 1");
			$sql->execute(array(":uID" => $id, ":tID" => $themeID));
			$themeDetail = $sql->fetchAll(PDO::FETCH_ASSOC);
			
			foreach($themeDetail as $theme){
				$filePath = "users/".$folder."/themes/".$theme["Name"].".json";
				unlink($filePath);
			}

			$sqlDelete = $this->DB->prepare("DELETE FROM UsersThemes WHERE UsersID = :uID AND ID = :tID LIMIT 1");
			$sqlDelete->execute(array(":uID" => $id, ":tID" => $themeID));
			return "Šablona úspěšně smazána";
			exit();
			
		}


		private function displayGuest($data){
			return header("Location: ../editor/display.php?ID=".$data['ID']."&Key=".$data['Dkey']);
		}

	}

?>