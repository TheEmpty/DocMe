<?php
	session_start();
	require_once "../config.php";
	require_once "../objects/user.php";
	$userFactory = new USER($db);	//Vyvtvoření instance USER, pouze jakožto "továrničky"
	$userFactory->is_loggedin(); //Kontrola, zda je uživatel přihlášen
	$user = $userFactory->useFactory($_SESSION["user_session"]); //Vytvoření instance třídy LOGGED_USER, která pomocí constructoru udržuje všechna data
	
	$colors = array(
		"red", 
		"pink",
		"deep-orange",
		"orange",
		"amber",
		"yellow",
		"indigo",
		"blue",
		"light-blue",
		"cyan",
		"green",
		"teal",
		"light-green",
		"lime",
		"deep-purple",
		"purple",
		"brown",
		"grey",
		"blue-grey",
		"black",
		"white",
	);	
	?>
<!DOCTYPE HTML>
 <html>
 	<head>
 		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css"/>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
     	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>
 		<meta charset="utf-8" />
		<meta author="Jan Kočvara" />
		<link rel="stylesheet" type="text/css" href="../resources/styles/dd.css"/> 
		<link rel="stylesheet" type="text/css" href="../resources/styles/style.css" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
		<script src="../resources/scripts/dropdownContextMenu.js"> </script>
		<script src="https://apis.google.com/js/platform.js" async defer></script>
     	<meta name="google-signin-client_id" content="864532971569-r15luhunka88mfp4m2qld9k0uc89smcr.apps.googleusercontent.com"> 

     	<?php
     		include_once "../resources/scripts/user_color.php";
     	 ?>

     	<script>
     		$(document).ready(function(){
     			$(".modal").modal();
    			$('textarea#bio, textarea#college, textarea#highschool, textarea#experience').characterCounter();
    			$('select').material_select();
    			$('.materialboxed').materialbox();
				$('.datepicker').pickadate({
				    selectMonths: true, // Creates a dropdown to control month
				    selectYears: 100, // Creates a dropdown of 15 years to control year,
				    today: 'Dnešní den',
				    clear: 'Vyčistit',
				    close: 'Potvrdit',
				    closeOnSelect: false // Close upon selecting a date,
				});
    		    $('.collapsible').collapsible();
    //Pomocné proměnné
    		    var pickedPhotoID = ""; //Proměnná pro získání ID na právě kliknutou fotku z kolekce fotek
   
				//Aby datepicker měl při novém otevření připravené hodnoty z databáze
				$('.datepicker').val("<?php echo date('d.m.Y', strtotime($user->show('Birth'))) ?>");

				//Pomocné proměnné
				var newNavColor = ""; //Poslední použitá barva na navigační menu

				//Funkce měnící barvu navigačního menu a ukládající konfiguraci do DB
				$(document.body).on('click', '.colorPickNav', function(e){
					e.preventDefault();
					newNavColor = $(this).attr('id');
					//$('#'+id).class("active");
					$('navColors').append('<div class="row"> <div class="colorPickNav '+newNavColor+'"> <span class="colorPick '+newNavColor +'"> </span> </div> </div>');
					$('nav').attr("class", newNavColor);

					$.ajax({
						type: "POST",
						url: "../core.php?action=saveUserNav",
						data: {color: newNavColor},
						dataType: "text",
						success: function(){
							Materialize.toast("Barva nastavena!", 2000);
						},
						error: function(){
							Materialize.toast("Nastala chyba při uložení barvy", 2000);
						},
					});
				});

				//Funkce na změnu barvy tlačítek
				$(document.body).on('click', '.colorPickBtn', function(e){
					e.preventDefault();
					newBtnColor = $(this).attr('id');
					$('navColors').append('<div class="row"> <div class="colorPickNav '+newBtnColor+'"> <span class="colorPick '+newBtnColor +'"> </span> </div> </div>');
					$('.profile-button').attr("class" ,"waves-effect waves-light btn right modal-trigger profile-button "+ newBtnColor);

					$.ajax({
						type: "POST",
						url: "../core.php?action=saveUserBtn",
						dataType: "text",
						data: {color: newBtnColor},
						success: function(response){
							Materialize.toast("Barva nastavena", 2000);
						},
						error: function(response){
							Materialize.toast("Nastala chyba při uložení barvy", 2000);
						},
					});
				});

				//Funkce, která upraví selected option v selectoru před změnou profilu (podle záznamu z databáze)
				$(document.body).on('click', '#editFormButton', function(e){
					var actualGender = "<?php echo $user->show('Gender') ?>";
					if("Muž" == actualGender){
						$('#male').remove();
						$('#genderEdit').append("<option id='male' value='Muž' selected> Muž </option>");
					}else if("Žena" == actualGender){
						$('#female').remove();
						$('#genderEdit').append("<option id='female' value='Žena' selected> Žena </option>");
					}else if("Jiné" == actualGender){
						$('#other').remove();
						$('#genderEdit').append("<option id='other' value='Jiné' selected> Jiné </option>");
					}
					
					$('#genderEdit').material_select();

				});

					//Funkce, která zobrazí všechny obrázky, které jsou k tomuto profilu (účtu) přiřazeny
					var userFolder = '<?php echo $user->show("FolderPath"); ?>';
					var folder = "../users/"+ userFolder +"/images/";
					$.ajax({
					    url : folder,
					    success: function (data) {
					        $(data).find("a").attr("href", function (i, val) {
					            if( val.match(/\.(jpe?g|png|gif)$/) ) { 
					                $("#userPicCollection").append( "<div class='col s4'> <div class='image-container'> <img id='"+val+"' class='userPicsCollection materialboxed picIdentity' src='"+ folder + val +"'/> </div> </div>" );
					                $('.materialboxed').materialbox();
					            }
					        });
					    },
					    error: function(data){
					    	Materialize.toast('Nastala chyba při načítání Vaší fotogalerie, prosíme, obnovte stránku', '4000');
					    }
					});

					//Funkce, na kliknutí na ikonu koše se smaže z databáze i ze složky textová šablona uživatele (pomocí POST metody)
					$(document.body).on('click', '.themeDel', function(e){
						var themeToDelete = jQuery(this).attr('id');
						var userID = "<?php echo $user->show('ID'); ?>";
						var userFolder = "<?php echo $user->show('FolderPath'); ?>";

						$.ajax({
							url: '../core.php?action=deleteUserTheme',
							type: 'POST',
							data: {
								ID: userID,
								file: themeToDelete,
								folder: userFolder
							},
							success: function(e){
									$('#'+themeToDelete+"tr").remove();
								},
							error: function(e){
								Materialize.toast('Nastala chyba při mazání!', '4000')
							}

						});
					
					});

					//Funkce pro full-screen zobrazení fotky
					$(document.body).on('mousedown', '.showPicture', function(e){
						//OTEVŘI TEN FULLSCREEN
						setTimeout(function(){
							jQuery.Event("focusin");
							$('#'+pickedPhotoID).trigger( event );
							//alert(pickedPhotoID);
						}, 200);
					});

					//Funkce pro získání ID elementu (fotky v kolekci) při kliknutí pravého tlačítka myši
					$(document.body).on('mousedown', '.picIdentity', function(e){
						pickedPhotoID = $(this).attr('id'); //Získání ID	
					});

					//Funkce pro nastavení profilového obrázku
					$(document.body).on('click', '.useAsProfilePicture', function(e){
						if(pickedPhotoID != 0){
							e.preventDefault();
							//Poslání src obrázku (nastavení) do DB
							var userFolder = '<?php echo $user->show("FolderPath"); ?>';
							$.ajax({
								url: '../core.php?action=useAsProfilePic',
								type: 'POST',
								data: {
									ID: '<?php echo $_SESSION["user_session"]; ?>',
									file: pickedPhotoID,
								},
								success: function(e){
									Materialize.toast('Obrázek nastaven jako profilový obrázek', 4000);
									$('#userPic').attr('src', '../users/'+userFolder+'/images/'+pickedPhotoID);
								},
								error: function(e){
									Materialize.toast('Nastala chyba při nastavování obrázku jako profilového obrázku');
								},
							});
						}else{
							Materialize.toast('Nebylo možné rozpoznat obrázek');
						}
					});

					//Funkce v context-menu (smazání fotky)
					$(document.body).on('click', '.deleteUserPic', function(e){
						//alert("ID: "+pickedPhotoID);
						if(pickedPhotoID != 0){
							e.preventDefault();
							$('#'+pickedPhotoID+'').css("display", "none");

							$.ajax({
								url: '../core.php?action=deleteUserPic',
								type: 'POST',
								data: {
									ID: '<?php echo $_SESSION["user_session"]; ?>',
									file: pickedPhotoID,
									folder: '<?php echo $user->show("FolderPath"); ?>',
								},
								success: function(e){
									$('#'+pickedPhotoID).remove();
									//$('.image-container').load(window.location + ".image-container");
									Materialize.toast('Obrázek úspěšně smazán', 4000);
								},
								error: function(e){
									Materialize.toast('Nastala chyba při mazání obrázku', 4000);
								},
							});
						}else{
							Materialize.toast('Nebylo možné rozpoznat obrázek');
						}
					});	
    		 

     		});
     	</script>
     	<title> DocMe! </title>

 	</head>
 	<body>
 		<div class="page">
				<?php
	 				include_once ("../resources/includes/nav_sub.php");
		      		if(!empty($_SESSION["error"])){
						echo "<script> Materialize.toast('".$_SESSION["error"]."', 4000) </script>";
						unset($_SESSION["error"]);
					}
	 			?>
	 			<ul class='profile-custom-menu custom-menu'>
	 				<li class="showPicture" data-action="third"> Zobrazit obrázek </li>
	 				<li class="useAsProfilePicture" data-action="first">Nastavit jako profilový obrázek</li>
				    <li class="deleteUserPic" data-action="second">Smazat obrázek </li>
				</ul>
 			<div class="profile_content">
	 			<div class="row"> 
	 			 	<?php 
	 			 		if(($user->show("ProfilePictureID") == "0") OR $user->show("ProfilePictureID") == NULL){
	 			 			echo'
	 			 			<div class="col s4">
			 					 <img id="userPic" class="right" src="../resources/images/default_user.jpg" />
			 				</div>';
			 			 }else{
			 			 	echo'
			 			 	<div class="col s4">
			 					 <img id="userPic" class="right" src="../users/'. $user->show("FolderPath").'/images/'.  $user->show("ProfilePictureID") .'_'. $user->show("ProfilePicture").'" />
			 				</div>';
			 			 }
 						//Generování profilu (používají se data z třídy logged_user)
	 					echo 
	 					'	 			
	 				<div id="userInfo" class="col s6">

	 					<h2> '. $user->show("Firstname") .' '. $user->show("Surname") .' </h2>
	 					<hr>
	 					<table>
	 						<tr>
	 							<td> Přezdívka: </td>
	 							<td> '. $user->show("DisplayName") .' </td>
	 						<tr>
	 							<td> Email: </td>
	 							<td> '. $user->show("Email") .'</td>
	 						</tr>
	 						<tr> 
	 							<td> Heslo: </td>
	 							<td> ******* </td>
	 						</tr>
	 						<tr>
	 							<td> Datum narození: </td>
	 							<td> '. date("d.m.Y", strtotime($user->show("Birth"))) .' </td>
	 						</tr>
	 						<tr>
	 							<td> Pohlaví: </td>
	 							<td> '. $user->show("Gender") .' </td>
	 						</tr>
	 						<tr>
	 							<td> Úroveň oprávnění: </td>
	 							<td>'. $user->show("LevelWord").'</td>
	 						</tr>
	 						<tr>
	 							<td> Naposled přihlášen: </td>
	 							<td> '. date("d.m.Y - H:i", strtotime($user->show("LastLogin"))) .'</td>
	 						</tr>
	 					</table>';

	 					//Generování formuláře pro editaci uživatelského profilu
	 					echo'
 						<div id="userEditModal" class="modal modal-fixed-footer">
						    <div class="modal-content">
						      <h4>Upravit profil</h4>
						      <br>
					      	    <div class="row">
								      <form class="col s12" method="POST" action="../core.php?action=editUser">
									      <div class="row">
									        <div class="input-field col s6">
									          <input name="name" id="name" type="text" class="validate" placeholder="'. $user->show("DisplayName") .'"/>
									          <label for="name">Vaše přezdívka: </label>
									        </div>
										  </div>
										  <div class="row">
									      	<div class="input-field col s6">
									      		<input id="birth" name="birth" type="text" class="datepicker">
									      		<label for="birth"> Datum Narození: </label>
									      	</div>
									      </div>
									      <div class="row">
									      	<div class="input-field col s6">
									      		<select id="genderEdit" name="gender">
									      			<option id="male" value="Muž"> Muž </option>
									      			<option id="female" value="Žena"> Žena </option>
									      			<option id="other" value="Jiné"> Jiné </option>
									      		</select>
									      		<label for="gender"> Pohlaví: </label>
									      	</div>
									      </div>
									      <div class="row"> 
									        <div class="input-field col s6">
									          <input name="email" id="email" type="email" class="validate" placeholder="'. $user->show("Email") .'"/>
									          <label for="email">Aktivní email: </label>
									        </div>
									      </div>
									      <div class="row">
									      	<div class="input-field col s6">
										      	<input id="password" name="password" type="password" placeholder="*****"/>
										      	<label for="password"> Nové heslo: </label>
										    </div>
									      </div>
									      <div class="row">
									      	<input type="hidden" name="ID" value="'.$user->show("ID").'"/>
									      </div>
								    </div>
								</div>
								<div class="modal-footer">
									<button id="submit" class="btn waves-effect waves-light right" type="submit" name="action"> Upravit účet</button>
								    <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Zrušit</a>
								</div>
							</form>
						</div>
						';
					?>
	 				</div>
	 				<div class="row">
	 					<div class="col s10">
	 						<a id="editFormButton" href="#userEditModal" class="waves-effect waves-light btn right modal-trigger profile-button orange darken-2"> Editovat účet </a> 	
	 					</div>
	 				</div>
	 				<div class="bigProfileContainer">
	 					<div class="row">
	 						<div class="col s10">
	 							<div class="row">
	 								<h3 class="left"> Zvolení barevného rozhraní </h3>
	 							</div>
	 							<p class="left"> Editujte si profil o vaše oblíbené barvy! Jednoduše si zvolte vlastní spektrum barev, které se aplikují na tlačítka, navigační panel a další všude ve vašem profilu! </p>	 						
	 						</div>
	 					</div>	
 						<div class="row">
						    <div class="col s10">
						        <ul class="tabs">
								    <li class="tab col s3"><a class="active" href="#navColors">Navigační panel</a></li>
							        <li class="tab col s3"><a href="#btnColors">Tlačítka</a></li>
							    </ul>
						    </div>
						   	<div id="navColors" class="col s10">
						   		<div class="row"> 
						    		<?php 
									foreach($colors as $color){
											echo "<div id='". $color ."' class='colorPickNav ". $color ."'> <span id='". $color ."_picker' class='colorPick ".$color."'> </span> </div>";
										}
										
									?>
								</div>
							</div>		
							<div id="btnColors" class="col s10"> 
								<div class="row">
							    	<?php 
										foreach($colors as $color){
												echo "<div id='". $color ."' class='colorPickBtn ". $color ."'> <span class='colorPick ".$color."'> </span> </div>";
											}
										
									?>
								</div>
							</div>						   
	 					</div>
	 				</div>
	 				<div class="bigProfileContainer">
	 					<div class="row">	
		 					<div class="col s10">
		 						<div class="row">
		 							<h3 class="left"> Vaše textové šablony </h3>
		 						</div>
		 						<p class="left"> Pomocí textové šablony si můžete předepsat text do vašich projektů, poté si jen při editaci vašeho projektu zvolte, kterou šablonu chcete použít! </p>
		 						<table class="responsive-table highlight">
		 							<thead>
			 							<tr>
			 								<td> Název šablony </td>
			 								<td> Typ projektu </td>
			 								<td> Datum vytvoření </td>
			 								<td> Akce </td>
			 							</tr>
		 							</thead>
		 							<tbody>
		 								<?php
		 									$themes = $user->showUserThemes();
		 									foreach($themes as $data){
			 									echo 
			 									'<tr id="'.$data["ID"].'tr">
			 										<td>'.$data["Name"].'</td>
			 										<td>'.$data["Type"].' </td>
			 										<td>'.$data["Created"].' </td>
			 										<td id="'.$data["ID"].'" class="btn red themeDel tooltipped" data-position="right" data-delay="30" data-tooltip="Smazat tuto šablonu" style="margin-top: 5px">
			 											<i class="material-icons"> delete </i> </td>
			 									</tr>';
			 								}
		 								?>
		 							</tbody>
		 						</table>
		 						<a href="#customThemeCreate" class="waves-effect waves-light btn right modal-trigger profile-button orange darken-2"> Vytvořit novou šablonu </a>
		 					</div>
		 				</div>
		 			</div>
		 			<div class="bigProfileContainer">
 						<div class="row">
 							<div class="col s10">
 								<div class="row">
 									<h3 class=left> Vaše kolekce obrázků </h3> 
 								</div>
 								<p class="left"> Zde jsou veškeré obrázky, které jste nahráli a uložili během svých projektů. Jsou k dispozici a viditelné pouze a jen vám.
 								</p>
 								<div id="userPicCollection" class="col s10 row">
 								</div>
 								<form action="../uploader/upload.php" class="col s12" method="POST" enctype="multipart/form-data">
 									<div class="input-field file-field">
 										<!-- <a href="#uploadNewPic" class="waves-effect waves-light btn right profile-button orange darken-2"> Nahrát nový obrázek </a> -->
 										 <div class="waves-effect waves-light btn modal-trigger orange darken-2">
							                <span>Vybrat obrázek pro nahrání</span>
							                <input type="file" id="image_input" class="file" name="fileToUpload" accept="image/png,image/jpeg,image/gif">
							              </div>
							              <div class="file-path-wrapper col s8">
							                <input class="file-path validate" type="text" placeholder="Název souboru">
							              </div>
							              <input type="hidden" name="userFolder" value=<?php echo ''.$user->show("FolderPath") .''; ?>/>
							              <button type="submit" class="waves-effect waves-light btn profile-button" name="submit" > Uložit obrázek </button>
 									</div>
 								</form>
 							</div>
 						</div>
 						<div class="row">
	 						<div id="customThemeCreate" class="modal modal-fixed-footer">
							    <div class="modal-content">
							      <h4>Nová šablona</h4>
							      <br />
							      <?php 
							      echo '
						      	    <div class="row">
									      <form class="col s12" method="POST" action="../core.php?action=newUserTheme">
	      						      			<div class="row">
											        <div class="input-field col s6">
											          <input name="file" id="file" type="text" class="validate" placeholder="Název šablony"/>
											          <label for="name">Název této šablony </label>
											        </div>
												</div>
												<div class="row">
											        <div class="input-field col s6">
											          <select name="type">
											          	<option value="CV"> Životopis / CV </option>
											          	<option value="Oznámení"> Oznámení </option>
											          	<option value="Pozvánka"> Pozvánka </option>
											          </select> 
											          <label for="name">Název této šablony </label>
											        </div>
												</div>
										      <div class="row">
										        <div class="input-field col s6">
										          <input name="name" id="username" type="text" placeholder="'. $user->show("Firstname") . $user->show("Surname") .'"/>
										          <label for="name">Vaše jméno: </label>
										        </div>
											  </div>
										      <div class="row"> 
										        <div class="input-field col s12">
										          <textarea id="bio" name="bio" class="materialize-textarea" data-length="200"></textarea>
										          <label for="bio">Základní informace o Vás: </label>
										        </div>
										      </div>
										      <div class="row">
										      	<div class="input-field col s12">
											      	<textarea id="college" name="college" class="materialize-textarea" data-length="500" ></textarea>
											      	<label for="college"> Kde jste studoval vysokou školu? </label>
											    </div> 
										      </div>
										      <div class="row">
										      	<div class="input-field col s12">
										      		<textarea id="highschool" name="highschool" class="materialize-textarea" data-length="500"></textarea>
										      		<label for="highschool"> Kde jste studoval střední školu / učiliště? </label>
										      	</div>
										      </div>
										      <div class="row">
										      	<div class="input-field col s12">
										      		<textarea id="experience" name="experience" class="materialize-textarea" data-length="500"></textarea>
										      		<label for="experience"> Pracoval jste již někde? Máte nějakou praxi? </label>
										      	</div>
										      </div>
										      <div class="row">
										      	<div class="input-field col s12">

										      	</div>
										      </div>
										      <div class="row">
										      	<input id="Folder" name="folder" type="hidden" value="'. $user->show("FolderPath").'"/>
										      </div>
										      <div class="row">
										      	<input id="IDuser" name="ID" type="hidden" value="'.$user->show("ID").'" />
										      </div>
									    </div>
									</div>
									<div class="modal-footer">
										<button id="submitTheme" class="btn waves-effect waves-light right" type="submit" name="action"> Vytvořit šablonu </button>
									    <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Zrušit</a>
									</div>
								</form>
							</div>
							';
							?>
		 				</div>
		 				</div>
		 				</div>
		 			</div>
	 				</div>
	 			</div>
	 		</div>
 	</body>
 </html>