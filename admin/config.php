<?php
	//Primární konfigurace
	//Definování root složky (kořenu webové stránky)

	//WEB VERZE: $path = 'http://' . $_SERVER['HTTP_HOST'] . "/~kocvaja14/project/";
	/*LOCALHOST VERZE:*/ $Path = $_SERVER['HTTP_HOST'] . "/docme"; //echo $Path;
 	//$PathR = " ". $_SERVER['DOCUMENT_ROOT'] . "/docme/";
	//$Path = "' ". $_SERVER['DOCUMENT_ROOT'] . "/docme/";

	ini_set('display_errors', TRUE);
	date_default_timezone_set( "Europe/Prague" );

	//Nastavení konstant webu
	  define("dbserver", "127.0.0.1");
	  define("dbuser", "root");
	  define("dbpass", "mysql");
	  define("dbname", "docme");
	  define('site_title', 'DocMe!');
	  define('path', $Path);

	/*define('PROJECT_BASE_FOLDER'    , substr($_SERVER['REQUEST_URI'], 0, 9) == '/sample/' ? '/sample/' : '');
	//sample is the name of the folder. the 0 to 8 is how many char your project name has including the '/'

	define('PROJECT_FOLDER'         , substr($_SERVER['REQUEST_URI'], 0, 9) == '/sample/' ? '' : '');
	define('HTTP'                   , 'http://');
	define('BASE_PATH'              , $_SERVER['DOCUMENT_ROOT'] . PROJECT_BASE_FOLDER . PROJECT_FOLDER);
	define('SITE_URL'               , HTTP . $_SERVER['SERVER_NAME'] . str_replace($_SERVER['DOCUMENT_ROOT'] . '/', '', BASE_PATH . "//~kocvaja14/project"));*/

	//Vlastní exception handler
	function exceptions_handler($e){
		echo "
			<!DOCTYPE HTML>
			<html>
				<head>
					<title> DocMe | Kritická chyba </title>
			 		<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
					<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css'/>
					<script src='https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js'></script>
			     	<link href='https://fonts.googleapis.com/icon?family=Material+Icons' rel='stylesheet'/>
				</head>
				<body>
					<div class='row center'>
						<div class='col s8 offset-s2'>
							<h1 style='margin-top: 10%; text-decoration: underline'> DocMe! </h1>
							<!-- <i class='material-icons large'> cloud_off </i> -->
							<i class='material-icons large'> error cloud_off flash_off highlight_off not_interested error_outline </i>
							<h4>
								Omlouváme se, ale byla zjištěna kritická chyba! Prosíme, počkejte do restartu webové stránky, nebo kontaktujte administrátora.
							</h4>
							<div class='row red center-align' style='margin-top: 10%'>
								<p>". $e->getMessage() ."</p>
							</div>
						</div>
					</div>
				</body>
			</html>
			";

		error_log($e->getMessage());
	}
	//Definování vlastního exception handleru
	set_exception_handler( 'exceptions_handler' );
	
	include_once $path."/resources/scripts/pdo.php";

?>