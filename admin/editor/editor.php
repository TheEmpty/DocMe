<?php  
	session_start();
	require_once "../resources/scripts/pdo.php";
	require_once "../objects/edit.php";
	require_once "../config.php";
	require_once "../objects/user.php";
	require_once "../objects/logged_user.php";
	if(!isset($_GET["key"])){
		if($_SESSION["editPermission"] == false){
			unset($_SESSION["error"]);
			$_SESSION["error"] = "Nemáte dostatečné oprávnění k práci s tímto souborem!";
			header("Location: ../../index.php");
			exit();
		}else{
			//require_once "../config.php";
			$editorFactory = new EDITOR_FACTORY($db);
			$user = new USER($db); 	
			$editor = $editorFactory->inicialize($_SESSION["edit_id"], $_SESSION["user_session"]); //Vytvoření instance třídy EDITOR a zároveň její připravení
			$userInstance = $user->useFactory($_SESSION["user_session"]);
		}
	}else{
		$downloader = new EDITOR_FACTORY($db);
		$downloader->checkKey($_GET["key"]);
	}
?>
<!DOCTYPE HTML>	
<html>
	<head>
		<meta charset="utf-8" />
		<meta author="Jan Kočvara" />
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
     	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
     	<!-- Defaultní CSS file pro editor -->
     	<link rel="stylesheet" type="text/css" href="../resources/styles/editor_default.css" />
   
     	<!-- linkování na všechny js knihovny, css soubory určené pro Drag&Drop -->
		<script src="..\resources\libraries\jquery-ui-1.12.1\jquery-ui.min.js"> </script>

		<script src="../resources/scripts/dropdownContextMenu.js"> </script>
		<!-- Přidání JS scriptů pro přepínání dynamické přepínání CSS souborů a dynamické operace s HTML prvky -->

     	<script>

 		 /* Prototyp přepisovatelného divu! 
     		function divClicked() {
			    var divHtml = $(this).html();
			    var editableText = $("<textarea />");
			    editableText.val(divHtml);
			    $(this).replaceWith(editableText);
			    editableText.focus();
			    // setup the blur event for this new textarea
			    editableText.blur(editableTextBlurred);
			}

			function editableTextBlurred() {
			    var html = $(this).val();
			    var viewableText = $("<div>");
			    viewableText.html(html);
			    $(this).replaceWith(viewableText);
			    // setup the click event for this new div
			    viewableText.click(divClicked);
			}

			$(document).ready(function() {
			    $("div").click(divClicked);
			});

			/* Konec přepisovatelného divu! */

     		$(document).ready(function () { 
    			$( ".resizable" ).resizable(); //Inicializace resizable
		 		$(".button-collapse").sideNav(); //Inicializace bočního navigačního panelu
				$('select').material_select(); //Inicializace selectoru (<select>)
				$('.modal').modal(); //Inicializace modálních oken

				//$(".editor").jqte(); //Inicializace textového editoru
				 
				var activateDrag = true; //False = povolení SORTABLE, True = Povolení DRAG&DROP
		 		var divCounter = 0; //Počítadlo pro vytváření unikátního divu
		 		var colorCounter = 0; //Počítadlo, kolikrát byla změněna barva pozadí
		 		var lastUsedColor = " "; //Proměnná pro uložení naposled použité barvy (přidané jako pozadí)
		 		var shadeCounter = 0; //Počítaldo, kolikrát byla změněna shade 
		 		var lastUsedShade = " "; //Naposled použitý shade
		 		var actualEditingElementID = ""; //Aktuálně editovaný textový element (jeho ID)
		 		var actualEditingElementText = ""; //Aktuálně editovaný textový element (text)
		 		var clickedDivID = ""; //Důležitá proměnná, která ukládá ID prvku (divu) na který jsem klikl (užitečné při pravém tlačítku hlavně) 
		 		/*var G_loadedBackground = ""; //Načtená barva pozadí
		 		var G_loadedFont = ""; //Načtený globální font
		 		var G_loadedColor = "";	//Načtená globální barva textu*/

		 		//Načtení projektu
		 		loadProject();

		 		//Funkce pro načtení JSON konfigurace (načtení uložených dat)
		 		function loadProject(){
		 			var firstLoad = '<?php echo $editor->firstLoad();?>';
					

		 			//Pokud se jedná o první spuštění projektu od jeho vytvoření, nahraj defaultní šablonu
					if(firstLoad == true){
						//Nahrání defaultní šablony
						var elJson = <?php echo $editor->loadDefaultText(); ?>;
						var confJson = <?php echo $editor->loadDefaultConfig(); ?>;
						var cmJson = <?php echo $editor->loadDefaultCustom(); ?>;
					}else{
						//Pokud toto není první spuštění, nahraj save
						var elJson = <?php echo $editor->loadText();?>;
						var confJson = <?php echo $editor->loadConfig();?>;
						var cmJson = <?php echo $editor->loadCustom();?>;
					}

					$.each(elJson, function(index, element){
						//$('#'+element.id).parent().css({position: 'relative'});
						$('#'+element.id).css({top: element.top, left: element.left, position: 'absolute'});
						$('#'+element.txtName).text(element.txtContent); //Přepsání textu v textových polích
						$('#'+element.id).css("background-color", element.background); //Upravení CSS Background Color v elementech(divech)
						/*if(element.id.includes('custom')){
							$('.sortable').append('<div id="'+element.id+'" class="moovable"> <h3 id="'+element.txtName+'Title" class="editable"> '+ element.txtContent +'</h3> <p id="'+element.txtName+'" class="editable"> '+element.txtContent+' </p> </div>');
						}*/

						//alert("data: "+ element.id);
					});

					$.each(confJson, function(index, element){
						$('#content-page').css("background-color", element.background);
						$('#content-page').css("font-family", element.font);
						$('#content-page').css("color", element.color);
						divCounter = element.divCounter;
						//Nastavení hodnot do selectorů na levém panelu

						//Nefunguje
						/*G_loadedBackground = element.background;
						G_loadedColor = element.color;
						G_loadedFont = element.font;*/

					});

					//Načtení JSON custom elementů (load funkce)
					$.each(cmJson, function(index, element){
						$('.sortable').append('<div id="'+element.id+'" class="moovable" style="background-color:'+element.background+'"> <h3 id="'+element.titleName+'" class="editable title"> '+element.titleText+' </h3> <p id="'+element.textName+'" class="editable"> '+element.textContent+'</p> </div>');
						$('#'+element.id).css({top: element.top, left: element.left, position: 'absolute'});
						
					});
					initDrag();

				

					//Zobrazení projektu (stránky)
					$('#content-page').fadeIn("300");

				}
		 		var type = "<?php echo $editor->show('Type') ?>";
		 		var theme = "<?php echo $editor->show('Theme') ?>";
		 		$('head').append('<link id="'+ type+theme +'" rel="stylesheet" href="../resources/styles/editor_templates/'+type+'_'+theme+'.css" type="text/css" />'); //Funkce pro nahrání defaultního CSS template souboru, který je zaznamenán v databázi

		 		//Funkce pro získání ID divu na který jsem klikl pravým tlačítkem myši (pro otevření context-menu)
		 		$(document.body).on('mousedown', '.moovable', function(e){
		 			clickedDivID = $(this).attr('id');
		 		});

		 		$(document.body).on('change', '#activateDragNdrop', function(e){
			 		activateDrag = $('#activateDragNdrop').check;
			 		alert(activateDrag);
			 		initDrag();
				});

		 		function initDrag(){
					if(activateDrag){ //Pokud je požadován drag&drop v nastavení editoru			 		
					 		$('.moovable').draggable({ //Inicializace draggable
								grid: [10,10] //Nastavení o kolik pixelů bude skákat aktuálně tažený prvek
							 });
			    		}else{
			    			$( ".sortable" ).sortable(); //Inicializace sortable
			    			$( ".sortable" ).disableSelection();
			    		}
				}
		 		//Funkce pro dynamickou změnu načteného CSS souboru, původní se smaže a načte se nový, požadovaný
		 		$(document.body).on('click', '.loadCSS', function (e) {
		 			e.preventDefault();
		 			var buttonTheme = jQuery(this).val(); //Získání value hodnoty z bttonu
		 			$('#'+type+theme).remove(); //Smazání <link> na původní CSS file
		 			$('#'+type+buttonTheme).remove(); //Smazání <link> CSS file, který mohl být vygenerován dříve tímto buttonem
		 			$('head').append('<link id="'+ type+buttonTheme +'" rel="stylesheet" href="../resources/styles/editor_templates/'+type+'_'+buttonTheme+'.css" type="text/css" />'); //Přidání požadovaného CSS file pomocí <link>
		 			 });

		 			//Jakmile se bude ukládat (save) tento dokument, do skrytého inputu se vloží aktuálně používaný template a aktualizuje se v databázi, pokud se změnil od minula (pokud se používá jiný)

		 			//Funkce pro dynamické přidání HTML prvků
		 		$(document.body).on('click', '.createDiv', function(e) {
		 			e.preventDefault();
		 			divCounter = divCounter + 1;
		 			var newElement = jQuery(this).val();
		 			if( newElement == "div-txt" ){ //Pokud uživatel žádá div s txtem
		 				$('.sortable').append('<div id="custom_'+ divCounter +'" class="moovable"> <p id="p'+divCounter+'" class="editable"> Kliknutím přidejte text </p> </div>');
		 				initDrag();
		 			}else if( newElement == "div-img" ){ //Pokud uživatel chce přidat obrázek, otevři nabídku
		 				$('#pictureModal').modal('open');
		 				initDrag();
		 			}else if(newElement == "hr"){ //Pokud uživatel chce vodorovnou čáru (hr)
		 				$('.sortable').append('<hr id="custom_' + divCounter +'" class="moovable line"/>');
		 				initDrag();
		 			}
		 			
		 		});

		 			//Funkce pro dynamické mazání HTML prvků
		 		$(document.body).on('click', '.deleteDiv', function(e) {
		 			e.preventDefault();
		 			$('#'+clickedDivID).remove();

		 		});	

		 			//Funkce pro změnu barvy pozadí
		 		$(document.body).on('click', '.changeBackground', function(e){
		 			e.preventDefault();
		 			if(colorCounter > 0){ //Kontrola counteru, jestli už někdy předtím nebyla barva měněna
		 				$('#content-page').removeClass(lastUsedColor); //Smazání naposled přidané barvy
		 				$('.changeBackgroundDetail').remove();
		 			}
		 			if(shadeCounter > 0){
		 				$('#content-page').removeClass(lastUsedShade); //Odebrání naposled použitého shade z aktivní classy
		 			}
	 				var color = jQuery(this).attr('id'); //Zjištění ID HTML elementu
	 				colorCounter = colorCounter + 1; // Přičtení do counteru
	 				lastUsedColor = color;//Uložení naposled použité barvy
	 				$('#content-page').addClass(color); //Přidání nové barvy	

 					if(color == "white" || color == "black"){
 						$('#colorDetail').material_select();
 					//Do nothing - nemají odstíny!
	 				}else{
	 				//Generování možností do selectoru (shades) pro právě zvolenou barvu pozadí
	 					$('#colorDetail').append('<option class="'+ color +' lighten-5 changeBackgroundDetail" value="lighten-5" data-icon="'+color+'"> světlá 5 </option> <option class="'+ color +' lighten-4 changeBackgroundDetail" value="lighten-4" data-icon="'+color+'"> světlá 4 </option> <option class="'+color+' lighten-3 changeBackgroundDetail" value="lighten-3" data-icon="'+color+'"> světlá 3 </option> <option class="'+color+' lighten-2 changeBackgroundDetail" value="lighten-2" data-icon="'+color+'"> světlá 2 </option> <option class="'+color+' lighten-1 changeBackgroundDetail" value="lighten-1" data-icon="'+color+'"> světlá 1 </option>  <option class="'+color+' changeBackgroundDetail" value="" data-icon="'+color+'" selected> základní </option> <option class="'+color+' darken-1 changeBackgroundDetail" value="darken-1" data-icon="'+color+'"> tmavá 1 </option> <option class="'+color+' darken-2 changeBackgroundDetail" value="darken-2" data-icon="'+color+'"> tmavá 2 </option> <option class="'+color+' darken-3 changeBackgroundDetail" value=" darken-3" data-icon="'+color+'"> tmavá 3 </option> <option class="'+color+' darken-4 changeBackgroundDetail" value="darken-4" data-icon="'+color+'"> tmavá 4 </option> <option class="'+color+' accent-1 changeBackgroundDetail" value="accent-1" data-icon="'+color+'"> nádech 1 </option> <option class="'+color+' accent-2 changeBackgroundDetail" value="accent-2" data-icon="'+color+'"> nádech 2</option> <option class="'+color+' accent-3 changeBackgroundDetail" value="accent-3" data-icon="'+color+'"> nádech 3 </option> <option class="'+color+' accent-4 changeBackgroundDetail" value="accent-4" data-icon="'+color+'"> nádech 4</option>');
 						$('#colorDetail').material_select(); //Znovu-načtení <option> elementu s novými options
	 				}
		 		});
		 			//Funkce pro změnu odstínu pozadí
		 		$(document.body).on('change', '#colorDetail', function(e) {
		 			e.preventDefault();
		 			if(shadeCounter > 0){
		 				$('#content-page').removeClass(lastUsedShade); //Odstranit naposledy použitou CSS Classu s odstínem
		 			}
		 			var shade = jQuery(this).val();
		 			lastUsedShade = shade;
		 			shadeCounter = shadeCounter + 1;
		 			$('#content-page').addClass(shade);
 				}); 

		 			//Funkce pro změnu průhlednosti pozadí
 				$(document.body).on('change', '#opacityRange', function(e) {
 					e.preventDefault();
 					var opacity = jQuery(this).val();
 					opacity = opacity / 100;
 					jQuery('#content-page').css('opacity', opacity);
 				});

 					//Funkce pro změnu globálního fontu textu
 				$(document.body).on('change', '#defaultFont', function(e) {
 					e.preventDefault();
 					var font = jQuery(this).val();
 					jQuery('#content-page').css('font-family', font);

 				});
 					//Funkce pro globální změnu barvy textu
 				$(document.body).on('change', '#defaultTextColor', function(e){
 					e.preventDefault();
 					var fontColor = jQuery(this).val();
 					jQuery('#content-page').css('color', fontColor);
				});

 					//Funkce pro globální změnu barvy pozadí prvků
 				$(document.body).on('change', '#defaultElementBackground', function(e){
 					e.preventDefault();
 					var elementBackground = jQuery(this).val();
 					jQuery('.moovable').css('background-color', elementBackground);
 				});

 					//Funkce pro globální změnu průhlednosti pozadí v prvcích
 				$(document.body).on('change', '#elementOpacityRange', function(e){
 					e.preventDefault();
 					var elementOpacity = jQuery(this).val();
 					elementOpacity = elementOpacity / 100;
 					jQuery('.moovable').css('opacity', elementOpacity);
 				});

					//Funkce, která přiradí source k obrázku (z modal okna)
				$(document.body).on('click', '.selectPicterCard', function(e){
					e.preventDefault();
					var pickedPhoto = jQuery(this).attr('src'); //získání SRC z kliknutí na obrázek v side-nav
					$('.sortable').append('<img id="custom_'+divCounter+' " src="'+pickedPhoto+'" class="moovable"/>');
					initDrag(); //Inicializovat Drag&Drop pro nově přidané prvky
					/*$('#profilePic').remove('src'); //Odstranení možného existujícího src
					$('#profilePic').attr('src', pickedPhoto); //Přiřazení nového src*/

				});

					//Funkce, která přiřadí source k profilovému obrázku v projektu
				$(document.body).on('click', '.sub_menu_image', function(e){
					e.preventDefault();
					var pickedPhoto = jQuery(this).attr('src'); //získání SRC z kliknutí na obrázek v side-nav
					$('#profilePic').remove('src'); //Odstranení možného existujícího src
					$('#profilePic').attr('src', pickedPhoto); //Přiřazení nového src

				});

					//Funkce pro zobrazení obrázků z uživatelské složky
				var usersFolder = '<?php echo $userInstance->show("FolderPath"); ?>';
 				var folder = "../users/"+usersFolder+"/images/"; //Uživatelská složka
					$.ajax({
					    url : folder,
					    success: function (data) {
					        $(data).find("a").attr("href", function (i, val) {
					            if( val.match(/\.(jpe?g|png|gif)$/) ) { 
					                $("#chooseImage").append( "<img class='sub_menu_image' src='"+ folder + val +"'>" );
					                $("#cards").append("<div id='"+val+"' class='card inner-picture hoverable'> <div class='card-image'> <img class='selectPicterCard modal-action modal-close' src='"+folder + val +"'/> <span class='card-title card-shadow'></span> </div> <div class='card-content'> <p> Vybrat tento obrázek </p> </div> </div> ");
					            } 
					        });
					    }
					});


				//CHYBA PŘI NAČÍTÁNÍ !!!!
				//Funkce pro nahrání JSON textové šablony uživatele do projektu
				$(document.body).on('change', '#showUserThemes', function(e){
					e.preventDefault();
					var themeName = jQuery(this).val();
					if(themeName == 'default'){
						//do nothing
					}else{			
						var themeDirectory = "<?php echo $userInstance->show('FolderPath'); ?>";
						var json = null;
						$.ajax({
							'async': false,
							'global': false,
							'url': '../users/'+themeDirectory+'/themes/'+themeName+'.json',
							'data': json,
							'dataType': 'json',
							'success': function(json){
								Materialize.toast("Šablona úspěšně použita!",4000);
								// Aktualizovat divy v projektu o uživatelskou šablonu
								$('div[id=experience] .description').text(json[0].experience);
								$('div[id=highschool] .description').text(json[0].highschool);
								$('div[id=username] .description').text(json[0].name);
								$('div[id=college] .description').text(json[0].college);
								$('div[id=bio] .description').text(json[0].bio);
								console.log('Chyba: '+JSON.stringify(response));

							},
							'error': function(data){
								alert("Nastala chyba při načtení šablony: "+JSON.stringify(data));
							},
						});
					}
				});

					//Funkce pro otevření modálního okna (editace vybraného textu)
				$(document.body).on('click', '.editable', function(e){
					e.preventDefault();
					actualEditingElementID = $(this).attr('id');
					actualEditingElementText = $(this).text();
					$('#textarea1').val(actualEditingElementText);
					$('#textEditor').modal('open');
					
				});

					//Funkce pro uložení a aktualizaci původního textu při uložení (ptvrzení) modálního okna
				$(document.body).on('click', '#saveText', function(e){
					e.preventDefault();
					actualEditingElementText = $('#textarea1').val();
					$('#'+actualEditingElementID).text(actualEditingElementText);
					$('#textarea1').val("");
					actualEditingElementID = "";
				});

					//Funkce která zpracuje uložení projektu a vytvoření záznamu o všech elementech -- IN PROGRESS
				$(document.body).on('click', '#saveProject', function(e){
					e.preventDefault();
				
					var project_id = "<?php echo $editor->show('pID'); ?>";
					
					//JSON s daty ohledně textových inputů a jejich pozici
					newJsonFile = [];
					$("div[class^='moovable']").each(function(){
						var id = $(this).attr('id');
						var top = $(this).offset().top; //Získání odsazení od vrchní hrany stránky
						var left = $(this).offset().left; //Získání odsazení zleva 
						var bckColor = $(this).css("background-color");
						var src = $(this).attr('src');
						
						item = {};
						item['id'] = id;
						item['top'] = top;
						item['left'] = left;
						item['background'] = bckColor;

						newJsonFile.push(item);
					});

					//Doplnění textových dat a jejich CSS nastavení
					$(".editable").each(function(){
						var id = $(this).attr('id');
						var text = $(this).text();
						var textCss = $(this).attr('style');

						item2 = {};
						item2['txtName'] = id;
						item2["txtContent"] = text;
						item2["txtStyle"] = textCss;

						newJsonFile.push(item2);
					});
			

					//JSON obsahující přidané vlastní prvky s texty (uložení) pokud jejich ID začíná na "custom"
					customJson = [];
					$("div[id^='custom']").each(function(){
						var C_id = $(this).attr('id');
						var C_top = $(this).offset().top;
						var C_left = $(this).offset().left;
						var C_bckColor = $(this).css("background-color");
						var C_titleName = $(this).find('h3').attr('id');
						var C_titleText = $(this).find('h3').text();
						var C_textName = $(this).find('p').attr('id');
						var C_textContent = $(this).find('p').text();
						var C_textColor = $(this).find('p').css('color');
						var C_pictureSrc = $(this).attr('src');

						item_C = {};
						item_C["id"] = C_id;
						item_C["top"] = C_top;
						item_C["left"] = C_left;
						item_C["background"] = C_bckColor;
						item_C["titleName"] = C_titleName;
						item_C["titleText"] = C_titleText;
						item_C["textName"] = C_textName;
						item_C["textContent"] = C_textContent;
						item_C["textColor"] = C_textColor;
						item_C["pictureSrc"] = C_pictureSrc;

						customJson.push(item_C);
					});

					var cjson = JSON.stringify(customJson);
					console.log(cjson);


					//JSON konfigurací projektu (globální textový formát, pozadí, etc)
					configJson = [];
					var G_background = $('#content-page').css('background-color');
					var G_font = $('#content-page').css('font-family');
					var G_color = $('#content-page').css('color');

					configItem = {};
					configItem['background'] = G_background;
					configItem['font'] = G_font;
					configItem['color'] = G_color;
					configItem['divCounter'] = divCounter;

					configJson.push(configItem);

					var jjson = JSON.stringify(newJsonFile);
					console.log(jjson);
			
					$.ajax({
						url: '../core.php?action=saveProject',
						type: 'POST',
						//contentType: 'application/json; charset=utf-8', //Formát, ve kterém já posílám
						dataType: 'text', //Formát odpovědi
						data: {elementJson: JSON.stringify(newJsonFile), configJson: JSON.stringify(configJson), customJson: JSON.stringify(customJson), ID: project_id},
						success: function(response){
							Materialize.toast("Projekt byl úspěšně uložen", 4000);
							console.log('Chyba: '+JSON.stringify(response));

						},
						error: function(response){
							Materialize.toast("Nastala chyba při uložení souboru", 4000);
							console.log('Chyba: '+JSON.stringify(response));
						},
					});
				});
	
		 	});
	
     	</script>
		<title> Editor / DocMe! </title>
	</head>
	<body>
		<div class="page">
			<ul class="editor-custom-menu custom-menu">
				<li class="deleteDiv"> Smazat celý prvek </li>
			</ul>
			<ul id="slide-out" class="side-nav fixed z-depth-3">
		        <li> <button class="btn waves-light waves-effect red menu-btn"> <a href="../index.php"> Zrušit </a> </button>  </li>
		         <li class="row"> <button id="saveProject" type="submit" class="btn blue waves-effect waves-light menu-btn" name="save"> Uložit </button></li>
		         <!-- <li class="no-padding">
		         	<ul class="collapsible collapsible-accordion">
		         		<li>
		         			<a class="collapsible-header"> <i class="material-icons"> insert_photo </i> Obrázky <i class="material-icons right"> arrow_drop_down </i> </a>
		         			<div class="collapsible-body">
		         				<ul>
		         					<div id="chooseImage" class="nav-submenu"> 
		         						<li> <a href="#pictureModal" class="modal-trigger btn waves-light waves-effect"> Zobrazit obrázky </a> </li>
	         						 	<form action="#">
											<div class="file-field input-field">
												<div class="btn center">
													<span>Nahrát obrázek</span>								
													<input type="file">
												</div> <br>
												<div class="file-path-wrapper">
													<input class="file-path validate" type="text">
												</div>
											</div>
										</form>
							      	</div>
							    </ul>
							</div>
						</li>
					</ul>
		      	</li> -->
		      	<li class="no-padding">
		      		<ul class="collapsible collapsible-accordion">
		      			<li>
		      				<a class="collapsible-header"> <i class="material-icons"> color_lens </i> Pozadí  <i class="material-icons right"> arrow_drop_down </i> </a>
		      				<div class="collapsible-body">
		      					<ul>
		      						<div class="nav-submenu">
			      						<li> <div id="red" class="changeBackground colorShow red"> </div> </li>
			      						<li> <div id="pink" class="changeBackground colorShow pink"></div> </li>
			      						<li> <div id="deep-orange" class="changeBackground colorShow deep-orange"> </div> </li>
			      						<li> <div id="orange" class="changeBackground colorShow orange"> </div> </li>
			      						<li> <div id="amber" class="changeBackground colorShow amber"> </div> </li>
			      						<li> <div id="yellow" class="changeBackground colorShow yellow">
			      						</div> </li>
			      						<li> <div id="indigo" class="changeBackground colorShow indigo"> </div> </li>
			      						<li> <div id="blue" class="changeBackground colorShow blue"> </div> </li>
			      						<li> <div id="light-blue" class="changeBackground colorShow light-blue"> </div>
			      						<li> <div id="cyan" class="changeBackground colorShow cyan"> </div> </li>
			      						<li> <div id="green" class="changeBackground colorShow green"></div> </li>
			      						<li> <div id="teal" class="changeBackground colorShow teal"> </div> </li>
			      						<li> <div id="light-green" class="changeBackground colorShow light-green"> </div> </li>
			      						<li> <div id="lime" class="changeBackground colorShow lime"> </div> </li>
			      						<li> <div id="deep-purple" class="changeBackground colorShow deep-purple"> </div> </li>
			      						<li> <div id="purple" class="changeBackground colorShow purple"></div> </li>
			      						<li> <div id="brown" class="changeBackground colorShow brown"> </div> </li>
			      						<li> <div id="grey" class="changeBackground colorShow grey"> </div> </li>
			      						<li> <div id="blue-grey" class="changeBackground colorShow blue-grey"> </div> </li>
			      						<li> <div id="black" class="changeBackground colorShow black"></div> </li>
			      						<li> <div id="white" class="changeBackground colorShow white z-depth-3"> </div>
			      						</li>	
			      					</div>
			      					<div class="row">
										<div class="input-field col s12">
											<span> Odstín barvy: </span>
											<select id="colorDetail" class="center">
											    <option value="" disabled>Vyberte si odstín</option>
										    </select>
									 	</div>
									</div>
									<div class="row">
										<div class="input-field col s12">
											<span> Průhlednost: </span>
											<div class="ranger">
												<p class="range-field">
											    	<input type="range" id="opacityRange" min="0" max="100" />
												</p>
											</div>
										</div>
									</div>
		      					</ul>
		      				</div>
		      			</li>
		      		</ul>
		      	</li>
		      	<li class="no-padding">
		      		<ul class="collapsible collapsible-accordion">
		      			<li>
		      				<a class="collapsible-header"> <i class="material-icons"> widgets </i> Prvky  <i class="material-icons right"> arrow_drop_down </i> </a>
			      			<div class="collapsible-body">
			      				<ul>
			      					<div class="nav-submenu">
				      					<li> <button class="createDiv btn waves-light waves-effect" value="div-txt"> Přidat text </button> </li>
				      					<li> <button class="createDiv btn waves-effect waves-light" value="div-img"> Přidat obrázek </button> </li>
				      					<li> <button class="createLine btn waves-effect waves-light" value="hr"> Přidat vodorovnou čáru </button> </li>
				      				</div>
			      				</ul>
			      			</div>
				      	</li>
				    </ul>
				</li>
				<li class="no-padding">
				    <ul class="collapsible collapsible-accordion">
				    	<li>
				      		<a class="collapsible-header"> Motivy <i class="material-icons"> collections </i> <i class="material-icons right"> arrow_drop_down </i> </a>
				      		<div class="collapsible-body">
					      		<ul>
					      			<div class="nav-submenu">
						      		  	<li> <button class="loadCSS btn waves-effect waves-light" value="1"> Vybrat CSS 1 </button> </li>
						      	  		<li> <button class="loadCSS btn waves-effect waves-light" value="2"> Vybrat CSS 2 </button> </li>
					        		</div>
					        	</ul>
					        </div>
					    </li>
				    </ul>	
		        </li>
		        <li class="no-padding">
		        	<ul class="collapsible collapsible-accordion">
		        		<li>
		        			<a class="collapsible-header"> Vaše šablony <i class="material-icons"> collections_bookmark </i> <i class="material-icons right"> arrow_drop_down </i> </a>
		        			<div class="collapsible-body">
								<ul>
									<div class="nav-submenu">
										<div class="input-field col s12">
											<span> Vaše textové šablony </span>
											<select id="showUserThemes" name="userThemes">
												<option id="default" value="default" disabled selected> Bez šablony </option> 
												<?php
													//Vypsání všech vytvořených uživatelských šablon pro tento typ projektu
													$userThemes = $editor->showUserThemes();
													foreach($userThemes as $theme){
														echo "<option id='theme".$theme['ID'] ."' value='".$theme['Name']."' class='userThemeOptions' > ". $theme['Name'] ."</option>";

													}
												?>
											</select>
										</div>
									</div>
								</ul>
							</div>
						</li>
					</ul>
				</li>
		        <li class="no-padding">
		        	<ul class="collapsible collapsible-accordion">
		        		<li>
		        			<a class="collapsible-header"> Nastavení <i class="material-icons"> settings </i> <i class="material-icons right"> arrow_drop_down </i> </a>
		        			<div class="collapsible-body">
		        				<ul>
		        					<div class="nav-submenu">
			        					<li>
			        						<div id="activateDragNdrop" class="switch">
			        							<label> Povolit Drag&Drop volný režim
			        								<input type="checkbox">
			        								<span class="lever"> </span>
			        							</label>
			        						</div>
			        					</li>
			        					<li>
			        						<div class="input-field col s12">
			        							<span> Globální textový font: </span>
			        							<select id="defaultFont" class="center">
			        								<option value="#" selected disabled> Font </option>
			        								<option value="arial"> Arial </option>
			        								<option value="verdana"> Verdana </option>
			        								<option value="Comic Sans MS"> Comic Snas MS </option>
			        								<option value="Times New Roman"> Times New Roman </option>
			        								<option value="Impact"> Impact </option>
			        								<option value="Lucida Sans Unicode"> Lucida Sans Unicode </option>
			        								<option value="Tahoma"> Tahoma </option>
			        								<option value="Courier New"> Courier New </option>
			        								<option value="Lucida Console"> Lucida Console </option>
			        							</select>
			        						</div>
			        					</li>
			        					<li>
			        						<div class="input-field col s12">
			        							<span> Globální barva textu: </span>
			        							<select id="defaultTextColor" class="center">
			        								<option value="#" selected disabled> Barva </option>
			        								<option value="#000000"> Černá </option>
			        								<option value="#ffffff"> Bílá </option>
			        								<option value="#607d8b"> Modro-šedá </option>
			        								<option value="#9e9e9e"> Šedá </option>
			        								<option value="#795548"> Hnědá </option>
			        								<option value="#ff5722"> Sytě oranžová </option>
			        								<option value="#ff9800"> Oranžová </option>
			        								<option value="#ffc107"> Jantarová </option>
			        								<option value="#ffeb3b"> Žlutá </option>
			        								<option value="#cddc39"> Limetková </option>
			        								<option value="#8bc34a"> Světle zelená </option>
			        								<option value="#4caf50"> Zelená </option>
			        								<option value="#009688"> Teal </option>
			        								<option value="#00bcd4"> Tyrkysová </option>
			        								<option value="#03a9f4"> Světle modrá </option>
			        								<option value="#2196f3"> Modrá </option>
			        								<option value="#3f51b5"> Indigo </option>
			        								<option value="#673ab7"> Sytě fialová </option>
			        								<option value="#9c27b0"> Fialová </option>
			        								<option value="#e91e63"> Růžová </option>
			        								<option value="#f44336"> Červená </option>
			        							</select> 
			        						</div>
			        					</li>
			        					<li> 
			        						<div class="input-field col s12">
			        							<span> Globální pozadí prvků: </span>
			        							<select id="defaultElementBackground" class="center">
			        								<option value="#" selected disabled> Barva </option>
			        								<option value="transparent">Průhledná </option>
			        								<option value="#000000"> Černá </option>
			        								<option value="#ffffff"> Bílá </option>
			        								<option value="#607d8b"> Modro-šedá </option>
			        								<option value="#9e9e9e"> Šedá </option>
			        								<option value="#795548"> Hnědá </option>
			        								<option value="#ff5722"> Sytě oranžová </option>
			        								<option value="#ff9800"> Oranžová </option>
			        								<option value="#ffc107"> Jantarová </option>
			        								<option value="#ffeb3b"> Žlutá </option>
			        								<option value="#cddc39"> Limetková </option>
			        								<option value="#8bc34a"> Světle zelená </option>
			        								<option value="#4caf50"> Zelená </option>
			        								<option value="#009688"> Teal </option>
			        								<option value="#00bcd4"> Cyan </option>
			        								<option value="#03a9f4"> Světle modrá </option>
			        								<option value="#2196f3"> Modrá </option>
			        								<option value="#3f51b5"> Indigo </option>
			        								<option value="#673ab7"> Sytě fialová </option>
			        								<option value="#9c27b0"> Fialová </option>
			        								<option value="#e91e63"> Růžová </option>
			        								<option value="#f44336"> Červená </option>
			        							</select>
			        						</div>
			        					</li>
			        					<li>
			        						<div class="input-field col s12">
												<span> Globální průhlednost pozadí prvků: </span>
												<div class="ranger">
													<p class="range-field"> 
												    	<input type="range" id="elementOpacityRange" min="0" max="100" />
													</p> 
												</div>
											</div>
										</li>
			        				</div>
		        				</ul>
		        			</div>
		        		</li>
		        	</ul>
		        </li>
    		</ul>
    		<a href="#" data-activates="slide-out" class="button-collapse"><i class="material-icons">menu</i></a>
			<div id="pictureModal" class="modal">
			    <div class="modal-content">
			        <h4>Vyberte obrázek</h4>
		      	    <div class="row">
					    <div id="cards" class="col s12 m4">
						    
				    	</div>
				    </div>
				</div>
				<div class="modal-footer">

				</div>
			</div>

		<!-- Textový editor (modální okno) -->
			<div id="textEditor" class="modal">
				<div class="modal-content">
					<h4> Editovat text </h4>
					<div class="row">
						<div class="input-field col s12">
						<!-- <div class="text-upper-pannel">
							<ul>
								<li> <i class="small material-icons"> format_bold </i> </li>
								<li> <i class="small material-icons"> format_underlined </i> </li>
								<li> <i class="small material-icons"> format_italic </i> </li>
								<li> <i class="small material-icons"> format_clear </i> </li>
								<li> <i class="small material-icons"> format_color_text </i>
									<ul>
										<li> red </li>
										<li> green </li>
										<li> blue </li>
									</ul>
								</li>
								<li> <i class="tiny material-icons"> format_color_fill </i>
									<ul>
										<li class="red"> red </li>
										<li class="blue"> b </li>
										<li> green </li>
									</ul>
								</li>
							</ul>
						</div>-->
						<div class="editor-inputs">
								<textarea id="textarea1" class="editor" name="textEdit"> </textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn modal-action modal-close waves-effect waves-light red"> Zavřít </button>
					<button id="saveText" class="btn modal-action modal-close waves-light waves-effect"> Uložit</button>
				</div>
			</div>
			</a>

	    <!-- Zde začíná div, ve kterém je projekt načten a zpracován -->
	    	<div class="content-background">
    			<div class="row">
		    		<div id="content-page" class="col s9 offset-s3">
		
							<div id='project-container'>
								<div class='sortable resizable'>
									<div id='image' class='moovable image'>
										<?php
										//Pokud uživatel nemá přiřazenou profilovou fotku
										if(($userInstance->show("ProfilePictureID") == "0") OR $userInstance->show("ProfilePictureID") == NULL){
 											echo "<img id='profilePic' class='responsive-img' src='../resources/images/default_user.jpg'/>";											
										}else{
											 echo "<img id='profilePic' class='responsive-img' src='../users/".$userInstance->show('FolderPath')."/images/". $userInstance->show('ProfilePictureID') ."_".$userInstance->show('ProfilePicture')."'/>"; 
										}
										?>
									</div>
									<div id='username' class='moovable'>
										<?php
										echo "<h3 id='usernameText' class='description editable'> ". $userInstance->show('Firstname') ." ". $userInstance->show('Surname')." </h3>";
										 ?>
									</div>
								</div>
							</div>





					<!-- <div id='project-container'>
						<div class='sortable resizable'>
							<div id='username' class='moovable'>
								<span id='usernameText' class='description editable'> ". $user[Firstname] ." ". $user[Surname] ."</span>
							</div>	
							<div id='image' class='moovable image'>
								<img id='profilePic' class='responsive-img' src='../resources/images/.jpg'/>
							</div>
							<div id='bio' class='moovable'>
								<span id='bioText' class='description editable'> Bio o mě, kdo že to jsem? </span>
							</div>
							<div id='experience' class='moovable'>
								<h3 id='experienceTitle' class='editable title'> Název firmy </h3>
								<span id='experiencePosition' class='editable'> Pozice ve firmě </span>
								<span id='experienceText' class='description editable'> Můj popisek firmy, co jsem všechno skvělého dělal </span>
							</div>
							<div id='highschool' class='moovable'>
								<h3 id='highschoolTitle' class='editable title'> Název vysoké školy </h3>
								<p id='highschoolPosition' class='editable'> Titul, kam jsem to dopracoval </p>
								<p id='highschoolText' class='description editable'> Co jsem se vše naučil </p>
							</div>
							<div id='school' class='moovable'>
								<h3 id='schoolTitle' class='editable title'> Název střední školy </h3>
								<p id='schoolPosition' class='editable'> Kdy jsem získal maturitu, od kdy do kdy jsem studoval </p>
								<p id='schoolText' class='description editable'> Moje úspěchy za dobu školy, co jsem se naučil </p>
							</div>
							<div id='ability' class='moovable'>
								<span id='abilityTitle' class='editable title'> Mé silné stránky </span>
								<p id='abilityText' class='editable'> - Čas věnovaný práci </p>
							</div>
							<div id='lang' class='moovable'>
								<span id='langTitle' class='editable title'> Jazyky </span>
								<p id='langText' class='editable'> seznam jazyků které ovládám </p>
							</div>
							<div id='skills' class='moovable'> 
								<span id='skillsTitle' class='editable title'> Mé schopnosti </span>
								<p id='skillsText' class='editable'> Nějaké ty skillz! :) </p>
							</div>
						</div>
						<div id='sortorder'> </div>
					</div> -->


								<!--<?php 
									$editor->load(); //Nahrání projektu z jeho složky (obsahuje všechny includes patřící k projektu)
						?> -->
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
