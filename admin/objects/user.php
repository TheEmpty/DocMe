<?php

	class USER
	{

	    private $DB;

	    function __construct($DB_con)
	    {
	      $this->DB = $DB_con;	    
	    }

	    //Funkce pro vytvoření nové instance třídy LOGGED_USER() -- naplnění constructoru
	    public function useFactory($id){
	    	require_once "logged_user.php";

	    	$sql = $this->DB->prepare("SELECT us.Firstname, us.Surname, us.Email, us.Level, us.Gender, us.DisplayName, us.Birth, um.FolderHash, um.LastLogin, um.LastIP, uc.ProfilePictureID, up.Name, up.ID FROM Users AS us JOIN UsersMeta AS um ON us.ID = um.UsersID JOIN UsersConfig AS uc ON us.ID = uc.UsersID LEFT JOIN UsersPictures AS up ON uc.ProfilePictureID = up.ID WHERE us.ID = :id LIMIT 1");
	    	$sql->execute(array(':id' => $id));	
	    	$sqlResult = $sql->fetchAll(PDO::FETCH_ASSOC);
	    	if(!empty($sqlResult)){
		    	foreach($sqlResult as $data){
		    		if(empty($data)){
		    			return null;
		    		}else{
		    			return new LOGGED_USER($this->DB, $id, $data["Firstname"], $data["Surname"], $data["Email"], $data["Level"], $data["FolderHash"], $data["LastLogin"], $data["LastIP"], $data["Gender"], $data["Birth"], $data["DisplayName"], $data["Name"], $data["ID"]);
		    		}
		    	}
		    }else{
		    	return "Prázdno!";
		    }

	    }
		//Testovací funkce - Zapsání / aktualizace private vlastností této třídy
		public function fill_me($fname, $level, $id, $email){
			$this->Firstname = $fname;
			$this->Level = $level;
			$this->ID = $id;
			$this->Email = $email;
		}

		//Vypsání danného uživatele (podle ID)
		public function showByID($id){
			try{
				$sql = $this->DB->prepare("SELECT * FROM Users WHERE ID = :id LIMIT 1");
				$sql->execute(array(":id" => $id));
				$sqlResult = $sql->fetchAll(PDO::FETCH_ASSOC);
				return $sqlResult;
			}catch(PDOException $e){
				$e->getMessage();
				return "Nepodařilo se připojit k databázi, zkuste to prosím později";
			}
		}

		//Vypsání všech dat z tabulky "Users"
	    public function showAll(){
	    	try
	    	{
	    		$stmt = $this->DB->prepare("SELECT * FROM Users ORDER BY ID DESC");
	    		$stmt->execute();
	    		$stmt_result = $stmt->fetchAll(PDO::FETCH_ASSOC);

	    		return $stmt_result;

	    	}catch(PDOException $e){
	    		$e->getMessage();
	    	}
	    }

	    //Funkce na smazání nahraných fotek / obrázků uživatele
	    public function deletePicture($id, $picID, $folder){
	    	try{

	    		$filePath = "users/".$folder."/images/".$picID;
	    		//Smazání ze složky
	    		if(is_file($filePath)){
	    			unlink($filePath);
	    		}

	    		$sql = $this->DB->prepare("SELECT ProfilePictureID FROM UsersConfig WHERE UsersID = :uID");
	    		$sql->execute(array(":uID" => $id));
	    		$sqlResult = $sql->fetchAll(PDO::FETCH_ASSOC);

	    		$pID = explode('_', $picID);
	    		$removeSQL = $this->DB->prepare("DELETE FROM UsersPictures WHERE ID = :id AND UsersID = :uID");
	    		$removeSQL->execute(array(":id" => $pID[0], ":uID" => $id));
	    		foreach($sqlResult as $data){
		    		if($pID[0] == $data["ProfilePictureID"]){
						$updateSQL = $this->DB->prepare("UPDATE UsersConfig SET ProfilePictureID = :picID WHERE UsersID = :uID");	    		
						$updateSQL->execute(array(":picID" => 1, ":uID" => $id));
			    	}	
			    }
			    	return "Smazáno";

	    	}catch(PDOException $e){
	    		$e->getMessage();
	    	}
	    }

	    //Funkce pro nastavení obrázku jako profilového obrázku
	    public function useAsProfilePic($id, $picID){
	    	try{
	    		//Rozdělení picID, ve kterém je nejen ID, ale i jméno a typ souboru (formát: ID_NAME_TYPE)
	    		$pID = explode("_", $picID);
	    		if($pID[0] != 0){
		    		$sql = $this->DB->prepare("UPDATE UsersConfig SET ProfilePictureID = :picID WHERE UsersID = :id LIMIT 1");
		    		$sql->execute(array(":id" => $id, ":picID" => $pID[0]));

		    		return "Obrázek nastaven jako profile_pic";
		    	}else{
		    		//return false;
		    	}
	    	}catch(PDOException $e){
	    		$e->getMessage();
	    	}
	    }

	    //Vypsání všech dat z tabulky "UsersMeta", kde je "UsersID" stejné jako "$id"
	    public function showAllMeta($id){
	    	try
	    	{
	    		$stmt = $this->DB->prepare("SELECT * FROM UsersMeta WHERE UsersID = :id LIMIT 1");
	    		$stmt->execute(array(":id" => $id));
	    		$stmt_result = $stmt->fetchAll(PDO::FETCH_ASSOC);

	    		return $stmt_result;
	    	}
	    	catch(PDOException $e){
	    		echo $e->getMessage();
	    	}
	    }

	    //Registrace a přihlášení pomocí googlu
	   public function register_google($name, $email){
	    	try{
	    		$parse_name = explode(" ", $name);
	    		$firstname = $parse_name[0];
	    		$surname = $parse_name[0];
    			$created = date("Y-m-d H:i:s");  
	    		$cIP = $this->WhatIP();

	    		//Kontrola, jestli je již toto googleID použito, případně k jakému účtu
	    		$sql_check_exist = $this->DB->prepare("SELECT ID, Level FROM Users WHERE Email = :email");
	    		$sql_check_exist->execute(array(":email" => $email));
	    		$check_result = $sql_check_exist->fetchAll(PDO::FETCH_ASSOC);
	    		if(!empty($check_result)){ 
	    			foreach($check_result as $data){
		    			//Pokud je již tento email registrován, přihlaš ho
		    			$sql_login_google = $this->DB->prepare("UPDATE UsersMeta SET LastLogin = :lastLogin, LastIP = :lastIP WHERE UsersID = :uID");
		    			$sql_login_google->execute(array(":lastLogin" => $created, ":lastIP" => $cIP, ":uID" => $data["ID"]));

			    		$_SESSION["Level"] = $data["Level"];
		                $_SESSION["user_session"] = $data["ID"];
		                $_SESSION["email"] = $email;
		                unset($_SESSION["error"]);

		    			return "Přihlášen!";	
		    		}
	    		}else{
	    			//Pokud email ještě zaregistrován v databázi není, vytvoř nový účet
		    		$sql_write_google = $this->DB->prepare("INSERT INTO Users(Firstname, Surname, Email, DisplayName, Gender, Level) VALUES (:firstname, :surname, :email, :name, :gender, :level)");
	    			$sql_write_google->execute(array(":firstname" => $firstname, ":surname" => $surname, ":email" => $email, ":name" => $name, ":gender" => "ostatni", ":level" => 0));
		    		
		    		$lastID = $this->LastInsertID();
		 			$folder = hash("SHA256", $email);
		    		unset($sql);

		           //Vytvoření "domovské" složky uživatele
		           $hashedEmail = hash("SHA256", $email);
		           $filepath = "../admin/users/".$hashedEmail;
				   $projectFolder = "../admin/users/".$hashedEmail."/projects";
				   $themeFolder = "../admin/users/".$hashedEmail."/themes";
				   $imagesFolder = "../admin/users/".$hashedEmail."/images";
				

		           mkdir($filepath);
		           mkdir($projectFolder);
		           mkdir($themeFolder);
		           mkdir($imagesFolder);

				    //Příprava na zápis do UsersMeta tabulky
		           $stmt_meta = $this->DB->prepare("INSERT INTO UsersMeta(UsersID, Created, CreatedIP, FolderHash) VALUES (:Id, :created, :Createdip, :FolderHash)");
		           $stmt_meta->execute(array(":Id" => $lastID, ":created" => $created, ":Createdip" => $cIP, ":FolderHash" => $hashedEmail)); 


		    		$sql = $this->DB->prepare("INSERT INTO UsersConfig(UsersID) VALUES (:uID)");
		    		$sql->execute(array(":uID" => $lastID));
		    		unset($sql);
		    			
		    		$_SESSION["Level"] = 0;
	                $_SESSION["user_session"] = $lastID;
	                $_SESSION["email"] = $email;
	                unset($_SESSION["error"]);

		    		return "new_user";
		    	}		    	

	    	}catch(PDOException $e){
	    		echo $e->getMessage();
	    	}
	    }

	    //Funkce pro uložení barevného schématu pro navigační menu pro danného uživatele
	    public function saveNav($id, $color){
	    	try{
	    		$sql = $this->DB->prepare("UPDATE UsersConfig SET NavColor = :color WHERE UsersID = :id");
	    		$sql->execute(array(":id" => $id, ":color" => $color));
	    		return "Uloženo";
	    	}catch(PDOException $e){
	    		echo $e->getMessage();
	    	}
	    }

	    //Funkce pro uložení barevného schématu pro buttony pro danného uživatele
	    public function saveBtn($id, $color){
	    	try{
	    		$sql = $this->DB->prepare("UPDATE UsersConfig SET ButtonColor = :color WHERE UsersID = :id");
	    		$sql->execute(array(":id" => $id, ":color" => $color));
	    		return "Uloženo";
	    	}catch(PDOException $e){
	    		echo $e->getMessage();
	    	}

	    }

	    //Registrace nového uživatele
	    public function register($Firstname_reg, $Surname_reg, $Password_reg, $Email_reg, $Level_reg)
	    {
	       try
	       {
	       		//Kontrola, jestli uživatel již není registrován
	           $new_password = hash("sha256", $Password_reg);
	   
	           $reg_time = date("Y-m-d H:i:s");  

	           $check = $this->DB->prepare("SELECT 1 FROM Users WHERE Email=:Email LIMIT 1");
	           $check->execute(array(":Email" => $Email_reg));
	           $checkRow = $check->fetchAll(PDO::FETCH_ASSOC);
	           if(empty($checkRow)){
	          
		           	//Zápis do databáze Users
		           $stmt = $this->DB->prepare("INSERT INTO Users(Firstname, Surname, Email, Password, Level) VALUES (:Firstname, :Surname, :Email, :Password, :Level)");
		           $stmt->execute(array(":Firstname" => $Firstname_reg, ":Surname" => $Surname_reg, ":Email" => $Email_reg, ":Password" => $new_password, ":Level" => $Level_reg)); 

		           $ipaddress = $this->WhatIP();
		           $lastUsedID = $this->LastInsertID();

		           //Vytvoření "domovské" složky uživatele
		           $hashedEmail = hash("SHA256", $Email_reg);
		           $filepath = "../admin/users/".$hashedEmail;
				   $projectFolder = "../admin/users/".$hashedEmail."/projects";
				   $themeFolder = "../admin/users/".$hashedEmail."/themes";
				   $imagesFolder = "../admin/users/".$hashedEmail."/images";

		           mkdir($filepath);
		           mkdir($projectFolder);
		           mkdir($themeFolder);
		           mkdir($imagesFolder);

				    //Příprava na zápis do UsersMeta tabulky
		           $stmt_meta = $this->DB->prepare("INSERT INTO UsersMeta(UsersID, Created, CreatedIP, FolderHash) VALUES (:Id, :created, :Createdip, :FolderHash)");
		           $stmt_meta->execute(array(":Id" => $lastUsedID, ":created" => $reg_time, ":Createdip" => $ipaddress, ":FolderHash" => $hashedEmail)); 

		            //Příprava na zápis do tabulky UsersConfig
		           $stmt_config = $this->DB->prepare("INSERT INTO UsersConfig(UsersID, ProfilePictureID) VALUES (:id, :pID)");
		           $stmt_config->execute(array(":id" => $lastUsedID, ":pID" => 1));

		           /*$stmt_picture = $this->DB->prepare("INSERT INTO UsersPictures(UsersID) VALUES (:id)");
		           $stmt_picture->execute(array(":id" => $lastUsedID));*/
		           unset($_SESSION["message"]);
		           unset($_SESSION["error"]);

		           $_SESSION["message"] = "Účet byl úspěšně vytvořen, nyní se můžete přihlásit";

		           $redirection = header("Location: login.php");

		           return $redirection; 
		    }else{
		       		unset($_SESSION["message"]);
		       		unset($_SESSION["error"]);
	       	   		$redirection = header("Location: registration.php");
	       	   		$_SESSION["error"] = "Účet s tímto emailem je již registrován";
		       		return $redirection;
	      	}
       }
       catch(PDOException $e)
       {
           echo $e->getMessage();
       }    
    }
	 
	 	//Přihlášení uživatele
	    public function login($Email_log, $Password_log)
	    {
	       try
	       {
	       	  unset($_SESSION["error"]);
	       	  $_SESSION["error"] = "Neshoda emailu a hesla!";
	       	  $temp_password = hash("sha256", $Password_log);
	          $stmt = $this->DB->prepare("SELECT * FROM Users WHERE Email=:Email LIMIT 1");
	          $stmt->execute(array(":Email"=>$Email_log));
	          $userRow=$stmt->fetch(PDO::FETCH_ASSOC);
	          if($stmt->rowCount() > 0)
	          {          	
	             if(($temp_password == $userRow["Password"]))
	             {
                  	$ID = $userRow["ID"];
                  	$firstname = $userRow["Firstname"];
                  	$surname = $userRow["Surname"];
                  	$email = $userRow["Email"];
                  	$this->fill_me($userRow["Firstname"], $userRow["Level"], $userRow["ID"], $userRow["Email"] );

                   	//Zápis do UsersMeta tabulky (data o loginu)
                  	$lastLogin = date("Y-m-d H:i:s");
                  	$lastUsedIP = $this->whatIP();    

                  	$stmt2 = $this->DB->prepare("UPDATE UsersMeta SET LastLogin = :lastLogin, LastIP = :lastUsedIP WHERE UsersID = :id LIMIT 1");
                  	$stmt2->execute(array(":id" => $userRow["ID"], ":lastLogin" => $lastLogin, ":lastUsedIP" => $lastUsedIP));

                  	$_SESSION["Level"] = $userRow["Level"];
	                $_SESSION["user_session"] = $userRow["ID"];
	                $_SESSION["email"] = $userRow["Email"];
	                unset($_SESSION["error"]);

	                $checkMeta = $this->DB->prepare("SELECT um.FolderHash, um.LastLogin, um.LastIP, up.Name, up.ID, uc.ProfilePictureID FROM UsersMeta AS um JOIN UsersConfig AS uc ON um.UsersID = uc.UsersID JOIN UsersPictures AS up ON up.ID = uc.ProfilePictureID WHERE um.UsersID = :id LIMIT 1");
	                $checkMeta->execute(array(":id" => $userRow["ID"]));
	                $metaRow = $checkMeta->fetchAll(PDO::FETCH_ASSOC);
	                foreach($metaRow as $metadata){
		                include_once "logged_user.php";
		                return "Přihlášen";
	             	}
	             	return true;
	             }
	             else
	             {
	                return $_SESSION["error"];
	             }
	          }else 
	          {
	          	return $_SESSION["error"];
	          }
	       }
	       catch(PDOException $e)
	       {
	           echo $e->getMessage();
	       }
	   }

 		public function edit($id, $username, $email, $password, $gender, $born){
	    	if($password != ""){
	    		$password = hash("SHA256", $password);
	    	}

	    	$time = strtotime($born);
	    	$birth = date('Y-m-d', $time);

		  	$sql = $this->DB->prepare("UPDATE Users SET DisplayName = COALESCE(NULLIF(:username,''),DisplayName), Birth = COALESCE(NULLIF(:birth, ''),Birth), Gender = COALESCE(NULLIF(:gender, ''),Gender), Email = COALESCE(NULLIF(:email, ''),Email), Password = COALESCE(NULLIF(:password, ''),Password) WHERE ID = :id LIMIT 1");
		  	$sql->execute(array(":username" => $username, ":birth" => $birth, ":gender" => $gender, ":email" => $email, ":password" => $password, ":id" => $id));

		  	return "Profil úspěšně změněn";

	   	}

	   	//Funkce pro editaci uživatelského profilu
	   /* public function edit($username, $email, $password, $gender, $born){
	    	if($password != ""){
	    		$password = hash("SHA256", $password);
	    	}
		  	$sql = $this->DB->prepare("UPDATE Users SET DisplayName = COALESCE(NULLIF(:username,''),DisplayName), Email = COALESCE(NULLIF(:email, ''),Email), Password = COALESCE(NULLIF(:password, ''),Password), Gender = COALESCE(NULLIF(:gender, ''),Gender), Birth = COALESCE(NULLIF(:born, ''),Birth) WHERE ID = :id LIMIT 1");
		  	$sql->execute(array(":username" => $username, ":email" => $email, ":password" => $password, ":gender" => $gender, ":born" => $born, ":id" => $this->ID));
		  	return "Success!";
	   	}*/

	   //Funkce, která ověřuje, zda-li je uživatel přihlášen
	   public function is_loggedin()
	   {
	      if(isset($_SESSION["user_session"]))
	      {
	         return true;
	      }else{
	      	$_SESSION["error"] = "Nejste přihlášen!";
	      	header("Location:../index.php");
	      }
	   }	 

	   //Funkce odhlašující uživatele
	   public function logout()
	   {
	        session_destroy();
	        unset($_SESSION["user_session"]);
            unset($_SESSION["username"]);
            unset($_SESSION[""]);
	   //     $way_to_login_page = "https://student.sps-prosek.cz/~kocvaja14/project/index.php";
	        return true;
	   }

	   //Funkce pro poslání emailu se zabezpečeným linkem pro obnovu hesla uživatele
	   public function recovery($userEmail){

	   		if(empty($userEmail)){
	   			return "Nezadal jste žádný email!";
	   		}else{

	   		$recover_request = $this->DB->prepare("SELECT * FROM Users WHERE Email=:Email LIMIT 1");
	   		$recover_request->execute(array(":Email" => $userEmail));
	   		$recover_result=$recover_request->fetch(PDO::FETCH_ASSOC);

	   		//Preparing recovery hash
	   		$time = date("His");
	   		$mail_hash = $recover_result["Email"] + $recover_result["ID"] + $time;
	   		$recover_hash = hash("sha256", $mail_hash);
	   		$recoverTime = date("Y-m-d H:i:s");

	   		$writeHash = $this->DB->prepare("UPDATE UsersMeta SET PasswordChanged=:passwordChanged, RecoveryHash=:recover_hash, RecoveryTime=:recoverTime WHERE UsersID=:id LIMIT 1 ");
	   		$writeHash->bindparam(":passwordChanged", $recoverTime);
	   		$writeHash->bindparam(":id", $recover_result["ID"]);
	   		$writeHash->bindparam(":recover_hash", $recover_hash);
	   		$writeHash->bindparam(":recoverTime", $recoverTime);
	   		$writeHash->execute();
			
	   		//Příprava emailu

	   		$recovery_link = "https://student.sps-prosek.cz/~kocvaja14/omg/admin/core.php?action=recovery_link&hash=". $recover_hash;
	   		$headers = "From: developers@docme.cz";
	   		//Tělo mailu 
	   		$send_body = wordwrap("<h1> Vážený/á ". $recover_result["Firstname"] ." ". $recover_result["Surname"] .", </h1> \n \n zažádal jste o obnovení vašeho hesla, pokud jste o obnovu hesla nežádal, prosíme, ignorujte tento automaticky generovaný email. Pro nastavení nového hesla a obnovu profilu, klikněte prosím, na odkaz níže. \n \n ". $recovery_link ." \n \n Těšíme se na Vaši další návštěvu naší webové služby, \n s přáním příjemného zbytku dne, \n tým administrátorů a vývojářů DocMe! \n \n ---------------------------------------------------------------------------- \n \n Dear ". $recover_result["Firstname"] ." ". $recover_result["Surname"] .",\n \n you asked us for recovering your password! If you didn't, just ignore this automatic email. If you did, please, click on the link below to set your new password and recover your account. \n \n ". $recovery_link ." \n \n We are looking forward to your next visit! \n Best regards, \n your DocMe! Developer team!", 70);

	   		//PHPMAILER PŘEDĚLAT
	   		mail($recover_result["Email"], "DocMe! - Recover your password", $send_body, $headers); //Odeslání emailu
	   		
	   		unset($_SESSION["annoucment"]);
	   		$_SESSION["annoucment"] = "Obnova hesla byla úspěšná, zkontrolujte si svůj email!";
	   		return $_SESSION["annoucment"]; 
	   		}
	   	}

	   	//Funkce pro odstranění uživatele
	   	public function deleteMe($id){
	   		try
	   		{	
	   			$emailSelect = $this->DB->prepare("SELECT * FROM Users WHERE ID = :id LIMIT 1");
	   			$emailSelect->execute(array(":id" => $id));
	   			$sqlEmail = $emailSelect->fetchAll(PDO::FETCH_ASSOC);
	   			foreach($sqlEmail as $kkk){
	   				$hashedEmail = hash("SHA256", $kkk["Email"]);

		   			$stmt = $this->DB->prepare("DELETE FROM UsersMeta WHERE UsersID = :id LIMIT 1");
		   			$stmt->execute(array(":id" => $id));

			   		$sql = $this->DB->prepare("DELETE FROM Users WHERE ID = :id LIMIT 1");
			   		$sql->execute(array(":id" => $id));

		            $user_folder = "../admin/users/".$hashedEmail;
		            $this->deleteDir($user_folder);

			   		unset($_SESSION["message"]);
			   		$_SESSION["message"] = "Účet byl úspěšně smazán";
			   	}
		   		return $_SESSION["message"];	
		   	}catch(PDOException $e){
		   		return $e->getMessage();
		   	}
	   }

			private function deleteDir($dirPath) {
			    if (! is_dir($dirPath)) {
			        throw new InvalidArgumentException("$dirPath musí být složka!");
			    }
			    if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
			        $dirPath .= '/';
			    }
			    $files = glob($dirPath . '*', GLOB_MARK);
			    foreach ($files as $file) {
			        if (is_dir($file)) {
			            self::deleteDir($file);
			        } else {
			            unlink($file);
			        }
			    }
			    rmdir($dirPath);
			}

		private function delTree($dir){ 
		   $files = array_diff(scandir($dir), array('.','..')); 
		    foreach ($files as $file) { 
		      (is_dir("$dir/$file")) ? delTree("$dir/$file") : unlink("$dir/$file"); 
		    } 
		    return rmdir($dir); 
		  }	  

	   private function whatIP(){
	   	//Zjištění uživatelovi IP adresy
	           	$ipaddress = '';
			    if (getenv('HTTP_CLIENT_IP'))
			        $ipaddress = getenv('HTTP_CLIENT_IP');
			    else if(getenv('HTTP_X_FORWARDED_FOR'))
			        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
			    else if(getenv('HTTP_X_FORWARDED'))
			        $ipaddress = getenv('HTTP_X_FORWARDED');
			    else if(getenv('HTTP_FORWARDED_FOR'))
			        $ipaddress = getenv('HTTP_FORWARDED_FOR');
			    else if(getenv('HTTP_FORWARDED'))
			       $ipaddress = getenv('HTTP_FORWARDED');
			    else if(getenv('REMOTE_ADDR'))
			        $ipaddress = getenv('REMOTE_ADDR');
			    else
			        $ipaddress = 'UNKNOWN';    
			    return $ipaddress;
	   }

	   private function LastInsertID(){
	   		return $this->DB->lastInsertId();
	   }
	}
?>