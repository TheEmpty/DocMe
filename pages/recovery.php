<?php
	session_start();
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="utf-8" />
		<meta author="Jan Kočvara" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
     	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
     	<link rel="stylesheet" type="text/css" href="../resources/styles/style.css" />
     	<script> $(".button-collapse").sideNav(); </script>
         
		<title> DocMe! </title>
	</head>
	<body>
		<div class="page">
			<?php  include_once "../resources/includes/nav_pages.php";
			 	   include_once "../resources/includes/msg.php";
			?>		
			<div class="content">
				<h2 class="text-center"> Obnova účtu </h2>
			 	<div class="row">
			    	 <?php echo '<form class="col s12" method="POST" action="../admin/core.php?action=recovery_script&ID='. $_SESSION["recovery_who"] .'">'; ?>
		      			<div class="row">
					        <div class="input-field col s6">
				        	   <input name="recovery_password" id="recovery_password" type="password" class="validate" minlength="5">
					           <label for="recovery_password" data-error="Zdejte minimálně 5 znaků">Nové heslo</label>
					        </div>
					        <div class="row">
						        <div class="input-field col s6">
						        	<input name="repassword" id="repassword" type="password" class="validate" minlength="5">
					          		<label for="repassword" data-error="Zadejte minimálně 5 znaků">Znovu zopakujte své heslo</label>
						        </div>
					        </div>
						    </div>
					        <button class="btn waves-effect waves-light" type="submit" name="action"> Obnovit účet </button>
			   		</form>
				</div>
			</div>
			<?php include_once "../resources/includes/footer.php" ?>
		</div>
	</body>
</html> 