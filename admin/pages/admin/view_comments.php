<?php 
	session_start();
	require_once "../../config.php";
	require_once "../../objects/user.php";
	require_once "../../objects/article.php";
	$user = new USER($db);
	$user->is_loggedin();
	$article = new ARTICLE($db);
?>
<!DOCTYPE HTML>
<html>
 	<head>
 		<meta charset="utf-8" />
		<meta author="Jan Kočvara" />

		<link rel="stylesheet" type="text/css" href="../resources/styles/dd.css"/>

		<link rel="stylesheet" type="text/css" href="../resources/styles/style.css" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css"/>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
     	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>

     	<script> $(".button-collapse").sideNav(); </script>
     	<title> DocMe! </title>

 	</head>
 	<body>
 		<div class="page">
 			<div class="content">
 				<?php
 					$comments = $article->showComments();
 					
 				?>
 			</div>
 		</div>
 	</body>
</html>