<!DOCTYPE HTML>
 <html>
 	<head>
 		<meta charset="utf-8" />
		<meta author="Jan Kočvara" />

		<link rel="stylesheet" type="text/css" href="resources/styles/dd.css"/>

		<link rel="stylesheet" type="text/css" href="resources/styles/style.css" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css"/>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
	    <script src="js/interact.js"> </script>
	    <script src="js/basic.js"> </script>
     	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>
     	<script>
     	$(document).ready(function () {
     		$(".button-collapse").sideNav();
     		$('.modal').modal();

     	});
     	</script>
     	<title> DocMe! </title>

 	</head>
 	<body>
 		<div class="page">
 			<?php 
 				include_once "../../resources/includes/nav_sub.php";
 				include_once "../../resources/includes/admin_menu_sub.php";
 				include_once "../../../resources/includes/msg.php";
	 		?>
	 		<h1 class="center"> Přehled příspěvků </h1>

	 		<?php 
	 			include_once "../../resources/scripts/pdo.php";
	 			include_once "../../objects/project.php";
	 			$project = new PROJECT($db);
	 			$resultedProject = $project->show();
					
					echo "
		    			<table class='highlight'>
		    				<thead>
		    					<tr>
		    						<th> ID </th>
		    						<th> Jméno projektu </th>
                                    <th> Popisek projektu </th>
		    						<th> Typ projektu </th>		    					
		    						<th> Vytvořen </th> 
		    					    <th> Vlastník </th>
		    					</tr>
		    				</thead>
		    				<tbody>";

	 			foreach($resultedProject as $data){ //Získání dat z tabulky "Projects"
	 				$resultSearch = $project->showUsersProject($data["User_ID"]);
					foreach($resultSearch as $users_data){ //Získání dat z tabulky "Users"
				    	echo "	    				
	    					<tr>
	    						<td>". $data['ID'] ."</td>
	    						<td>". $data['Name'] ."</td>
                                <td>". $data['Description'] ."</td>
	    						<td>". $data['Type']. "</td>
	    						<td>". $data['Created'] ." </td>
	    						<td>". $users_data['Firstname'] ." ". $users_data["Surname"]."</td>
	    						<td> <a href='#modal1' class='modal-trigger' title='Zobrazit detaily zápisu'><i class='material-icons'>fullscreen</i></a></td>
	    						<td> <a href='#modal_edit' class='modal-trigger' title='Editovat zápis'><i class='material-icons'>mode_edit</i></a></td>
	    						<td> <a href='../../core.php?action=deleteProject&ID=". $data['ID'] ."' title='Smazat zápis'><i class='material-icons'>clear</i></a></td>
	    					</tr>
				    		";

						
						    //Vytvoření modálního okna s detaily o projektu
					    	echo 
					    	'
					 		  <!-- Modal Structure -->
							  <div id="modal1" class="modal modal-fixed-footer">
						  		 <div class="modal-content">
								      <h4 class="center"> Detaily o příspěvku</h4>
								      <h5> '. $data["Name"] .' </h5> <span> <i> <b> Vlastník: </b> '. $users_data["Firstname"] .' '. $users_data["Surname"] .' <b> Vytvořeno: </b> '. $data["Created"] .' <b> Typ: </b>'. $data["Type"] .' </i> </span> <br/> <hr/>
								      <p> <i> '. $data["Description"] .' </i> </p> <br/> 
								 </div>
								 <div class="modal-footer">
								      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ">Agree</a>
						    	 </div>
						      </div>
						     <div id="modal_edit" class="modal modal-fixed-footer">
								    <div class="modal-content">
								      <h4>Upravit projekt</h4>
								      	    <div class="row">
											      <form class="col s12" method="POST" action="../../core.php?action=editProject">
												      <div class="row">
												        <div class="input-field col s6">
												          <input name="name" id="name" type="text" class="validate" placeholder="'. $data["Name"] .'"/>
												          <label for="name">Název</label>
												        </div>
													  </div>
												      <div class="row"> 
												        <div class="input-field col s6">
												          <input name="description" id="description" type="text" class="validate" placeholder="'. $data["Description"] .'"/>
												          <label for="description">Popisek</label>
												        </div>
												      </div>
												      <div class="row">
												      	<input id="ID" name="ID" type="hidden" value="'. $data["ID"] .'"/>
												      	<button id="submit" class="btn waves-effect waves-light right" type="submit" name="action"> Upravit projekt </button>
												      </div>
												  </form>
										    </div>
										</div>
										<div class="modal-footer">
										    <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ">Zrušit</a>
										</div>
									</div>
 					    	';		
					    	}
					//	}
					}
				echo "</tbody>
			    			</table>";
			    	
	 		?>
	 		  

	 	</div>
	 </body>
</html>