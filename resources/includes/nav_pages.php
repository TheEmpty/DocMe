			<nav>
				<div class="nav-wrapper">
					<a href="../index.php" class="brand-logo">DocMe!</a>
					<a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
					<ul id="nav-mobile" class="right hide-on-med-and-down">
						<li><a href="about.php" title="Details about project"> O projektu </a> </li>
						<li><a href="log.php" title="What changed in last update?"> Novinky</a> </li>
						<li><a href="faq.php" title="The most asked questions by users"> F&Q </a> </li> 
						<li><a href="../admin/index.php"> <i class="material-icons left">person_pin</i> Profilové rozhraní </a> </li>
					</ul>
				</div>
			</nav>