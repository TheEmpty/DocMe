<!DOCTYPE HTML>
 <html>
 	<head>
 		<meta charset="utf-8" />
		<meta author="Jan Kočvara" />

		<link rel="stylesheet" type="text/css" href="resources/styles/dd.css"/>

		<link rel="stylesheet" type="text/css" href="resources/styles/style.css" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css"/>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
     	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>
     	<script src="resources/scripts/dropdownContextMenu.js"> </script>
     	<script>
     	$(document).ready(function () {
     		$(".button-collapse").sideNav();

     	});

     	</script>
     	<title> DocMe! </title>

 	</head>
 	<body>
 		<div class="page">
 			<?php 
 				include_once "resources/scripts/user_color.php";
 				include_once "resources/includes/nav.php";
 				include_once "resources/includes/admin_menu.php";
	 		?>
	 		<h1> ADMIN INTERFACE TEST </h1>

	 		<?php echo "<a href='core.php?action=logout'> logout </a>"; ?>

	 		<ul class='custom-menu'>
			  <li data-action="first">First thing</li>
			  <li data-action="second">Second thing</li>
			  <li data-action="third">Third thing</li>
			</ul>

				<span class="context-menu-one btn btn-neutral">right click me</span>
	 	</div>
	 </body>
</html>