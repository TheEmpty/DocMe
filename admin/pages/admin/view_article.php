<!DOCTYPE HTML>
 <html>
 	<head>
 		<meta charset="utf-8" />
		<meta author="Jan Kočvara" />

		<link rel="stylesheet" type="text/css" href="resources/styles/dd.css"/>

		<link rel="stylesheet" type="text/css" href="resources/styles/style.css" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css"/>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
	    <script src="js/interact.js"> </script>
	    <script src="js/basic.js"> </script>
     	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>
     	<script>
     	$(document).ready(function () {
     		$(".button-collapse").sideNav();
     		$('.modal').modal();
     	});
     	</script>
     	<title> DocMe! </title>

 	</head>
 	<body>
 		<div class="page">
 			<?php 
 				include_once "../../resources/includes/nav_sub.php";
 				include_once "../../resources/includes/admin_menu_sub.php";
 				include_once "../../../resources/includes/msg.php";
	 		?>
	 		<div class="row">
	 			<div class="col s10 offset-s1">
		 		<h1 class="center"> Přehled příspěvků </h1>

		 		<?php 
		 			include_once "../../resources/scripts/pdo.php";
		 			include_once "../../objects/article.php";
		 			$article = new ARTICLE($db);
		 			$filter = "";
		 			$resultedArticle = $article->viewArticleTable($filter);
						
						echo "
			    			<table class='highlight'>
			    				<thead>
			    					<tr>
			    						<th> ID </th>
			    						<th> Jméno příspěvku </th>
			    						<th> Popisek příspěvku </th>
			    						<th> Obsah popisku </th>
			    						<th> Typ příspěvku </th>
			    						<th> Vytvořen </th>
			    						<th> Upraven </th>
			    						<th> Stav </th>
			    					</tr>
			    				</thead>
			    				<tbody>";

		 			foreach($resultedArticle as $data){
						$modal_id = "#modal_". $data['ID'] .""; //unikátní modal see ID
						$modal_id_d = "modal_". $data['ID']. ""; //unikátní modal see ID pro samotný div (id)
						$modal_id_edit = "#modal_edit_". $data['ID'] .""; // unikátní modal edit ID
						$modal_id_edit_d = "modal_edit_". $data['ID'] .""; // unikátní modal edit ID pro samotný div (id)
					    /*$short_content = substr($data["Content"], 0, 30);
					    $short_description = substr($data["Description"], 0, 30);*/
					    echo "                      
					        <tr>
					            <td>". $data['ID'] ."</td>
					            <td>". $data['Name'] ."</td>
					            <td>". $data['Description'] ."</td>
					            <td>". $data['Content'] ." </td>
					            <td>". $data['Type']. "</td>
					            <td>". $data['Created'] ." </td>
					            <td>". $data['Modified'] ."</td>
					            <td>". $data['Status'] . "</td>
					            <td> <a href='". $modal_id ."' class='modal-trigger' title='Zobrazit detaily zápisu'><i class='material-icons'>fullscreen</i></a></td>
					            <td> <a href='". $modal_id_edit ."' class='modal-trigger' title='Editovat zápis'><i class='material-icons'>mode_edit</i></a></td>
					            <td> <a href='../../core.php?action=deleteArticle&ID=". $data['ID'] ."' title='Smazat zápis'><i class='material-icons'>clear</i></a></td>
						        </tr>
						        <div id='". $modal_id_edit_d ."' class='modal modal-fixed-footer'>
						            <div class='modal-content'>
						                <h4>Editovat příspěvek</h4>
						                <div class='row'>
						                    <form class='col s12' method='POST' action='../../core.php?action=editArticle'>
						                        <div class='row'>
						                            <div class='input-field col s6'>
						                                <input name='name' id='name' type='text' class='validate' placeholder='". $data["Name"] ."'/>
						                                <label for='name'>Název</label>
						                            </div>
						                        </div>
						                        <div class='row'> 
						                            <div class='input-field col s6'>
						                                <input name='description' id='description' type='text' class='validate' placeholder='". $data["Description"] ."'/>
						                                <label for='description'>Popisek</label>
						                            </div>
						                        </div>
						                        <div class='row'>
						                            <input id='ID' name='ID' type='hidden' value='". $data["ID"] ."'/>
						                            <button id='submit' class='btn waves-effect waves-light right' type='submit' name='action'> Upravit projekt </button>
						                        </div>
						                    </form>
						                </div>
						            </div>
						            <div class='modal-footer'>
						                <a href='#!'' class='modal-action modal-close waves-effect waves-green btn-flat ''>Zrušit</a>
						            </div>
						        </div>
						    ";
							
						    //Vytvoření modálního okna s detaily o článku
						    	echo 
						    	'
						 		  <!-- Modal Structure -->
								  <div id="'. $modal_id_d .'" class="modal modal-fixed-footer">
							  		 <div class="modal-content">
									      <h4 class="center"> Detaily o příspěvku</h4>
									      <h5> '. $data["Name"] .' </h5> <span> <i> <b> Vytvořeno: </b> '. $data["Created"] .' <b> Upraveno: </b>'. $data["Modified"] .' <b> Typ: </b>'. $data["Type"] .' <b> Stav: </b> '. $data["Status"] .'</i> </span> <br/> <hr/>
									      <p> <i> '. $data["Description"] .' </i> </p> <br/> <p> '. $data["Content"] .' </p>
									 </div>
									 <div class="modal-footer">
									      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ">Agree</a>
							    	 </div>
							      </div>
						    	';		
						    	}

							echo "</tbody>
						    			</table>";
						    	
				 		?>
				</div>
			</div>
	 	</div>
	 </body>
</html>